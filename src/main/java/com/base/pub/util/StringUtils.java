package com.base.pub.util;

public class StringUtils {

	/**
	 * Returns a String concatenating the specified String,
	 * retaining their spans if any.
	 */
	public static String concat(String... text) {
		if (text.length == 0) {
			return "";
		}
		if (text.length == 1) {
			return text[0];
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < text.length; i++) {
			sb.append(text[i]);
		}
		return sb.toString();
	}
}
