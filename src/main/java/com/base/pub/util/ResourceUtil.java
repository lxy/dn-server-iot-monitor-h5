package com.base.pub.util;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 资源工具类 Class Name: ResourceUtil.java
 * 
 * @author songxy DateTime 2015/8/28 16:17
 * @company winter
 * @email thinkdata@163.com
 * @version 1.0
 */
public class ResourceUtil extends PropertyPlaceholderConfigurer implements
		ApplicationContextAware {

	final static String TAG = "ResourceUtil";
	public static final String[] PICTURE_SUFFIX = { "png", "jpg", "jpeg" };
	private static Map<String, Object> propsMap;
	private static ApplicationContext applicationContext;

	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactory, Properties props)
			throws BeansException {
		super.processProperties(beanFactory, props);

		propsMap = new HashMap<String, Object>();
		for (Map.Entry<Object, Object> entry : props.entrySet()) {
			String key = entry.getKey().toString();
			Object obj = entry.getValue();
			propsMap.put(key, obj);
		}

	}

	public static Object getContextProps(String name) {
		try {
			return propsMap.get(name);
		} catch (NullPointerException e) {
			return null;
		}
	}

	public static File getPictureFile() {
		URI path = URI.create((String) getContextProps("file.path"));
		File file1 = new File(path);
		if (!file1.exists()) {
			file1.mkdirs();
		}
		return file1;
	}

	public static <T> T getBean(Class<T> classType) {
		return applicationContext.getBean(classType);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
}
