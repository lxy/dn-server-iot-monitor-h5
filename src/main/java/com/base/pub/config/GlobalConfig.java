package com.base.pub.config;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.base.pub.util.PropertiesLoader;
import com.google.common.collect.Maps;

@Configuration
@PropertySource(value = "classpath:common.properties", ignoreResourceNotFound = true)
public class GlobalConfig {
	/**
	 * 当前对象实例
	 */
	private final static GlobalConfig			global	= new GlobalConfig();

	/**
	 * 保存全局属性值
	 */
	private final static Map<String, String>	map		= Maps.newHashMap();

	/**
	 * 获取当前对象实例
	 */
	public static GlobalConfig getInstance() {
		return global;
	}
    /**
     * token有效期（小时）
     */
    public static final int TOKEN_EXPIRES_HOUR = 72;

    /**
     * 存放Authorization的header字段
     */
    public static final String AUTHORIZATION = "authorization";
	
	private static PropertiesLoader	loader	= new PropertiesLoader("common.properties");

	public static String getConfig(String key) {
		String value = map.get(key);
		if (value == null) {
			value = loader.getProperty(key);
			map.put(key, value != null ? value : StringUtils.EMPTY);
		}
		System.out.println("key="+key+" ,value="+value);
		return value;
	}

	/**
	 * 获取管理端根路径
	 */
	public static String getAdminPath() {
		return getConfig("adminPath");
	}
	

}
