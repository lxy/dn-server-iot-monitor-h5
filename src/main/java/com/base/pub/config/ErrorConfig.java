package com.base.pub.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource(value = "classpath:error.properties", ignoreResourceNotFound = true)
public class ErrorConfig {

	private static Environment	environment;

	@Autowired
	private void setEnvironment(Environment env) {
		environment = env;
	}

	public String getDescription(String no) {
		return getDes(no);
	}

	public static String getDes(String no) {
		try {
			return environment.getRequiredProperty(no);
		} catch (IllegalStateException e) {
			return "unKnown Errors " + e.getMessage();
		}
	}
}
