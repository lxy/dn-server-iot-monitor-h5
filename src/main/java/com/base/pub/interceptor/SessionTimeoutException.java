package com.base.pub.interceptor;

@SuppressWarnings("serial")
public class SessionTimeoutException extends Exception {

	public SessionTimeoutException(String msg) {
		super(msg);
	}

}
