package com.base.pub.message;

import java.io.Serializable;

public class ComMessage<T> implements Serializable {
	private static final long	serialVersionUID	= -7131405393369255512L;

	private int					code;

	private String				description;

	private T					result;

	public ComMessage() {
	}

	public ComMessage(int no, String description) {
		this.code = no;
		this.description = description;
	}

	public ComMessage(int no) {
		this.code = no;
	}

	public ComMessage(T result) {
		this.code = 0;
		this.result = result;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

}