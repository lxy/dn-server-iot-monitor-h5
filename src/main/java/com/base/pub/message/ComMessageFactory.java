package com.base.pub.message;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.base.pub.config.ErrorConfig;

@Component
public class ComMessageFactory {

	private static ErrorConfig	errorConfig;

	@Resource
	public void setErrorConfig(ErrorConfig config) {
		ComMessageFactory.errorConfig = config;
	}

	public ComMessage newMessage(String no) {
		return new ComMessage(-1, errorConfig.getDescription(no));
	}

	public ComMessage newMessage(String no, Object... formatObjs) {
		return new ComMessage(-1, String.format(errorConfig.getDescription(no), formatObjs));
	}
}
