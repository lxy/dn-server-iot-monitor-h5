package com.base.pub.repository;

import org.dragonnova.business.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * User类的CRUD操作
 * @see User
 */
public interface UserRepository extends CrudRepository<User, Long> {

    public User findByUsername(String username);
}
