package com.base.pub.websocket;

import java.io.IOException;

import org.dragonnova.business.web.WebSocketSessionManagerAdapter;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

public class SimpleWebSocketHandler extends AbstractWebSocketHandler implements
		DisposableBean {

	@Autowired
	private WebSocketSessionManagerAdapter webSocketSessionManager;

	@Override
	public void destroy() throws Exception {
		webSocketSessionManager.close();
	}

	protected WebSocketSessionManagerAdapter getWebSocketSessionManager() {
		return webSocketSessionManager;
	}

	protected void closeSession(WebSocketSession session) throws IOException {
		webSocketSessionManager.removeViewMapper(session);
		webSocketSessionManager.removeSession(session);
	}

	protected void initSession(WebSocketSession session) {
		webSocketSessionManager.addSession(session);
		webSocketSessionManager.setViewMapper(session, session.getUri()
				.getPath());

	}

	protected void sendMessage(TextMessage message) throws IOException {
		webSocketSessionManager.sendAllSession(message);
	}
}
