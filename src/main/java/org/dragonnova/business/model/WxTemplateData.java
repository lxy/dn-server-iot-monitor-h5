package org.dragonnova.business.model;

import org.dragonnova.business.model.WxDataValueColor;

public class WxTemplateData {
	private WxDataValueColor first;
	private WxDataValueColor keynote1;
	private WxDataValueColor keynote2;
	private WxDataValueColor keynote3;
	private WxDataValueColor remark;

	public WxDataValueColor getFirst() {
		return first;
	}

	public void setFirst(WxDataValueColor first) {
		this.first = first;
	}

	public WxDataValueColor getKeynote1() {
		return keynote1;
	}

	public void setKeynote1(WxDataValueColor keynote1) {
		this.keynote1 = keynote1;
	}

	public WxDataValueColor getKeynote2() {
		return keynote2;
	}

	public void setKeynote2(WxDataValueColor keynote2) {
		this.keynote2 = keynote2;
	}

	public WxDataValueColor getKeynote3() {
		return keynote3;
	}

	public void setKeynote3(WxDataValueColor keynote3) {
		this.keynote3 = keynote3;
	}

	public WxDataValueColor getRemark() {
		return remark;
	}

	public void setRemark(WxDataValueColor remark) {
		this.remark = remark;
	}

}