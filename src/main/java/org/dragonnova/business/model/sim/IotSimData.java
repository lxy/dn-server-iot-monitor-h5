package org.dragonnova.business.model.sim;

import java.util.List;

/**
 * 物联网卡请求结果类
 * @author cheng
 *
 */
public class IotSimData<T> {

	private Integer error;
	private String reason;
	private T result ;
	public Integer getError() {
		return error;
	}
	public void setError(Integer error) {
		this.error = error;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	
}
