package org.dragonnova.business.model.sim;

/**
 * Sim物联网卡详情类
 * @author cheng
 *
 */
public class SimDetail {

	private String iccid;
    private String sim;
    private String imsi;
    private Integer status;
    private Integer boot_desc;
    private Integer packagesn;
    private String packageX;
    private Integer amount_usage;
    private Integer done_usage;
    private Integer surplus_usage;
    private String expire_date;
    private String open_time;
    private Integer realname_status;

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBoot_desc() {
        return boot_desc;
    }

    public void setBoot_desc(Integer boot_desc) {
        this.boot_desc = boot_desc;
    }

    public Integer getPackagesn() {
        return packagesn;
    }

    public void setPackagesn(Integer packagesn) {
        this.packagesn = packagesn;
    }

    public String getPackageX() {
        return packageX;
    }

    public void setPackageX(String packageX) {
        this.packageX = packageX;
    }

    public Integer getAmount_usage() {
        return amount_usage;
    }

    public void setAmount_usage(Integer amount_usage) {
        this.amount_usage = amount_usage;
    }

    public Integer getDone_usage() {
        return done_usage;
    }

    public void setDone_usage(Integer done_usage) {
        this.done_usage = done_usage;
    }

    public Integer getSurplus_usage() {
        return surplus_usage;
    }

    public void setSurplus_usage(Integer surplus_usage) {
        this.surplus_usage = surplus_usage;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public Integer getRealname_status() {
        return realname_status;
    }

    public void setRealname_status(Integer realname_status) {
        this.realname_status = realname_status;
    }
	
}
