package org.dragonnova.business.model.sim;

/**
 * 物联网卡套餐类
 * @author cheng
 *
 */
public class SimPackage {
	
	private Integer packagesn;
    private String package_name;
    private Integer period_usage;
    private Integer period_day;

    public Integer getPackagesn() {
        return packagesn;
    }

    public void setPackagesn(Integer packagesn) {
        this.packagesn = packagesn;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public Integer getPeriod_usage() {
        return period_usage;
    }

    public void setPeriod_usage(Integer period_usage) {
        this.period_usage = period_usage;
    }

    public Integer getPeriod_day() {
        return period_day;
    }

    public void setPeriod_day(Integer period_day) {
        this.period_day = period_day;
    }
}
