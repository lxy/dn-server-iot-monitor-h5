package org.dragonnova.business.model;

import javax.validation.constraints.NotNull;

public class DeviceConfig {
	private Byte id;
	@NotNull(message = "{config.wakeup_time.null}")
	private Byte wakeup_time;
	@NotNull(message = "{config.dat_report_time.null}")
	private Byte dat_report_time;
	@NotNull(message = "{config.gps_report_time.null}")
	private Byte gps_report_time;
	@NotNull(message = "{config.service_status.null}")
	private Byte service_status;
	@NotNull(message = "{config.alarm_time.null}")
	private Byte alarm_time;
	@NotNull(message = "{config.alarm_time1.null}")
	private Byte alarm_time1;
	@NotNull(message = "{config.alarm_time2.null}")
	private Byte alarm_time2;
	private String update_time;
	private String insert_time;
	@NotNull(message = "{config.boxId.null}")
	private String boxId;
	private int sign;
	
	public DeviceConfig() {
		super();
	}

	public DeviceConfig(Byte wakeup_time, Byte dat_report_time, Byte gps_report_time, Byte service_status,
			Byte alarm_time, Byte alarm_time1, Byte alarm_time2, String update_time, String boxId) {
		super();
		this.wakeup_time = wakeup_time;
		this.dat_report_time = dat_report_time;
		this.gps_report_time = gps_report_time;
		this.service_status = service_status;
		this.alarm_time = alarm_time;
		this.alarm_time1 = alarm_time1;
		this.alarm_time2 = alarm_time2;
		this.update_time = update_time;
		this.boxId = boxId;
	}

	public String getBoxId() {
		return boxId;
	}

	public void setBoxId(String boxId) {
		this.boxId = boxId;
	}

	public Byte getId() {
		return id;
	}

	public void setId(Byte id) {
		this.id = id;
	}

	public Byte getWakeup_time() {
		return wakeup_time;
	}

	public void setWakeup_time(Byte wakeup_time) {
		this.wakeup_time = wakeup_time;
	}

	public Byte getDat_report_time() {
		return dat_report_time;
	}

	public void setDat_report_time(Byte dat_report_time) {
		this.dat_report_time = dat_report_time;
	}

	public Byte getGps_report_time() {
		return gps_report_time;
	}

	public void setGps_report_time(Byte gps_report_time) {
		this.gps_report_time = gps_report_time;
	}

	public Byte getService_status() {
		return service_status;
	}

	public void setService_status(Byte service_status) {
		this.service_status = service_status;
	}

	public Byte getAlarm_time() {
		return alarm_time;
	}

	public void setAlarm_time(Byte alarm_time) {
		this.alarm_time = alarm_time;
	}

	public Byte getAlarm_time1() {
		return alarm_time1;
	}

	public void setAlarm_time1(Byte alarm_time1) {
		this.alarm_time1 = alarm_time1;
	}

	public Byte getAlarm_time2() {
		return alarm_time2;
	}

	public void setAlarm_time2(Byte alarm_time2) {
		this.alarm_time2 = alarm_time2;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getInsert_time() {
		return insert_time;
	}

	public void setInsert_time(String insert_time) {
		this.insert_time = insert_time;
	}

	public int getSign() {
		return sign;
	}

	public void setSign(int sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "TagConfig [id=" + id + ", wakeup_time=" + wakeup_time + ", dat_report_time=" + dat_report_time
				+ ", gps_report_time=" + gps_report_time + ", service_status=" + service_status + ", alarm_time="
				+ alarm_time + ", alarm_time1=" + alarm_time1 + ", alarm_time2=" + alarm_time2 + ", update_time="
				+ update_time + ", insert_time=" + insert_time + "]";
	}

}
