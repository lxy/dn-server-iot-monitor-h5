package org.dragonnova.business.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Report {
	private String time;
	private Float humMean;
	private Float tempMean;
	private Float batteryMean;
	private boolean isAlertHum;
	private boolean isAlertTemp;
	private int humCount;
	private int tempCount;
	private int batteryCount;

	
	public Report() {
		super();
	}

	public Report(String time, Float humMean, Float tempMean, Float batteryMean, boolean isAlertHum,
			boolean isAlertTemp, int humCount, int tempCount, int batteryCount) {
		super();
		this.time = time;
		this.humMean = humMean;
		this.tempMean = tempMean;
		this.batteryMean = batteryMean;
		this.isAlertHum = isAlertHum;
		this.isAlertTemp = isAlertTemp;
		this.humCount = humCount;
		this.tempCount = tempCount;
		this.batteryCount = batteryCount;
	}

	public Float getBatteryMean() {
		return batteryMean;
	}

	public void setBatteryMean(Float batteryMean) {
		this.batteryMean = batteryMean;
	}

	public int getBatteryCount() {
		return batteryCount;
	}

	public void setBatteryCount(int batteryCount) {
		this.batteryCount = batteryCount;
	}

	public String getTime() {
		return time;
	}

	public void setTime(long time) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(time);
		this.time = simpleDateFormat.format(date);
	}

	public Float getHumMean() {
		return humMean;
	}

	public void setHumMean(Float humMean) {
		this.humMean = humMean;
	}

	public Float getTempMean() {
		return tempMean;
	}

	public void setTempMean(Float tempMean) {
		this.tempMean = tempMean;
	}

	public int getHumCount() {
		return humCount;
	}

	public void setHumCount(int humCount) {
		this.humCount = humCount;
	}

	public int getTempCount() {
		return tempCount;
	}

	public void setTempCount(int tempCount) {
		this.tempCount = tempCount;
	}

	public boolean isAlertHum() {
		return isAlertHum;
	}

	public void setAlertHum(boolean isAlertHum) {
		this.isAlertHum = isAlertHum;
	}

	public boolean isAlertTemp() {
		return isAlertTemp;
	}

	public void setAlertTemp(boolean isAlertTemp) {
		this.isAlertTemp = isAlertTemp;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Report [time=" + time + ", humMean=" + humMean + ", tempMean=" + tempMean + ", isAlertHum=" + isAlertHum
				+ ", isAlerttemp=" + isAlertTemp + ", humCount=" + humCount + ", tempCount=" + tempCount + "]";
	}

}
