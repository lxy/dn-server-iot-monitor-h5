package org.dragonnova.business.model;

public class Threshold {
	private Integer id;
	private String tempAlert;
	private String humAlert;
	private Integer batteryAlert;
	private String tagSn;
	private String updatetime;
	private Integer createtime;
	private Integer sign;
	private String remark;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTempAlert() {
		return tempAlert;
	}

	public void setTempAlert(String tempAlert) {
		this.tempAlert = tempAlert;
	}

	public String getHumAlert() {
		return humAlert;
	}

	public void setHumAlert(String humAlert) {
		this.humAlert = humAlert;
	}

	public Integer getBatteryAlert() {
		return batteryAlert;
	}

	public void setBatteryAlert(Integer batteryAlert) {
		this.batteryAlert = batteryAlert;
	}

	public String getTagSn() {
		return tagSn;
	}

	public void setTagSn(String tagSn) {
		this.tagSn = tagSn;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public Integer getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Integer createtime) {
		this.createtime = createtime;
	}

	public Integer getSign() {
		return sign;
	}

	public void setSign(Integer sign) {
		this.sign = sign;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "SceneThreshold [id=" + id + ", tempAlert=" + tempAlert + ", humAlert=" + humAlert + ", batteryAlert="
				+ batteryAlert + ", tagSn=" + tagSn + ", updatetime=" + updatetime + ", createtime=" + createtime
				+ ", sign=" + sign + ", remark=" + remark + "]";
	}

}
