package org.dragonnova.business.model;

import java.io.Serializable;
import java.util.List;

/**
 * 标签报表
 */
public class TagReports implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tagId;
	private String title; // tag title
	private String sn;
	int humTotal = 0, tempTotal = 0;// 总数量
	float humAllMean = 0, tempAllMean = 0;// 总平均值
	private String startTime;
	private String endTime;
	private String days;
	private String unit;
	private List<Report> reports;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public List<Report> getReports() {
		return reports;
	}

	public void setReports(List<Report> reports) {
		this.reports = reports;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public int getHumTotal() {
		return humTotal;
	}

	public void setHumTotal(int humTotal) {
		this.humTotal = humTotal;
	}

	public int getTempTotal() {
		return tempTotal;
	}

	public void setTempTotal(int tempTotal) {
		this.tempTotal = tempTotal;
	}

	public float getHumAllMean() {
		return humAllMean;
	}

	public void setHumAllMean(float humAllMean) {
		this.humAllMean = humAllMean;
	}

	public float getTempAllMean() {
		return tempAllMean;
	}

	public void setTempAllMean(float tempAllMean) {
		this.tempAllMean = tempAllMean;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime =endTime;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "TagReports [title=" + title + ", sn=" + sn + ", tagId=" + tagId + ", humTotal=" + humTotal
				+ ", tempTotal=" + tempTotal + ", humAllMean=" + humAllMean + ", tempAllMean=" + tempAllMean
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", reports=" + reports + "]";
	}

}
