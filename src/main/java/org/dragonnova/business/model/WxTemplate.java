package org.dragonnova.business.model;

/**
 * 微信模版消息内容
 *
 */
public class WxTemplate {
	private String touser;
	private String template_id;
	private String url;
	private WxTemplateData data;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WxTemplateData getData() {
		return data;
	}

	public void setData(WxTemplateData data) {
		this.data = data;
	}

}
