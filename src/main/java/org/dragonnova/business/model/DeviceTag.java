package org.dragonnova.business.model;

import java.io.Serializable;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.dragonnova.business.model.DeviceTag.First;
import org.dragonnova.business.model.DeviceTag.Second;

import com.base.pub.util.Base64Util;

/**
 * 设备标签entity
 */
@GroupSequence({First.class, Second.class, DeviceTag.class})  
public class DeviceTag implements Serializable {
	private static final long serialVersionUID = 1L;
	@NotNull (message = "{tag.id.null}", groups = { First.class })
	private String id;

	@NotNull(message = "{tag.title.null}", groups = { Second.class })
	@Size(min=1,max=20,message = "{tag.title.length}", groups = { Second.class })
	private String title;
	@NotNull(message = "{tag.sn.null}", groups = { Second.class })
	private String sn;
	private Integer zone;
	private String zoneName;
	private Integer creatorUid;
	private Integer tagType;
	private String updatetime;
	private String createtime;
	private String serviceId;
	private String serviceSn;
	private String remark;
	private String sign;
	@NotNull (message = "{tag.tempAlert.null}", groups = { Second.class })
	private String tempAlert;
	@NotNull (message = "{tag.humAlert.null}", groups = { Second.class })
	private String humAlert;
	private Float batteryAlert;
	private TagValue tagValue;


	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = Base64Util.getBase64(id);
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = Base64Util.getBase64(serviceId);
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getZone() {
		return zone;
	}

	public void setZone(Integer zone) {
		this.zone = zone;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Integer getTagType() {
		return tagType;
	}

	public void setTagType(Integer tagType) {
		this.tagType = tagType;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public Integer getCreatorUid() {
		return creatorUid;
	}

	public void setCreatorUid(Integer creatorUid) {
		this.creatorUid = creatorUid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public TagValue getTagValue() {
		return tagValue;
	}

	public void setTagValue(TagValue tagValue) {
		this.tagValue = tagValue;
	}

	public String[] getTempAlert() {
		return tempAlert == null ? null : tempAlert.split(",");
	}

	public String getTempAlert1() {
		return tempAlert;
	}

	public void setTempAlert(String tempAlert) {
		this.tempAlert = tempAlert;
	}

	public String[] getHumAlert() {
		return humAlert == null ? null : humAlert.split(",");
	}

	public String getHumAlert1() {
		return humAlert;
	}

	public void setHumAlert(String humAlert) {
		this.humAlert = humAlert;
	}

	public Float getBatteryAlert() {
		return batteryAlert;
	}

	public void setBatteryAlert(Float batteryAlert) {
		this.batteryAlert = batteryAlert;
	}

	@Override
	public String toString() {
		return "DeviceTag [id=" + id + ", title=" + title + ", sn=" + sn + ", zone=" + zone + ", creatorUid="
				+ creatorUid + ", tagType=" + tagType + ", updatetime=" + updatetime + ", createtime=" + createtime
				+ ", serviceId=" + serviceId + ", remark=" + remark + ", sign=" + sign + ", tagValue=" + tagValue + "]";
	}
	public interface First {
	}

	public interface Second {
	}

	public String getServiceSn() {
		return serviceSn;
	}

	public void setServiceSn(String serviceSn) {
		this.serviceSn = serviceSn;
	}

	/**
	 * 解码
	 * 
	 * @param id
	 */
	public void setIdFromBase64(String id) {
		this.id = Base64Util.getFromBase64(Base64Util.getFromBase64(id));
	}
	public String getIdFromBase64() {
		return Base64Util.getBase64(Base64Util.getBase64(id));
	}

}
