package org.dragonnova.business.model;

import org.springframework.util.DigestUtils;

import com.base.pub.util.StringUtils;

/**
 * 
 */
public class IotSim {

	private String userId;
	private String key;
	private String num;
	private String num_type;
	private int timestamp;
	private String sign;

	public IotSim(String userId, String key, String num, String num_type) {
		super();
		this.userId = userId;
		this.key = key;
		this.num = num;
		this.num_type = num_type;
		this.timestamp = (int)(System.currentTimeMillis()/1000);
		String str = StringUtils.concat(userId, key, String.valueOf(timestamp));
		this.sign = DigestUtils.md5DigestAsHex(str.getBytes()).toUpperCase();
		System.out.println(sign);
	}


	public String getUserId() {
		return userId;
	}


	public String getKey() {
		return key;
	}


	public String getNum() {
		return num;
	}


	public String getNum_type() {
		return num_type;
	}


	public int getTimestamp() {
		return timestamp;
	}


	public String getSign() {
		return sign;
	}


}
