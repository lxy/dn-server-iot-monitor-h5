package org.dragonnova.business.model;

import java.io.Serializable;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.dragonnova.business.model.User.First;
import org.dragonnova.business.model.User.Second;
import org.dragonnova.business.validator.EmailType;
import org.dragonnova.business.validator.MobileType;

/**
 * 用户表
 */
@GroupSequence({ First.class, Second.class, User.class })
public class User implements Serializable {

	public static final long serialVersionUID = -8487729891897203929L;

	@NotNull(message = "{user.id.null}", groups = { First.class })
	private Integer id;
	private String sessionId;
	@NotNull(message = "{user.name.null}", groups = { Second.class })
	@Size(min = 2, max = 10, message = "{user.name.length}", groups = { Second.class })
	@Pattern(regexp = "[a-zA-Z0-9_]{2,10}", message = "{user.username.illegal}")
	private String username;

	@NotNull(message = "{user.password.null}", groups = { Second.class })
	@Size(min = 6, max = 28, message = "{user.password.length}", groups = { Second.class })
	private String password;
	private String wxopenid;
	private String type;
	private String expiredtime;
	@NotNull(message = "{user.truename.null}", groups = { Second.class })
	private String truename;
	private String idcard;
	private String nickname;
	@EmailType(groups = { Second.class })
	private String email;
	@NotNull(message = "{user.mobile.null}", groups = { Second.class })
	@MobileType(groups = { Second.class })
	private String mobile;
	private String login_ip;
	private String login_time;
	private String login_count;
	private String cityid;
	private String remark;
	private String groupid;
	private String create_time;
	private int gid;

	public Integer getId() {
		return id;// ;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getWxopenid() {
		return wxopenid;
	}

	public void setWxopenid(String wxopenid) {
		this.wxopenid = wxopenid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExpiredtime() {
		return expiredtime;
	}

	public void setExpiredtime(String expiredtime) {
		this.expiredtime = expiredtime;
	}

	public String getTruename() {
		return truename;
	}

	public void setTruename(String truename) {
		this.truename = truename;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getLogin_ip() {
		return login_ip;
	}

	public void setLogin_ip(String login_ip) {
		this.login_ip = login_ip;
	}

	public String getLogin_time() {
		return login_time;
	}

	public void setLogin_time(String login_time) {
		this.login_time = login_time;
	}

	public String getLogin_count() {
		return login_count;
	}

	public void setLogin_count(String login_count) {
		this.login_count = login_count;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", sessionId=" + sessionId + ", username=" + username + ", password=" + password
				+ ", wxopenid=" + wxopenid + ", type=" + type + ", expiredtime=" + expiredtime + ", truename="
				+ truename + ", idcard=" + idcard + ", nickname=" + nickname + ", email=" + email + ", mobile=" + mobile
				+ ", login_ip=" + login_ip + ", login_time=" + login_time + ", login_count=" + login_count + ", cityid="
				+ cityid + ", remark=" + remark + ", groupid=" + groupid + ", create_time=" + create_time + ", gid="
				+ gid + "]";
	}

	public interface First {
	}

	public interface Second {
	}

}
