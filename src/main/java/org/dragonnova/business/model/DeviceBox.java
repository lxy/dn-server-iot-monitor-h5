package org.dragonnova.business.model;

import com.base.pub.util.Base64Util;

public class DeviceBox {
	private String id;
	private String sn;
	private Integer type;
	private Float battery;
	private String title;
	private String updatetime;
	private String createtime;
	private String owner;
	private String status;
	private String sign;
	private String creatorId;
	private String supply;
	private TagValue tagValue;

	public TagValue getTagValue() {
		return tagValue;
	}

	public void setTagValue(TagValue tagValue) {
		this.tagValue = tagValue;
	}
	
	/**
	 * 解码
	 * 
	 * @param id
	 */
	public void setIdFromBase64(String id) {
		this.id = Base64Util.getFromBase64(Base64Util.getFromBase64(id));
	}
	public String getId() {
		return id;// ;
	}

	public void setId(String id) {
		this.id = Base64Util.getBase64(String.valueOf(id));
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Float getBattery() {
		return battery;
	}

	public void setBattery(Float battery) {
		this.battery = battery;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getSupply() {
		return supply;
	}

	public void setSupply(String supply) {
		this.supply = supply;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "DeviceBox [id=" + id + ", sn=" + sn + ", type=" + type + ", battery=" + battery + ", title=" + title
				+ ", updatetime=" + updatetime + ", createtime=" + createtime + ", status=" + status + ", sign=" + sign
				+ "]";
	}

}
