package org.dragonnova.business.model;

public class TagValue {

	private String tagSn; //comment '标签或网关sn外键'
	private Float temp;
	private Float hum;
	private Float battery;
	private Double latitude;
	private Double longitude;
	private String updatetime;
	private String inserttime;
	private String tagType;
	private String sign;
	private int status;//0,1,2,3

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getInserttime() {
		return inserttime;
	}

	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}

	public String getTagType() {
		return tagType;
	}

	public void setTagType(String tagType) {
		this.tagType = tagType;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTagSn() {
		return tagSn;
	}

	public void setTagSn(String tagSn) {
		this.tagSn = tagSn;
	}

	public Float getTemp() {
		return temp;
	}

	public void setTemp(Float temp) {
		this.temp = temp;
	}

	public Float getHum() {
		return hum;
	}

	public void setHum(Float hum) {
		this.hum = hum;
	}

	public Float getBattery() {
		return battery;
	}

	public void setBattery(Float battery) {
		this.battery = battery;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TagValue [tagSn=" + tagSn + ", temp=" + temp + ", hum=" + hum + ", battery=" + battery + ", latitude="
				+ latitude + ", longitude=" + longitude + ", updatetime=" + updatetime + ", inserttime=" + inserttime
				+ ", tagType=" + tagType + ", sign=" + sign + ", status=" + status + "]";
	}

}
