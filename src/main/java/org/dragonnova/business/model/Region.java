package org.dragonnova.business.model;

import java.math.BigInteger;
/**
 * 用户区域表
 */
public class Region {

	private BigInteger id;
	private int uid;
	private Integer type;
	private String name;
	private String updatetime;
	private String createtime;
	private String sign;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "Region [id=" + id + ", uid=" + uid + ", type=" + type + ", name=" + name + ", updatetime=" + updatetime
				+ ", createtime=" + createtime + ", sign=" + sign + "]";
	}

}
