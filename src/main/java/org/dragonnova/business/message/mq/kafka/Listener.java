package org.dragonnova.business.message.mq.kafka;

import java.util.Set;

public interface Listener {

	Set<String> getTopics();

}
