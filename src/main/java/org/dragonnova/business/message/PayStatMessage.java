package org.dragonnova.business.message;

import java.io.Serializable;

public class PayStatMessage extends Message implements Serializable {

	/**
	 * @description:
	 * @author songxy DateTime 2017年6月2日
	 * 
	 */

	private static final long serialVersionUID = 2715391607194631802L;
	/**
	 * 设备唯一号
	 */
	private String tagSn;
	/**
	 * 未缴费
	 */
	private Float owedTotal;
	/**
	 * 未缴费笔数
	 */
	private Integer unPayNum;

	/**
	 * 已缴费
	 */
	private Float paid;

	/**
	 * 钱包剩余
	 */
	private Float walletMony;
	/**
	 * 单位
	 */
	private Character unit;

	/**
	 * @return get unPayNum
	 */

	public Integer getUnPayNum() {
		return unPayNum;
	}

	/**
	 * @param unPayNum
	 *            set unPayNum
	 */

	public void setUnPayNum(Integer unPayNum) {
		this.unPayNum = unPayNum;
	}

	/**
	 * @return get paid
	 */

	public Float getPaid() {
		return paid;
	}

	/**
	 * @param paid
	 *            set paid
	 */

	public void setPaid(Float paid) {
		this.paid = paid;
	}

	/**
	 * @return get walletMony
	 */

	public Float getWalletMony() {
		return walletMony;
	}

	/**
	 * @param walletMony
	 *            set walletMony
	 */

	public void setWalletMony(Float walletMony) {
		this.walletMony = walletMony;
	}

	/**
	 * @return get unit
	 */

	public Character getUnit() {
		return unit;
	}

	/**
	 * @param unit
	 *            set unit
	 */

	public void setUnit(Character unit) {
		this.unit = unit;
	}

	/**
	 * @return get owedTotal
	 */

	public Float getOwedTotal() {
		return owedTotal;
	}

	/**
	 * @param owedTotal
	 *            set owedTotal
	 */

	public void setOwedTotal(Float owedTotal) {
		this.owedTotal = owedTotal;
	}

	/**
	 * @return get tagSn
	 */

	public String getTagSn() {
		return tagSn;
	}

	/**
	 * @param tagSn
	 *            set tagSn
	 */

	public void setTagSn(String tagSn) {
		this.tagSn = tagSn;
	}
}
