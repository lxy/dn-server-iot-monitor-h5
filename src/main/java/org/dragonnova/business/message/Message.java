package org.dragonnova.business.message;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public abstract class Message {

	private String id;

	private Date createTime;

	/**
	 * @return get id
	 */

	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            set id
	 */

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return get createTime
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            set createTime
	 */

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
