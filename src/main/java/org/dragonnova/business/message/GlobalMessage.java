package org.dragonnova.business.message;

import java.util.Date;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class GlobalMessage {

	private MessageType type;
	private JSONObject alertEvent;
	private Date recTime;
	private Date createTime;
	private String targetSn;

	public GlobalMessage() {
	}

	public static enum MessageType {
		Email(1), TagAlarm(2), FlowStat(3), TagAlarmStat(4), PayStat(5);

		private int num;

		MessageType(int num) {
			this.num = num;
		}

	}

	/**
	 * @return get type
	 */

	public MessageType getType() {
		return type;
	}

	/**
	 * @param type
	 *            set type
	 */

	public void setType(MessageType type) {
		this.type = type;
	}

	/**
	 * @return get recTime
	 */

	public Date getRecTime() {
		return recTime;
	}

	/**
	 * @param recTime
	 *            set recTime
	 */

	public void setRecTime(Date recTime) {
		this.recTime = recTime;
	}

	/**
	 * @return get createTime
	 */

	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            set createTime
	 */

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return get targetSn
	 */

	public String getTargetSn() {
		return targetSn;
	}

	/**
	 * @param targetSn
	 *            set targetSn
	 */

	public void setTargetSn(String targetSn) {
		this.targetSn = targetSn;
	}

	public void putAll(Map<String, ? extends Object> data) {
		if (this.getAlertEvent() == null) {
			this.alertEvent = new JSONObject();
		}
		this.alertEvent.putAll(data);
	}

	public void put(String key, String value) {
		if (this.getAlertEvent() == null) {
			this.alertEvent = new JSONObject();
		}
		this.alertEvent.put(key, value);
	}

	/**
	 * @return get alertEvent
	 */

	public JSONObject getAlertEvent() {
		return alertEvent;
	}

	/**
	 * @param alertEvent
	 *            set alertEvent
	 */

	public void setAlertEvent(JSONObject alertEvent) {
		this.alertEvent = alertEvent;
	}

}
