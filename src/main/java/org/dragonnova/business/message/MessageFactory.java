package org.dragonnova.business.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.dragonnova.business.common.Constants;
import org.dragonnova.business.message.GlobalMessage.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import com.alibaba.fastjson.JSONObject;
import com.base.pub.util.ObjectUtils;

public class MessageFactory implements DisposableBean {
	private final static Logger LOGGER = LoggerFactory
			.getLogger(MessageFactory.class);

	/**
	 * 
	 * @description: 消息转换
	 * @author songxy DateTime 2017年6月2日 上午11:42:59
	 * @param textJson
	 * @return 统一消息-GlobalMessage
	 */
	public static List<GlobalMessage> transfer(MessageFactory factory,
			String topic, String textJson, long timestamp) {
		MessageAdapter msgAdapter = new MessageAdapter(factory, topic);
		try {
			return factory
					.createMessage(msgAdapter, topic, textJson, timestamp);
		} catch (Exception e) {
			LOGGER.error(
					e.getMessage() == null ? "unkown error" : e.getMessage(), e);
			return Collections.EMPTY_LIST;
		}
	}

	public List<GlobalMessage> createMessage(MessageAdapter adapter,
			String topic, String msgText, long timestamp) throws Exception {
		if (StringUtils.isEmpty(adapter.getTopic())) {
			throw new NullPointerException("topic is empty.");
		}

		List<GlobalMessage> result = new ArrayList<GlobalMessage>();
		if (adapter.getTopic().equals(Constants.TOPIC_NETFLOW)) {
			NetFlowStatMessage oldMsgObj = adapter.parseJson(msgText,
					NetFlowStatMessage.class);
			GlobalMessage temp = new GlobalMessage();
			temp.setType(MessageType.FlowStat);
			JSONObject entry = new JSONObject();
			entry.put("下行流量", Double.toString(oldMsgObj.getDownflow()));
			entry.put("上行流量", Double.toString(oldMsgObj.getUpflow()));
			temp.setCreateTime(oldMsgObj.getCreateTime());
			temp.setRecTime(new Date(timestamp));
			result.add(temp);
		} else if (adapter.getTopic().equals(Constants.TOPIC_ALARM)) {
			TagAlarmMessage oldMsgObj = adapter.parseJson(msgText,
					TagAlarmMessage.class);
			if (oldMsgObj.getAlertEvent().size() > 0) {
				List<GlobalMessage> temp = new ArrayList<GlobalMessage>(
						oldMsgObj.getAlertEvent().size());
				GlobalMessage t = new GlobalMessage();
				t.setType(MessageType.TagAlarm);
				t.setCreateTime(oldMsgObj.getCreateTime());
				JSONObject entry = new JSONObject();
				entry.putAll(oldMsgObj.getAlertEvent());
				t.putAll(oldMsgObj.getAlertEvent());
				t.setRecTime(new Date(timestamp));
				t.setTargetSn(oldMsgObj.getTagSn());
				temp.add(t);
				result.addAll(temp);
			}
		} else if (adapter.getTopic().equals(Constants.TOPIC_PAY)) {
			PayStatMessage oldMsgObj = adapter.parseJson(msgText,
					PayStatMessage.class);
			Map<String, Object> data = ObjectUtils.objectToMap(oldMsgObj);
			GlobalMessage t = new GlobalMessage();
			t.setType(MessageType.PayStat);
			t.setRecTime(new Date(timestamp));
			t.setTargetSn(oldMsgObj.getTagSn());
			t.putAll(data);
			result.add(t);
		}
		return result;
	}

	@Override
	public void destroy() throws Exception {

	}
}
