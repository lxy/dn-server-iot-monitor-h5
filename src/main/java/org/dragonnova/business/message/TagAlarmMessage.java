package org.dragonnova.business.message;

import java.io.Serializable;
import java.util.Map;

public class TagAlarmMessage extends Message implements Serializable {

	/**
	 * @description:
	 * @author songxy DateTime 2017年6月2日
	 * 
	 */

	private static final long serialVersionUID = -7863221678122685464L;

	private Map<String, String> alertEvent;

	private String tagSn;

	/**
	 * @return get tagSn
	 */

	public String getTagSn() {
		return tagSn;
	}

	/**
	 * @param tagSn
	 *            set tagSn
	 */

	public void setTagSn(String tagSn) {
		this.tagSn = tagSn;
	}

	/**
	 * @return get alertEvent
	 */

	public Map<String, String> getAlertEvent() {
		return alertEvent;
	}

	/**
	 * @param alertEvent
	 *            set alertEvent
	 */

	public void setAlertEvent(Map<String, String> alertEvent) {
		this.alertEvent = alertEvent;
	}

}
