package org.dragonnova.business.message;

import com.alibaba.fastjson.JSON;

public class MessageAdapter {

	private MessageFactory messageFactory;
	private String topic;

	public MessageAdapter(MessageFactory factory, String topic) {
		this.messageFactory = factory;
		this.topic = topic;
	}

	public <T> T parseJson(String msgText, Class<T> clazz) {
		return JSON.parseObject(msgText, clazz);
	}

	public String getTopic() {
		return topic;
	}
}
