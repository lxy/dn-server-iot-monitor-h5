package org.dragonnova.business.message;

import java.io.Serializable;

public class NetFlowStatMessage extends Message implements Serializable {

	/**
	 * @description:
	 * @author songxy DateTime 2017年5月26日
	 * 
	 */

	private static final long serialVersionUID = -1013410353078536012L;

	/**
	 * 下行
	 */
	private Double upflow;
	/**
	 * 下行
	 */
	private Double downflow;
	/*
	 * 包大小
	 */
	private int count;

	/**
	 * @return get count
	 */

	public int getCount() {
		return count;
	}

	/**
	 * @param count
	 *            set count
	 */

	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return get upflow
	 */

	public Double getUpflow() {
		return upflow;
	}

	/**
	 * @param upflow
	 *            set upflow
	 */

	public void setUpflow(Double upflow) {
		this.upflow = upflow;
	}

	/**
	 * @return get downflow
	 */

	public Double getDownflow() {
		return downflow;
	}

	/**
	 * @param downflow
	 *            set downflow
	 */

	public void setDownflow(Double downflow) {
		this.downflow = downflow;
	}

	/**
	 * @description:
	 * @author songxy DateTime 2017年5月26日 下午2:15:11
	 * @return
	 */

	@Override
	public String toString() {
		return String.format("StatMessage [upflow=%s, downflow=%s, count=%s]",
				upflow, downflow, count);
	}

}
