package org.dragonnova.business.service;

import java.util.List;

import org.dragonnova.business.dto.DeviceTagDto;
import org.dragonnova.business.dto.PageDto;
import org.dragonnova.business.model.DeviceBox;
import org.dragonnova.business.model.DeviceTag;
import org.dragonnova.business.model.TagReports;
import org.dragonnova.business.model.TagValue;

public interface TagService extends BaseService {
	
	public boolean addDeviceTag(DeviceTag deviceTag) throws Exception;
	
	public boolean updateDeviceTag(DeviceTag deviceTag) throws Exception;

	/**
	 * 查询devicetag ,条件为空时查询所有
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<DeviceTag> findDeviceTag(Integer userId,String searchText,String id,Integer orderType) throws Exception;
	
	public boolean isUsedZoneId(Integer zoneId) throws Exception;

	public List<DeviceBox> findDeviceBox(int userId, String searchText, String id) throws Exception;
	
	public TagReports getReport(Integer userId,String tagId, String startTime, String endTime, Integer days,Integer type) throws Exception;

	public void addTestData(List<TagValue> list);
	
	public boolean delTagById(String sn) throws Exception;
	
	public boolean addTagValue(TagValue tagValue) throws Exception;
	
	public boolean delValueAndThresoldByTagSn(String tagSn) throws Exception;

	public TagValue findNewestValue(String id)throws Exception;

	public boolean updateBox(DeviceBox box)throws Exception;

	public DeviceTag findDeviceTagBySn(String sn)throws Exception;

	public PageDto<List<DeviceTag>> findTagByPage(Integer userId, Integer indexPage, Integer pageSize,Integer orderType, String searchText) throws Exception;
}
