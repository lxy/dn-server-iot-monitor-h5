package org.dragonnova.business.service;

import java.util.List;

import org.dragonnova.business.model.Region;
/**
 * 区域
 */
public interface RegionService extends BaseService {
	
	public boolean addRegion(Region region) throws Exception;
	
	public boolean updateRegion(Region region) throws Exception;

	public List<Region> findRegion(int userId,String searchText,Integer id) throws Exception;
	
	public boolean delRegionById(Integer id,Integer uid) throws Exception;

	public boolean isUsedName(Integer uid,String name )throws Exception;
	
}
