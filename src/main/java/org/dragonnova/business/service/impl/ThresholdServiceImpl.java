package org.dragonnova.business.service.impl;

import java.util.List;

import org.dragonnova.business.dao.ThresholdDao;
import org.dragonnova.business.model.Threshold;
import org.dragonnova.business.service.ThresholdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 阀值
 */
@Service("sceneService")
public class ThresholdServiceImpl extends BaseServiceImpl implements ThresholdService {
	@Autowired
	private ThresholdDao thresholdDao;

	@Override
	public boolean addSecne(Threshold deviceTag) throws Exception {
		return false;
	}

	@Override
	public boolean saveOrUpdateThreshold(Threshold deviceTag) throws Exception {
		return thresholdDao.saveOrUpdateThreshold(deviceTag)>0?true:false;
	}

	@Override
	public List<Threshold> findScene(String tagTitle, String sn) throws Exception {
		return null;
	}

	@Override
	public boolean delScene(String id) throws Exception {
		return false;
	}

}
