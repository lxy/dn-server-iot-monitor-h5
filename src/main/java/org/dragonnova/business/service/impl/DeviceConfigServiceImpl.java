package org.dragonnova.business.service.impl;

import java.util.List;

import org.dragonnova.business.dao.DeviceConfigDao;
import org.dragonnova.business.model.DeviceConfig;
import org.dragonnova.business.service.DeviceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 配置
 */
@Service("deviceConfigService")
public class DeviceConfigServiceImpl extends BaseServiceImpl implements DeviceConfigService {
	@Autowired(required = true)
	private DeviceConfigDao tagConfigDao;
	

	@Override
	public List<DeviceConfig> findTagConfig(String boxId) throws Exception {
		return tagConfigDao.findTagConfig(boxId);
	}


	@Override
	public boolean saveOrUpdateConfig(DeviceConfig config) throws Exception {
		int result = tagConfigDao.saveOrUpdateConfig(config);
		return result>0?true:false;
	}

}
