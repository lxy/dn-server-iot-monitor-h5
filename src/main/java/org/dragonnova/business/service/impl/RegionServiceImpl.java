package org.dragonnova.business.service.impl;

import java.util.List;

import org.dragonnova.business.dao.RegionDao;
import org.dragonnova.business.model.Region;
import org.dragonnova.business.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("regionService")
public class RegionServiceImpl extends BaseServiceImpl implements RegionService {
	@Autowired(required = true)
	private RegionDao ragionDao;

	@Override
	public boolean addRegion(Region region) throws Exception {
		int result =	ragionDao.addRegion(region);
		return result > 0 ? true : false;
	}

	@Override
	public boolean updateRegion(Region region) throws Exception {
		int result =	 ragionDao.updateRegion(region);
		return result > 0 ? true : false;
	}

	@Override
	public List<Region> findRegion(int userId, String searchText, Integer id) throws Exception {
		return	ragionDao.findRegion(userId, searchText, id);
	}

	@Override
	public boolean delRegionById(Integer id,Integer uid) throws Exception {
		int result = ragionDao.delRegionById(id,uid);
		return result > 0 ? true : false;
	}

	@Override
	public boolean isUsedName(Integer uid,String name) throws Exception {
		int result = ragionDao.isUsedName(uid,name);
		return result>0;
	}

}
