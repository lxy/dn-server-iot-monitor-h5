package org.dragonnova.business.service.impl;

import java.util.List;

import org.dragonnova.business.dao.WxDao;
import org.dragonnova.business.model.WxConfig;
import org.dragonnova.business.service.WxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("wxService")
public class WxServiceImpl extends BaseServiceImpl implements WxService{
	@Autowired
	private WxDao wxDao;
	@Override
	public boolean updateWxConfig(WxConfig config) throws Exception {
		return wxDao.updateWxConfig(config)>0?true:false;
	}

	@Override
	public List<WxConfig> findWxConfig(String appId) throws Exception {
		return wxDao.findWxConfig( appId);
	}


}
