package org.dragonnova.business.service;

import java.util.List;

import org.dragonnova.business.model.WxConfig;

public interface WxService extends BaseService {

	public boolean updateWxConfig(WxConfig wxc) throws Exception ;
	public List<WxConfig> findWxConfig(String appId )throws Exception ;

}
