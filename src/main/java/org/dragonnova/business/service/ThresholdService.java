package org.dragonnova.business.service;

import java.util.List;

import org.dragonnova.business.model.Threshold;

public interface ThresholdService extends BaseService {
	
	public boolean addSecne(Threshold deviceTag) throws Exception;

	public boolean saveOrUpdateThreshold(Threshold deviceTag) throws Exception;

	public List<Threshold> findScene(String tagTitle, String sn) throws Exception;

	public boolean delScene(String id) throws Exception;

}
