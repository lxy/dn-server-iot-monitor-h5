package org.dragonnova.business.service;

import java.util.List;

import org.dragonnova.business.model.DeviceConfig;

public interface DeviceConfigService extends BaseService {
	
	public boolean saveOrUpdateConfig(DeviceConfig config) throws Exception;

	public List<DeviceConfig> findTagConfig(String boxId)throws Exception;

}
