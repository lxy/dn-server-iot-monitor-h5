package org.dragonnova.business.service;

import java.util.List;
import java.util.Set;

import org.dragonnova.business.model.User;

public interface UserService extends BaseService {

	public User getUserByName(String userName) throws Exception;

	public void insertUser(User user) throws Exception;

	public boolean updateUser(User user) throws Exception;

	public List<User> findUser(Integer id, String searchText) throws Exception;

	public boolean delUser(Integer id) throws Exception;

	public boolean addUser(User user) throws Exception;

	public Set<User> getLoginUsers() throws Exception;

}
