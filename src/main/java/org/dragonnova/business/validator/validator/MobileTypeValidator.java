package org.dragonnova.business.validator.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.dragonnova.business.util.ValidatorUtils;
import org.dragonnova.business.validator.MobileType;
import org.springframework.util.StringUtils;

/** 
 */
public class MobileTypeValidator implements ConstraintValidator<MobileType, String> {

	@Override
	public void initialize(MobileType constraintAnnotation) {
		// 初始化，得到注解数据
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(value)) {
			return true;
		}
		if (!ValidatorUtils.checkMobileNumber(value)) {
			return false;// 验证失败
		}
		return true;
	}
}