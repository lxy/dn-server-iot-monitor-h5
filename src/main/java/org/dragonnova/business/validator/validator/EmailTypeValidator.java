package org.dragonnova.business.validator.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.dragonnova.business.util.ValidatorUtils;
import org.dragonnova.business.validator.EmailType;
import org.springframework.util.StringUtils;

/** 
 */
public class EmailTypeValidator implements ConstraintValidator<EmailType, String> {

	@Override
	public void initialize(EmailType constraintAnnotation) {
		// 初始化，得到注解数据
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(value)) {
			return true;
		}
		if (!ValidatorUtils.checkEmail(value)) {
			return false;// 验证失败
		}
		return true;
	}
}