package org.dragonnova.business.dto;


/**
 * webSocket 数据通信封装类
 * @author cheng
 *
 * @param <T>
 */
public class WSResponseDto {

	private String desc;
	private int result;
	private Object data;
	
	public WSResponseDto(int result) {
		this.result = result;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String descr) {
		this.desc = descr;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getResult() {
		return result;
	}
	
}
