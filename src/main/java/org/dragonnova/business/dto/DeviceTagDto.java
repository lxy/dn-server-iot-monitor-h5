package org.dragonnova.business.dto;

import javax.validation.constraints.NotNull;

import org.dragonnova.business.model.DeviceTag.Second;

public class DeviceTagDto {
	
	private String id ="";
	private String sn="";
	private String title ="";
	private String zoneName="";
	private String updatetime="";
	private String serviceId ="";
	private String serviceSn ="";
	private String remark="";
	private String sign="" ;
	private String[] tempAlert;
	private String[] humAlert;
	private Float batteryAlert=-1f;
	private TagValueDto tagValue = new TagValueDto();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceSn() {
		return serviceSn;
	}
	public void setServiceSn(String serviceSn) {
		this.serviceSn = serviceSn;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String[] getTempAlert() {
		return tempAlert;
	}

	public void setTempAlert(String[] tempAlert) {
		this.tempAlert = tempAlert;
	}
	public String[] getHumAlert() {
		return humAlert;
	}
	public void setHumAlert(String[] humAlert) {
		this.humAlert = humAlert;
	}
	public Float getBatteryAlert() {
		return batteryAlert;
	}
	public void setBatteryAlert(Float batteryAlert) {
		this.batteryAlert = batteryAlert;
	}
	public TagValueDto getTagValue() {
		return tagValue;
	}
	public void setTagValue(TagValueDto tagValue) {
		this.tagValue = tagValue;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	@Override
	public String toString() {
		return "DeviceTagDto [id=" + id + ", sn=" + sn + ", title=" + title + ", zoneName=" + zoneName + ", updatetime="
				+ updatetime + ", serviceId=" + serviceId + ", serviceSn=" + serviceSn + ", remark=" + remark
				+ ", sign=" + sign + ", batteryAlert=" + batteryAlert + ", tagValue=" + tagValue + "]";
	}
	
}
