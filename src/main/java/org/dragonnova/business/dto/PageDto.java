package org.dragonnova.business.dto;

import java.io.Serializable;
import java.util.List;

import javassist.expr.NewArray;

public class PageDto<T> implements Serializable {

	private static final long serialVersionUID = 1893318875364614768L;

	//当前页面
	private Integer indexPage = 0;
	//页面元素数量
	private Integer pageSize = 5;
	//总数
	private Long totalCount;
	//总页数
	private Integer totalPage;
	//排序
	private Integer orderType;
	//搜索条件
	private String searchText;
	//页面元素
	private T list;

	public PageDto() {

	}

	public PageDto(Integer indexPage, Long totalCount, T list) {
		super();
		this.indexPage = indexPage;
		this.totalCount = totalCount;
		this.list = list;
	}

	public Integer getIndexPage() {
		return indexPage;
	}

	public void setIndexPage(Integer indexPage) {
		this.indexPage = indexPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public Integer getTotalPage() {
		int num = (int) (totalCount % pageSize);
		totalPage = (int) (totalCount/pageSize);
		if (num>0) {
			totalPage = totalPage+1;
		}
		return totalPage;
	}

	public T getList() {
		return list;
	}

	public void setList(T list) {
		this.list = list;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	@Override
	public String toString() {
		return "PageDto [indexPage=" + indexPage + ", pageSize=" + pageSize + ", totalCount=" + totalCount
				+ ", totalPage=" + totalPage + ", orderType=" + orderType + ", searchText=" + searchText + ", list="
				+ list + "]";
	}
	
}
