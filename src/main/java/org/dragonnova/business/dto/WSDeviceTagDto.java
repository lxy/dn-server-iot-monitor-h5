package org.dragonnova.business.dto;

import java.io.Serializable;

public class WSDeviceTagDto implements Serializable {
	
	private static final long serialVersionUID = -3769194077327112457L;
	
	private String id;
	private TagValueDto tagValue = new TagValueDto();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public TagValueDto getTagValue() {
		return tagValue;
	}
	public void setTagValue(TagValueDto tagValue) {
		this.tagValue = tagValue;
	}
	@Override
	public String toString() {
		return "WSDeviceInfoDto [id=" + id + ", tagValue=" + tagValue + "]";
	}
}
