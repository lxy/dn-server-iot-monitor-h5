package org.dragonnova.business.dto;

import java.io.Serializable;

public class TagValueDto implements Serializable {
	
	private static final long serialVersionUID = 3695331756488766209L;
	
	private String tagSn =""; //comment '标签或网关sn外键'
	private Float temp =  -1f;
	private Float hum =  -1f;
	private Float battery = -1f;
	private String updatetime = "";
	private String tagType ="";
	private String sign ="";
	private int status = -1;//0,1,2,3
	public String getTagSn() {
		return tagSn;
	}
	public void setTagSn(String tagSn) {
		this.tagSn = tagSn;
	}
	public Float getTemp() {
		return temp;
	}
	public void setTemp(Float temp) {
		this.temp = temp;
	}
	public Float getHum() {
		return hum;
	}
	public void setHum(Float hum) {
		this.hum = hum;
	}
	public Float getBattery() {
		return battery;
	}
	public void setBattery(Float battery) {
		this.battery = battery;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	public String getTagType() {
		return tagType;
	}
	public void setTagType(String tagType) {
		this.tagType = tagType;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TagValueDto [tagSn=" + tagSn + ", temp=" + temp + ", hum=" + hum + ", battery=" + battery
				+ ", updatetime=" + updatetime + ", tagType=" + tagType + ", sign=" + sign + ", status=" + status + "]";
	}

}
