package org.dragonnova.business.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.Threshold;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface ThresholdDao {
	
	public List<Threshold> findThreshold(@Param("name") String name, @Param("id") String id)
			throws Exception;

	public int saveOrUpdateThreshold(Threshold deviceTag) throws Exception;

	public int delThreshold(@Param("tagSn") String tagSn) throws Exception;

}
