package org.dragonnova.business.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.User;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface UserDao {
	
	public User loginOnWeb(String userName, String password) throws Exception;
	
	public User findUserByName(String userName) throws Exception;
	
	public void insertUser(User userName) throws Exception;

	public int updateUser(User user)throws Exception;

	public List<User> findUser(@Param("id")Integer id, @Param("searchText")String searchText)throws Exception;

	public int delUser(@Param("id") Integer id)throws Exception;

	public int addUser(User user)throws Exception;

}
