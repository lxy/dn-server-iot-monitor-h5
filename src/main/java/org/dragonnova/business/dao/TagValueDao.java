package org.dragonnova.business.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.Report;
import org.dragonnova.business.model.TagValue;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface TagValueDao {

	public int addTagValue(TagValue tagValue) throws Exception;

	public int delTagValueByTagSn(@Param("tagSn") String tagSn) throws Exception;

	public void addTestData(List<TagValue> list) throws Exception;

	public TagValue findNewestValue(@Param("tagSn")String tagSn) throws Exception;

	public List<Report> findTagValueAvg(@Param("tagSn")String sn, @Param("unit") int unit, @Param("startTime") long startTime,@Param("endTime")  long endTime)  throws Exception;

}
