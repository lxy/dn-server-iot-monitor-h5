package org.dragonnova.business.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.DeviceConfig;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface DeviceConfigDao {
	public int addTagConfig(Map<String,Byte> map) throws Exception;

	public List<DeviceConfig> findTagConfig(@Param("boxId")String boxId)throws Exception;

	public int saveOrUpdateConfig(DeviceConfig config)throws Exception;
	
}
