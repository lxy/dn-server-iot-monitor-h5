package org.dragonnova.business.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.Region;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface RegionDao {

	public int addRegion(Region region) throws Exception;
	
	public int updateRegion(Region region) throws Exception;

	public List<Region> findRegion(@Param("userId")int userId,@Param("searchText")String searchText,@Param("id")Integer id) throws Exception;
	
	public int delRegionById(@Param("id")Integer id,@Param("uid")Integer uid) throws Exception;

	public int isUsedName(@Param("userId")Integer uid, @Param("name")String name)throws Exception;

}
