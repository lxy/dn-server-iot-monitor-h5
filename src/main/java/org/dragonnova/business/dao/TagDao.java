package org.dragonnova.business.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.DeviceBox;
import org.dragonnova.business.model.DeviceTag;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface TagDao {
	public int addDeviceTag(DeviceTag deviceTag) throws Exception;

	/**
	 * 查询设备devicetag,参数为空时查询所有
	 * 
	 * @param title
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public List<DeviceTag> findDeviceTag(@Param("userId") Integer userId, @Param("searchText") String searchText,
			@Param("id") String id,@Param("order") String order) throws Exception;
	
	public List<DeviceBox> findDeviceBox(@Param("userId") int userId, @Param("searchText") String searchText,
			@Param("id") String id) throws Exception;

	public int updateDeviceTag(DeviceTag deviceTag) throws Exception;

	public int delTagById(@Param("id") String sn) throws Exception;
	
	public List<DeviceTag>  findAll(@Param("userId") int userId, @Param("searchText") String searchText,
			@Param("id") Integer id) throws Exception;

	public int updateBox(DeviceBox box)throws Exception;

	public int findCountByZoneId(@Param("zoneId")  Integer zoneId) throws Exception;

	public DeviceTag findDeviceTagBySn(@Param("sn") String sn)throws Exception;

	public Long getTotalCount(@Param("userId")int userId, @Param("searchText")String searchText);

	public List<DeviceTag> findTagByPage(@Param("userId")Integer userId, @Param("start")int start, @Param("size")int size, 
			@Param("searchText")String searchText, @Param("order")String order) throws Exception;

}
