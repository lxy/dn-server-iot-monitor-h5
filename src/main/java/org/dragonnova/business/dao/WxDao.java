package org.dragonnova.business.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.dragonnova.business.model.WxConfig;
import org.springframework.stereotype.Component;

import com.base.pub.persistence.MyBatisDao;

@Component
@MyBatisDao
public interface WxDao  {
	public int updateWxConfig(WxConfig config) throws Exception ;
	public List<WxConfig> findWxConfig(@Param("appId")String appId )throws Exception ;
}
