/*
 * Copyright (c) dragonnova Corporation and other Contributors.
 * 
 *  Project Name: dn-server-iot-monitor-web
 *  Description: 
 *  @author songxy  DateTime 2016年12月30日 上午11:15:20 
 *  @email thinkdata@163.com  
 *  @version 1.0
 */

package org.dragonnova.business.common;

/**
 * Class Name: Constants.java Description: 常量类
 * 
 * @author songxy DateTime 2016年12月30日 上午11:15:20
 * @company winter
 * @email thinkdata@163.com
 * @version 1.0
 */

public class Constants {
	/**
	 * 当前登录用户 login_key
	 */
	public static final String LOGIN_KEY = "login_user_session";
	public static final String USER_ID = "user_id";

	public static final String VALIDATE_SESSION_TOKEN = "session_token_invalidate";

	public static final String USER_CACHE_NAME = "userCache";
	public static final String MQ_CACHE_NAME = "mqCache";
	/*
	 * ==========================================================================
	 */
	public static final String TOPIC_ALARM = "Alarm-STAT";
	public static final String TOPIC_NETFLOW = "NetFlow-STAT";
	public static final String TOPIC_PAY = "Pay-STAT";
	public static final String TOPIC_COUNT = "COUNT-STAT";

	/*
	 * ==========================================================================
	 */

	public static final int EXECUTOR_MAX_THREAD = 3;

	public static final int RESULT_ERROR_INFO = -1;
	public static final int RESULT_NORMAL_INFO = 0;
	public static final int RESULT_WARN_INFO = 1;

	public static final String KAFKA_MESSAGE_GROUP_ID_KEY = "group.id";
	public static final String KAFKA_MESSAGE_GROUP_ID = "_tag_message";

	
	/**
	 * thymeleaf template view
	 */
	public static final String THYMELEAF_TEMPLATE_ERROR= "content :: error";
	public static final String THYMELEAF_TEMPLATE_EMPTY= "content :: empty";
	public static final String THYMELEAF_TEMPLATE_LOGIN= "content :: login";
}
