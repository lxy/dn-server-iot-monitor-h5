package org.dragonnova.business.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.dragonnova.business.common.Constants;
import org.dragonnova.business.model.DeviceConfig;
import org.dragonnova.business.service.DeviceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/tag")
public class TagConfigController extends BaseController {
	@Autowired
	private DeviceConfigService deviceConfigService;

	/**
	 * 删除
	 * 
	 * @param session
	 * @param id
	 *            设备编号
	 * @return
	 */
	@RequestMapping("/addConfig")
	@ResponseBody
	public Map<String, Object> addConfig(HttpSession session, @Validated DeviceConfig config, BindingResult result) {
		if (result.hasErrors()) {
			return bindingResult(result);
		}
		try {
			return packSaveInfo(deviceConfigService.saveOrUpdateConfig(config));
		} catch (Exception e) {
			e.printStackTrace();
			return warnException(e);
		}
	}

	/**
	 * 查询最新devicebox
	 * 
	 * @param boxId
	 *            = box_sn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/findTagConfig")
	public String findTagConfig(HttpSession session, 
			@RequestParam(value = "boxId") String boxId,
			@RequestParam(value = "callback") String callback,
			Model model) {
		try {
			List<DeviceConfig> list = deviceConfigService.findTagConfig(boxId);
			if (list.size()<1) {
				return Constants.THYMELEAF_TEMPLATE_EMPTY;
			}
			model.addAttribute("data", packData(list));
			return callback;
		} catch (Exception e) {
			e.printStackTrace();
			return Constants.THYMELEAF_TEMPLATE_ERROR;
		}
	}

}
