package org.dragonnova.business.web.test;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.dragonnova.business.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Controller
@RequestMapping("/fileup")
public class FileUploadController extends BaseController {

	@RequestMapping("/uploadFile")
	public String uploadFile(@RequestParam(value = "file") CommonsMultipartFile file, Model model) {
		String fileName = file.getName();
		String ogFilename = file.getOriginalFilename();
		String path = new Date().getTime() + file.getOriginalFilename();
		try {
			file.transferTo(new File("F:/Image/" + path));
		} catch (Exception e) {
			e.printStackTrace();
			return "menu :: empty";
		}
		System.out.println("filename:" + fileName + "   ogFileName:" + ogFilename);
		model.addAttribute("url", "/Image/" + path);
		return "/test/image :: showImage";
	}

}
