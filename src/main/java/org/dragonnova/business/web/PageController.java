package org.dragonnova.business.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController extends BaseController {

	@RequestMapping(value = { "/", "/index", "index.html", "/login" })
	public String index() {
		return "login";
	}

	@RequestMapping("/logout")
	public void logout(HttpSession session, HttpServletRequest request,
			HttpServletResponse resp) throws Exception {
		session.invalidate();

		resp.sendRedirect("login.html");
	}

	/**
	 * 曲线图
	 * 
	 * @return
	 */
	@RequestMapping("/diagram")
	public String diagram() {
		return "diagram/diagram";
	}

	/**
	 * 跳转jsp页面
	 * 
	 * @return
	 */
	@RequestMapping("/page")
	public ModelAndView page(HttpServletResponse resp, String jsp) {
		return new ModelAndView(jsp);
	}

	/**
	 * 跳转lable/index页面
	 * 
	 * @return
	 */
	@RequestMapping("/lable")
	public String lableIndex() {
		return "lable/index";
	}

	/**
	 * 跳转lable/details页面
	 * 
	 * @return
	 */
	@RequestMapping("/details")
	public String lableDetails() {
		return "lable/details";
	}

	/**
	 * 跳转lable/search页面
	 * 
	 * @return
	 */
	@RequestMapping("/search")
	public String lableSearch() {
		return "lable/search";
	}

	/**
	 * 跳转添加标签页面
	 * 
	 * @return
	 */
	@RequestMapping("/addLable")
	public String addLable() {
		return "lable/addLable";
	}

	/**
	 * 跳转路由器页面
	 * 
	 * @return
	 */
	@RequestMapping("/router")
	public String router() {
		return "router/router";
	}

	/**
	 * 跳转路由器详情页面
	 * 
	 * @return
	 */
	@RequestMapping("/routerDetails")
	public String routerDetails() {
		return "router/routerDetails";
	}

	/**
	 * 跳转用户详情页面
	 * 
	 * @return
	 */
	@RequestMapping("/user")
	public String user() {
		return "user/user";
	}

	/**
	 * 跳转编辑页面
	 * 
	 * @return
	 */
	@RequestMapping("/editUser")
	public String editUser() {
		return "user/editUser";
	}

	/**
	 * 跳转设置页面
	 * 
	 * @return
	 */
	@RequestMapping("/setting")
	public String settingLable() {
		return "setting/setting";
	}

	/**
	 * 跳转扫码添加编辑页面
	 * 
	 * @return
	 */
	@RequestMapping("/codeAddLable")
	public String codeAddLable() {
		return "lable/codeAddLable";
	}

	/**
	 * 跳转注册页面
	 * 
	 * @return
	 */
	@RequestMapping("/register")
	public String registerUser() {
		return "user/register";
	}

	/**
	 * 跳转支付页面
	 * 
	 * @return
	 */
	@RequestMapping("/pay")
	public String trafficPay() {
		return "pay/pay";
	}

	/**
	 * 跳转menu页面
	 * 
	 * @return
	 */
	@RequestMapping("/menu")
	public String menu() {
		return "menu";
	}

	/**
	 * 跳转card页面
	 * 
	 * @return
	 */
	@RequestMapping("/card")
	public String card() {
		return "router/card";
	}

	/**
	 * 跳转统计页面
	 * 
	 * @return
	 */
	@RequestMapping("/statistics")
	public String statistics() {
		return "statistics/statistics";
		// return "forward:/user/getLoginUsers";
	}

	@RequestMapping("/file")
	public String fileUpload() {
		return "test/fileUpload";
	}

	/**
	 * 跳转警报页面
	 * 
	 * @return
	 */
	@RequestMapping("/warning")
	public String warning() {
		return "warning/warning";
	};

	@RequestMapping("/main")
	public String main() {
		return "main";
	};
}
