package org.dragonnova.business.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.dragonnova.business.model.IotSim;
import org.dragonnova.business.model.sim.IotSimData;
import org.dragonnova.business.model.sim.SimDetail;
import org.dragonnova.business.model.sim.SimPackage;
import org.dragonnova.business.service.IotSimService;
import org.dragonnova.business.util.IotSimUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

@Controller
@RequestMapping("/iotsim")
public class IotSimController extends BaseController {
	@Autowired
	private IotSimService iotSimService;

	/**
	 * 查询卡号是否存在
	 * 
	 * @return
	 */
	@RequestMapping("/checkTerminal")
	@ResponseBody
	public String checkTerminal(HttpSession session, String simType, String num) {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", num, simType);
		String result = IotSimUtils.checkTerminal(iotSim);
		JSONObject json = JSONObject.parseObject(result);
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}
		return json.toJSONString();
	}

	/**
	 * 查询卡号详情
	 * 
	 * @return
	 */
	@RequestMapping("/terminalDetail")
	@ResponseBody
	public String terminalDetail(HttpSession session, String simType, String num) {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", num, simType);
		String result = IotSimUtils.terminalDetail(iotSim);
		JSONObject json = JSONObject.parseObject(result);
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}
		return json.toJSONString();
		// return getDetail();
	}

	/**
	 * 卡号可续费套餐接口
	 * 
	 * @return
	 */
	@RequestMapping("/renewalsList")
	@ResponseBody
	public String renewalsList(HttpSession session, String simType, String num) {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", num, simType);
		String result = IotSimUtils.renewalsPackageList(iotSim);
		JSONObject json = JSONObject.parseObject(result);
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}
		return json.toJSONString();
		// return getPackage();
	}

	/**
	 * 卡续费接口
	 * 
	 * @param packageSn
	 * @return
	 */
	@RequestMapping("/renewals")
	@ResponseBody
	public String renewalsList(HttpSession session, String simType, String num, Integer packageSn) {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", num, simType);
		String result = IotSimUtils.renewals(iotSim, packageSn);
		JSONObject json = JSONObject.parseObject(result);
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}
		return json.toJSONString();
	}

	/**
	 * 获取续费套餐的二维码
	 * 
	 * @param packageSn
	 * @return
	 */
	@RequestMapping("/getQrcode")
	@ResponseBody
	public String getQrcode(HttpSession session, String simType, String num, Integer packageSn) {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", num, simType);
		String result = IotSimUtils.packageQrcode(iotSim, packageSn);
		JSONObject json = JSONObject.parseObject(result);
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}
		return json.toJSONString();
	}

	/**
	 * 获取续费套餐的二维码
	 * 
	 * @param packageSn
	 * @return
	 */
	@RequestMapping("/orderQuery")
	@ResponseBody
	public String orderQuery(HttpSession session, String simType, String num, String ordersn) {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", num, simType);
		String result = IotSimUtils.orderQuery(iotSim, ordersn);
		JSONObject json = JSONObject.parseObject(result);
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}

		return json.toJSONString();
	}

	@Test
	public void test() {
		IotSim iotSim = new IotSim("131436332", "9D8C3CB16223EB5FAAAC9D69D08234AD", "898602B9191750169301", "iccid");

//		 String result = IotSimUtils.terminalDetail(iotSim);
		 String result = IotSimUtils.renewalsPackageList(iotSim);
		// IotSimUtils.orderQuery(iotSim,"201705151428406724281");
//		String result = IotSimUtils.renewals(iotSim, 222122);
		// String result = IotSimUtils.packageQrcode(iotSim,222122);
		JSONObject json = JSONObject.parseObject(result);
		
		int error = json.getInteger("error");
		if (error != 0) {
			json.fluentPut("reason", IotSimUtils.getErrorStr(error));
		}
		System.out.println(json.toJSONString());
	}

}
