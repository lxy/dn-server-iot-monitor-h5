package org.dragonnova.business.cache;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import com.google.common.collect.Maps;

public class EhCacheBean {

	private CacheManager cacheManager;

	@Resource
	public void setCacheManager(CacheManager cm) {
		cacheManager = cm;
	}

	public int getSize(String cacheName) {
		if (existCache(cacheName)) {
			Cache cache = getCache(cacheName);
			return cache.getSize();
		}
		return -1;
	}

	public Object get(String cacheName, String key) {
		Element elem = getCache(cacheName).get(key);
		return elem == null ? null : elem.getObjectValue();
	}

	public Map<String, Object> iterator(String cacheName) {
		Map<String, Object> result = Maps.newLinkedHashMap();
		if (existCache(cacheName)) {
			Cache cache = getCache(cacheName);
			List<String> keys = cache.getKeysWithExpiryCheck();
			for (ListIterator<String> iter = keys.listIterator(); iter
					.hasNext();) {
				String key = iter.next();
				Object value = get(cacheName, key);
				result.put(key, value);
			}
		}
		return result;
	}

	public void remove(String cacheName, String key) {
		if (existCache(cacheName)) {
			Cache cache = getCache(cacheName);
			cache.remove(key);
		}
	}

	public void save(String cacheName, String key, Object value) {
		if (existCache(cacheName)) {
			innerPutKV(cacheName, key, value);
		}
	}

	private boolean existCache(String cacheName) {
		if (cacheManager != null && cacheManager.cacheExists(cacheName)) {
			return true;
		}
		return false;
	}

	private Cache getCache(String cacheName) {
		return cacheManager.getCache(cacheName);
	}

	private void innerPutKV(String cacheName, String key, Object value) {
		Cache cache = getCache(cacheName);
		cache.put(new Element(key, value));
	}

}
