$(function(){
	isJson = function(obj){
		var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
		return isjson;
}
	var pageName = $("body").attr("class");
	if(pageName!=undefined){
		var location = getUrlParam("sign");
		$("#facilityDiv").css("display","inline-block")
		$("#facilityBtn").addClass("rotate");
		console.log(pageName)
		if(pageName=="detailsPage"){
			console.log(123)
			var myArr = ['report','edit','del','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
					window.location.href='diagram.html?sign='+location;
				})
				$("#facBtn_1").click(function(){
					window.location.href='addLable.html?sign='+location+'';
				})
				$("#facBtn_2").click(function(){
			    	swal({
			    		title: "确定删除吗?",
			    		text: "",
			    		type: "warning",
			    		showCancelButton: true,
			    		confirmButtonColor: '#DD6B55',
			    		confirmButtonText: '确定',
			    		cancelButtonText: "取消",
			    		closeOnConfirm: false,
//			    		closeOnCancel: false
			    	},
			    	function(isConfirm){
			    	    if (isConfirm){
			    	 		$.ajax({
			        			url:'tag/delTag.html',
			        			type:'post',
			        			data:{
			        				id:location,
			        			},
			        			success:function(result){
			        				if(isJson(result)==false){
			        	    			result=JSON.parse(result);
			        	    		}; 
			        				/*判断是否删除成功*/
			        				if(result.result==0){
			        					/*删除成功提示成功并跳转到标签列表页面*/
//			        					alert(result.message);
			        					 setTimeout(function(){
			        	    				swal("删除成功！", "点击ok返回首页!", "success");
			        	    				$(".confirm").unbind();
			        	    				 $(".confirm").click(function(){
			            						 window.location.href='lable.html'
			            					 })
			     	             		},0);
			        					
			        				}else{
			        					setTimeout(function(){
			        	             		swal(" 删除失败！",result.message, "error");
			        	             	},0);
			        						 return false;
			        				}
			        			}
			        		})
			    	    }
			    		});
			})
				$("#facBtn_3").click(function(){
				window.location.href='lable.html';
			})
		}
//////////////////////////////////////////////////////////////////////////////////////////////////
		if(pageName=="addEditPage"){
			if (window.location.search==""){
				var myArr = ['save','reset'];
				var myArrNum = myArr.length;
				for(var i =0;i<myArrNum;i++){
					var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
					$("#facilityDiv").append(myBtn);
					$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
				}
				$("#facBtn_0").click(function(){
		            var lableName = $("#addLableName").val();/*区域名称*/
		            var lableZone = $("#addLableRegoin").attr("name");/*获取#addLableRegoin的name，该值为区域ID*/
		       	 	var tempMin = $("#AddTempMin").val();/*温度最低值*/
		       	 	var tempMax = $("#AddTempMax").val();/*温度最高值*/
		       	 	var humMin = $("#AddHumMin").val();/*湿度最低值*/
		       	 	var humMax = $("#AddHumMax").val();/*湿度最高值*/
		       	 	var lableSn = $("#addLableSn").val();/*标签编号*/
		         /*判断获取到的信息是否为空*/
		            if ($("#addLableName").val()==""){
		            	setTimeout(function(){
		            		swal("备注名称为空!");
		            		$("div h2").css("fong-size","20px");
		            	},0)
//		                alert("备注名称为空！");
		                return false;
		            }
		            if ($("#addLableRegoin").val()==""){
		            	setTimeout(function(){
		            		swal("标签区域为空!");
		            		$("div h2").css("fong-size","20px");
		            	},0)
//		                alert("标签区域为空！");
		                return false;
		            }
		            if ($("#addLableSn").val()==""){
		            	setTimeout(function(){
		            		swal("标签编号为空!");
		            		$("div h2").css("fong-size","20px");
		            	},0)
//		                alert("标签编号为空！");
		                return false;
		            }
		            if (tempMin==""||tempMax==""||tempMin>tempMax){
		            	setTimeout(function(){
		            		swal("温度阀值设置不正确!");
		            		$("div h2").css("fong-size","20px");
		            	},0)
//		                alert("温度阀值设置不正确！");
		                return false;
		            }
		            if (humMin==""||humMax==""||humMin>humMax||humMin<0||humMax>100){
		            	setTimeout(function(){
		            		swal("湿度阀值设置不正确!");
		            		$("div h2").css("fong-size","20px");
		            	},0)
//		                alert("湿度阀值设置不正确！");
		                return false;
		            }
		            /*将输入框中信息发送到服务器端保存*/
		                $.ajax({
		                    url:"tag/addDeviceTag.html",
		                    type:"post",
		                    data:{
		                        title:lableName,
		                        sn:lableSn,
		                        zone:lableZone,
		                        tempAlert:tempMin+','+tempMax,
		                        humAlert:humMin+','+humMax,
		                    },
		                    success:function(result){
		                    	if(isJson(result)==false){
		        	    			result=JSON.parse(result);
		        	    		}; 
		                        /*判断是否保存成功*/
		                        if(result.result==0){
		                        	/*保存成功进行提示并跳转到标签列表页*/
		                        	setTimeout(function(){
		        	    				swal("保存成功！", "点击ok返回首页!", "success");
		        	    				$(".confirm").unbind();
		        	    				$(".confirm").click(function(){
		        	    					window.location.href='lable.html'
		        	    				})
		     	             		},0);
//		                        	alert("新增标签成功");
		                        	
		                        }else if(result.result!=0){
//		                        	alert(result.message);
		                        	setTimeout(function(){
		        	             		swal(" 保存失败！",result.message, "error");
		        	             	},0);
		                        }
		                    },
		                    error:function(err){
		                        console.log(err);
		                    }
		                })
				})
				$("#facBtn_1").click(function(){
					$("#addLableRegoin").attr("name","")
		    		$("p input").val("");
				})
				
			}else{
				var myArr = ['save','back'];
				var myArrNum = myArr.length;
				for(var i =0;i<myArrNum;i++){
					var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
					$("#facilityDiv").append(myBtn);
					$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
				}
					$("#facBtn_0").click(function(){
			             var lableName = $("#addLableName").val();/*标签名称*/
			             var lableZone = $("#addLableRegoin").attr("name");/*获取#addLableRegoin的name，该值为区域ID*/
			             var lableSn = $("#addLableSn").val();/*标签编号*/
			        	 var tempMin = $("#AddTempMin").val();/*温度最低值*/
			             var tempMax = $("#AddTempMax").val();/*温度最高值*/
			             var humMin = $("#AddHumMin").val();/*湿度最低值*/
			             var humMax = $("#AddHumMax").val();/*湿度最高值*/
			             /*判断获取到的数据是否为空*/
			            if (lableName==""){
			            	setTimeout(function(){
			            		swal("备注名称为空!");
			            		$("div h2").css("fong-size","20px");
			            	},0);
//			                alert("备注名称为空！");
			                return false;
			            }
			            if (lableZone==""){
			            	setTimeout(function(){
			            		swal("备注区域为空!");
			            		$("div h2").css("fong-size","20px");
			            	},0)
//			                alert("标签区域为空！");
			                return false;
			            }
			            if (lableSn==""){
			            	setTimeout(function(){
			            		swal("备注编号为空!");
			            		$("div h2").css("fong-size","20px");
			            	},0)
//			                alert("标签编号为空！");
			                return false;
			            }
			            if (tempMin==""||tempMax==""||tempMin>tempMax){
			            	setTimeout(function(){
			            		swal("温度阀值设置不正确!");
			            		$("div h2").css("fong-size","20px");
			            	},0);
//			                alert("温度阀值设置不正确！");
			                return false;
			            }
			            if (humMin==""||humMax==""||humMin>humMax||humMin<0||humMax>100){
			            	setTimeout(function(){
			            		swal("湿度阀值设置不正确!");
			            		$("div h2").css("fong-size","20px");
			            	},0)
//			                alert("湿度阀值设置不正确！");
			                return false;
			            }
			            /*将数据发送到服务器端进行保存*/
			                $.ajax({
			                    url:"tag/updateDeviceTag.html",
			                    type:"post",
			                    data:{
			                        id:location,
			                        sn:lableSn,
			                        title:lableName,
			                        zone:lableZone,
			                        tempAlert:tempMin+','+tempMax,
			                        humAlert:humMin+','+humMax,
			                    },
			                    success:function(result){
			                    	if(isJson(result)==false){
			        	    			result=JSON.parse(result);
			        	    		}; 
			                    	/*判断是否保存成功*/
			                        if(result.result==0){
			                        	/*保存成功进行提醒并跳转到该标签详情页*/
			                        	setTimeout(function(){
			        	    				swal("修改成功！", "点击ok返回详情页!", "success");
			        	    				$(".confirm").unbind();
			        	    				$(".confirm").click(function(){
			        	    					window.location.href='details.html?sign='+location+'';
			        	    				})
			     	             		},0);                        	
			                        }else if(result.result!=0){
			                        	setTimeout(function(){
			        	             		swal(" 修改失败！",result.message, "error");
			        	             	},0);
			                        }
			                    },
			                    error:function(err){
			                        console.log(err);
			                    }
			                }) 
					})
					$("#facBtn_1").click(function(){
						window.location.href='details.html?sign='+location;
					})
			}
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if(pageName=="codeAddPage"){
			var myArr = ['save','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
			$("#facBtn_0").click(function(){
	             var lableName = $("#addLableName").val();/*标签名称*/
	             var lableZone = $("#addLableRegoin").attr("name");/*获取#addLableRegoin的name，该值为区域ID*/
	             var lableSn = $("#addLableSn").val();/*标签编号*/
	        	 var tempMin = $("#AddTempMin").val();/*温度最低值*/
	             var tempMax = $("#AddTempMax").val();/*温度最高值*/
	             var humMin = $("#AddHumMin").val();/*湿度最低值*/
	             var humMax = $("#AddHumMax").val();/*湿度最高值*/
	             /*判断获取到的数据是否为空*/
	            if (lableName==""){
	            	setTimeout(function(){
	            		swal("备注名称为空!");
	            		$("div h2").css("fong-size","20px");
	            	},0);
//	                alert("备注名称为空！");
	                return false;
	            }
	            if (lableZone==""){
	            	setTimeout(function(){
	            		swal("备注区域为空!");
	            		$("div h2").css("fong-size","20px");
	            	},0)
//	                alert("标签区域为空！");
	                return false;
	            }
	            if (lableSn==""){
	            	setTimeout(function(){
	            		swal("备注编号为空!");
	            		$("div h2").css("fong-size","20px");
	            	},0)
//	                alert("标签编号为空！");
	                return false;
	            }
	            if (tempMin==""||tempMax==""||tempMin>tempMax){
	            	setTimeout(function(){
	            		swal("温度阀值设置不正确!");
	            		$("div h2").css("fong-size","20px");
	            	},0);
//	                alert("温度阀值设置不正确！");
	                return false;
	            }
	            if (humMin==""||humMax==""||humMin>humMax||humMin<0||humMax>100){
	            	setTimeout(function(){
	            		swal("湿度阀值设置不正确!");
	            		$("div h2").css("fong-size","20px");
	            	},0)
//	                alert("湿度阀值设置不正确！");
	                return false;
	            }
	            /*将数据发送到服务器端进行保存*/
	                $.ajax({
	                    url:"tag/addDeviceTag.html",
	                    type:"post",
	                    data:{
	                        sn:lableSn,
	                        title:lableName,
	                        zone:lableZone,
	                        tempAlert:tempMin+','+tempMax,
	                        humAlert:humMin+','+humMax,
	                    },
	                    success:function(result){
	                    	if(isJson(result)==false){
	        	    			result=JSON.parse(result);
	        	    		}; 
	                    	/*判断是否保存成功*/
	                        if(result.result==0){
	                        	/*保存成功进行提醒并跳转到该标签详情页*/
	                        	setTimeout(function(){
	        	    				swal("添加成功！", "点击ok返回!", "success");
	        	    				$(".confirm").unbind();
	        	    				$(".confirm").click(function(){
	        	    					window.location.href='lable.html';
	        	    				})
	     	             		},0);                        	
	                        }else if(result.result!=0){
	                        	setTimeout(function(){
	        	             		swal(" 保存失败！",result.message, "error");
	        	             	},0);
	                        }
	                    },
	                    error:function(err){
	                        console.log(err);
	                    }
	                }) 
			})
			$("#facBtn_1").click(function(){
	    		window.location.href='addLable.html';
			})
		}
		///////////////////////////////////////////////////////////////////
		if(pageName=="routerDetailsPage"){
			var myArr = ['card','setting','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
					window.location.href='card.html?sign='+location;
				})
				$("#facBtn_1").click(function(){
					var roterSn = $(".routerSn").text();
					var routerTitle = $("#routerTitle").text();
					var url = roterSn+"_"+location+"title="+routerTitle;
					url=encodeURI(encodeURI(url))
		        	window.location.href='setting.html?sign='+url;
				})
				$("#facBtn_2").click(function(){
					window.location.href='router.html'
//					window.history.go(-1);
				})
		}
		///////////////////////////////////////////////////////////////////////
		if(pageName=="payPage"){
			var myArr = ['order','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
					
				})
				$("#facBtn_1").click(function(){
					window.location.href = 'card.html?sign='+location;
				})
		}
		/////////////////////////////////////////////////////////////////////
		if(pageName=="settingPage"){
		    var boxId = location.substring(0,location.indexOf("_"))
		    /*获取路由器ID*/
		    var routerId = location.substring(location.indexOf("_")+1,location.indexOf("title"))
		    $("#awakenSn").html(boxId)
			var myArr = ['save','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
			      	var awakenLable = $("#awakenLable").val();
			        var reportData = $("#reportData").val();
			   	 	var reportGPS = $("#reportGPS").val();
			   	 	var serviceState = $(".serviceState input[name='serviceState']:checked").val();
			   	 	var alert_1 = $(".alert_1 input[name='alert_1']:checked").val();
			   	    var alert_2 = $(".alert_2 input[name='alert_2']:checked").val();
//			   	 	var serviceState = $("#serviceState").val();
//			   	 	var alert_1 = $("#alert_1").val();
//			   	 	var alert_2 = $("#alert_2").val();
			   	 	var alertInterval = $("#alertInterval").val();
			   	 	var routerTitleVal = $("#routerTitleInput").val()
					var myData = new Array();
					var myDataName=["标签唤醒周期","数据上报周期","GPS上报周期","服务状态查询","本地报警1","本地报警2","报警间隔"];
					myData.push(awakenLable,reportData,reportGPS,serviceState,alert_1,alert_2,alertInterval)
					for(var i=0;i<7;i++){
						if(myData[i]==""){
							setTimeout(function(){
			            		swal(myDataName[i]+"为空!");
			            		$("div h2").css("fong-size","20px");
			            	},0);
							return false;
						};
					};
					if(routerTitleVal==""){
						setTimeout(function(){
		            		swal("路由备注为空!");
		            		$("div h2").css("fong-size","20px");
		            	},0);
						return false;
					};
					$.ajax({
						/*将备注信息发送到服务器端保存*/
			            url:"tag/updateBox.html",
			            type:"post",
			            data:{
			            	id:routerId,
			            	title:routerTitleVal,
			            },
			            success:function(result){
			            	
			            }
					});
					$.ajax({
						/*将路由配置信息发送到服务器端保存*/
			            url:"tag/addConfig.html",
			            type:"post",
			            data:{
			            	boxId:boxId,
			            	wakeup_time:awakenLable,
			            	dat_report_time:reportData,
			            	gps_report_time:reportGPS,
			            	service_status:serviceState,
			            	alarm_time1:alert_1,
			            	alarm_time2:alert_2,
			            	alarm_time:alertInterval,
			            },
			            success:function(result){
			            	if(isJson(result)==false){
	        	    			result=JSON.parse(result);
	        	    		}; 
			            	if(result.result==0){
			            		setTimeout(function(){
	        	    				swal("保存成功！", "点击ok返回路由详情页!", "success");
	        	    				$(".confirm").unbind();
	        	    				 $(".confirm").click(function(){
	            						 window.location.href='routerDetails.html?sign='+routerId;
	            					 });
	     	             		},0);
			            	}else{
			            		setTimeout(function(){
	        	             		swal(" 保存失败！",result.message, "error");
	        	             	},0);
			            	}
			            	},
			            	error:function(err){
			            		
			            	}
			            });
				})
				$("#facBtn_1").click(function(){
					window.location.href='routerDetails.html?sign='+routerId;
				})
		}
		//////////////////////////////////////////////////////////////////////////
		if(pageName=="editUserPage"){
			var myArr = ['save','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
					var userName = $("#editUsername").val();
					var trueName = $("#editTruename").val();
					var loginName = $("#editUsername").val();
					var userId = $("#editUserVal").attr("name");
					var phone = $("#editUserPhone").val();
					var psw = $("#editUserPsw").val();
					/*判断输入的用户信息是否为空*/
					var pattern = /^1[34578]\d{9}$/;    
					if(psw==""){
						setTimeout(function(){
				    		swal("密码为空!");
				    		$("div h2").css("fong-size","20px")
				    	},0);
//						alert("密码为空！");
						return false;
					};
					$.ajax({/*将新编辑好的用户信息发送到服务器端保存*/
						url:"user/updateUser.html",
						type:"post",
						data:{"id":userId,"password":psw},
					 success:function(result){
							if(isJson(result)==false){
	        	    			result=JSON.parse(result);
	        	    		}; 
						 /*判断是否保存成功*/
						 if(result.result==0){
							 setTimeout(function(){
				 				swal("保存成功！", "点击ok返回用户页!", "success");
				 				$(".confirm").unbind();
				 				 $(".confirm").click(function(){
										 window.location.href='user.html'
									 });
				       		},0);
//							 alert(result.message);
//							 window.location.href='user.html';
						 }else{
								setTimeout(function(){
				             		swal(" 保存失败！",result.message, "error");
				             	},0);
//							 alert(result.message);
						 };
						 }
					 })
				})
				$("#facBtn_1").click(function(){
					window.location.href = 'user.html';
				})
		}
		//////////////////////////////////////////////////////////////////////////
		
		if(pageName=="userPage"){
			var myArr = ['edit'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
					window.location.href='editUser.html';
				})
		}
		//////////////////////////////////////////////////////////////////////////
		if(pageName=="cardPage"){
			var myArr = ['recharge','back'];
			var myArrNum = myArr.length;
			for(var i =0;i<myArrNum;i++){
				var myBtn = '<div id="facBtn_'+i+'"><img src="resources/images/'+myArr[i]+'.png"/></div>';
				$("#facilityDiv").append(myBtn);
				$("#facBtn_"+i+"").css({position:"absolute",bottom:"5px",right:"10px","z-index":"999"})
			}
				$("#facBtn_0").click(function(){
					window.location.href='pay.html?sign='+location;
				})
				$("#facBtn_1").click(function(){
					window.location.href = 'routerDetails.html?sign='+location;
				})
		}
		///////////////////////////////////////////////////////////////////////////
		$("#facilityBtn").click(function(){
			if ($("#facBtn_0").css("right")=="10px"){
				var num = myArrNum;
				for(var k = 0;k<myArrNum;k++){
					if(k==3){
						$("#facBtn_"+k+"").animate({right:num*60+20+"px"},100);
					}else{
						$("#facBtn_"+k+"").animate({right:num*60+20+"px"},100);
					}
                    num=num-1;
                }
			}else {/*if($("#facBtn_0").css("right")=="260px")*/
				for(var j = 0;j<myArrNum;j++){
                    $("#facBtn_"+j+"").animate({right:"10px"},100);
                }
			}
		})

	}else{
		$("#facilityDiv").css("display","none");
	}
})
