/**
 * Created by karim on 2017/1/23.
 */
	isJson = function(obj){
				var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
				return isjson;
		}
var splitString = function(string,sign){
        var str = string;
        var sn = sign;
        var myArr = [];
        if (str.indexOf(sn)!=-1){
            myArr.push(str.substring(0,str.indexOf(sn)));
            str = str.substring(str.indexOf(sn)+1,str.length);
        }
        for (var i=0;i<str.length;i++){
            if (str.indexOf(sn)!=-1){
                myArr.push(str.substring(0,str.indexOf(sn)));
                str = str.substring(str.indexOf(sn)+1,str.length);
            }else if (myArr!=null&&str.indexOf(sn)==-1){
                myArr.push(str.substring(0,str.length));
                return myArr;
            }
        }
        myArr.push(str.substring(0,str.length));
        return myArr;

    }



$(function () {
    	$("#scanQRCode").css("display","none");/*隐藏扫码添加功能*/
    	$("title").text("编辑标签");
    	/*获取编辑标签的ID*/
    	var location = window.location.search;
        location = location.substring(3,location.length-2);
        var myData = [];
        location = decodeURI(location)
        myData = splitString(location,",")
           	$("#addLableName").val(myData[0]);
   			$("#addLableRegoin").attr("name",myData[2]);
   			$("#addLableSn").val(myData[1]);
   			$("#AddTempMin").val(myData[3]);
			$("#AddTempMax").val(myData[4]);
			$("#AddHumMin").val(myData[5]);
			$("#AddHumMax").val(myData[6]);
			$.ajax({/*请求所有区域信息*/
	            url:"region/findRegion.html?id="+myData[2],
	            type:"get",
	            success:function(result){
	            	if(isJson(result)==false){
	        			result=JSON.parse(result);
	        		}; 
	            	$("#addLableRegoin").val(result.data[0].name)
	            }
			})
       /*将重置按钮改为返回标签*/
        $("#resetAddLable").val("取消");
        /*点击返回按钮事件*/
    	$("#resetAddLable").click(function(){
    		/*点击返回跳转至该标签的详情页*/
    		window.location.href='addLable.html';
    	})
        $('#rightMain_title').html('编辑标签');
        /*发送请求获取该标签数据*/
/*        $.ajax({
            url:"tag/findDeviceTag.html",
            type:"post",
            data:{
                id:location,
            },
            success:function(result){
                result=JSON.parse(result);
                将获取到的信息在输入框中显示出来
   			$("#addLableName").val(result.data[0].title);
   			$("#addLableRegoin").val(result.data[0].zoneName);
   			$("#addLableRegoin").attr("name",result.data[0].zone);
   			$("#addLableSn").val(result.data[0].sn);
   			判断阀值是否为空
   			if(result.data[0].tempAlert!=null&&result.data[0].humAlert!=null){
   				阀值不为空，将阀值显示在输入框中
   				$("#AddTempMin").val(result.data[0].tempAlert[0]);
   				$("#AddTempMax").val(result.data[0].tempAlert[1]);
   				$("#AddHumMin").val(result.data[0].humAlert[0]);
   				$("#AddHumMax").val(result.data[0].humAlert[1]);
   			}
            },
            error:function(err){
                console.log(err);
            }
        });*/
 
/*     	$("#backAddLable").click(function(){
     		window.location.href='details.html?'+location+'';
    	});*/
        /*点击保存按钮事件*/
        $("#saveAddLable").click(function(){
        	/*获取输入框中的数据*/
             var lableName = $("#addLableName").val();/*标签名称*/
             var lableZone = $("#addLableRegoin").attr("name");/*获取#addLableRegoin的name，该值为区域ID*/
             var lableSn = $("#addLableSn").val();/*标签编号*/
        	 var tempMin = $("#AddTempMin").val();/*温度最低值*/
             var tempMax = $("#AddTempMax").val();/*温度最高值*/
             var humMin = $("#AddHumMin").val();/*湿度最低值*/
             var humMax = $("#AddHumMax").val();/*湿度最高值*/
             /*判断获取到的数据是否为空*/
            if (lableName==""){
            	setTimeout(function(){
            		swal("备注名称为空!");
            		$("div h2").css("fong-size","20px");
            	},0);
//                alert("备注名称为空！");
                return false;
            }
            if (lableZone==""){
            	setTimeout(function(){
            		swal("备注区域为空!");
            		$("div h2").css("fong-size","20px");
            	},0)
//                alert("标签区域为空！");
                return false;
            }
            if (lableSn==""){
            	setTimeout(function(){
            		swal("备注编号为空!");
            		$("div h2").css("fong-size","20px");
            	},0)
//                alert("标签编号为空！");
                return false;
            }
            if (tempMin==""||tempMax==""||tempMin>tempMax){
            	setTimeout(function(){
            		swal("温度阀值设置不正确!");
            		$("div h2").css("fong-size","20px");
            	},0);
//                alert("温度阀值设置不正确！");
                return false;
            }
            if (humMin==""||humMax==""||humMin>humMax||humMin<0||humMax>100){
            	setTimeout(function(){
            		swal("湿度阀值设置不正确!");
            		$("div h2").css("fong-size","20px");
            	},0)
//                alert("湿度阀值设置不正确！");
                return false;
            }
            /*将数据发送到服务器端进行保存*/
                $.ajax({
                    url:"tag/addDeviceTag.html",
                    type:"post",
                    data:{
                        sn:lableSn,
                        title:lableName,
                        zone:lableZone,
                        tempAlert:tempMin+','+tempMax,
                        humAlert:humMin+','+humMax,
                    },
                    success:function(result){
                    	if(isJson(result)==false){
                			result=JSON.parse(result);
                		}; 
                    	/*判断是否保存成功*/
                        if(result.result==0){
                        	/*保存成功进行提醒并跳转到该标签详情页*/
                        	setTimeout(function(){
        	    				swal("添加成功！", "点击ok返回!", "success");
        	    				$(".confirm").unbind();
        	    				$(".confirm").click(function(){
        	    					window.location.href='lable.html';
        	    				})
     	             		},0);                        	
                        }else if(result.result!=0){
                        	setTimeout(function(){
        	             		swal(" 保存失败！",result.message, "error");
        	             	},0);
                        }
                    },
                    error:function(err){
                        console.log(err);
                    }
                }) 
        })
    
    /*区域输入框获取焦点事件*/
    $("#addLableRegoin").bind("focus",function(){
    	$("#addZoneVal").html("");/*将#addZoneVal中内容清空*/
    	$("#addLableVal").css("display","none");/*将添加编辑框隐藏*/
    	$("#addZoneVal").css("display","block");/*将区域设置内容显示*/
        $.ajax({/*请求所有区域信息*/
            url:"region/findRegion.html",
            type:"get",
            success:function(result){
            	if(isJson(result)==false){
        			result=JSON.parse(result);
        		}; 
            	if(result.result!=0){
					 setTimeout(function(){
           			swal(" 区域获取失败！",result.message, "error");
           		},0);
					 return false;
				 }
            	/*设置区域信息表格*/
                var myTable = '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%"><tr><th colspan=2>区域名</th><th colspan=2>操作</th></tr>'
                for(var i=0;i<result.data.length;i++){
                	myTable=myTable+'<tr><td colspan=2 id="zoneName_'+result.data[i].id+'">'+result.data[i].name+'</td><td id="zoneSel_'+result.data[i].id+'" class="zoneSel" name="'+i+'">√</td><td id="zoneDel_'+result.data[i].id+'" name="'+result.data[i].id+'" class="zoneDel">×</td></tr>';
				 };
                myTable=myTable+'</tbody></table></a>';
                /*配置新增区域DIV*/
                var addZoneDiv = '<div style="text-align:center;margin:5px 0;width:100%;height:30px;box-sizing:border-box;"><input type="text" id="newZone" placeholder="请输入新增区域名称" style="width:70%;box-sizing:border-box;height:30px"><input id="addZoneBtn" type="button" value="新增" style="width:25%;box-sizing:border-box;height:30px"></div>'
                	$("#addZoneVal").append(addZoneDiv);/*将新增区域div插入区域设置框*/
                	$("#addZoneVal").append(myTable);/*将区域信息表格插入区域设置框*/
      				 var myWhite = '<div style="height:100px;width:100%;background:transparent" id="myWhite"></div>'
       					 $("#addZoneVal").append(myWhite);
      				 /*点击选择事件*/
                $(".zoneSel").click(function(){
                	/*获取该选择元素的name*/
                	var selNum = $(this).attr("name")
                	$("#addLableVal").css("display","block");/*将添加编辑框显示*/
                	$("#addZoneVal").css("display","none");/*将区域设置内容隐藏*/
                	$("#addLableRegoin").val(result.data[selNum].name)/*将选中区域名显示在区域输入框*/
                	$("#addLableRegoin").attr("name",result.data[selNum].id)/*将选中区域名ID设置为区域输入框name*/
                });
                /*点击删除区域事件*/
                $(".zoneDel").click(function(){
                	var delNum = $(this).attr("name")
                	delNum =parseInt(delNum);
                	swal({
                		title: "确定删除吗?",
                		text: "",
                		type: "warning",
                		showCancelButton: true,
                		confirmButtonColor: '#DD6B55',
                		confirmButtonText: '确定',
                		cancelButtonText: "取消",
                		closeOnConfirm: false,
//                		closeOnCancel: false
                	},
                	function(isConfirm){
                		if (isConfirm){
                			$.ajax({
                    			/*发送删除区域请求*/
                                url:"region/delRegion.html",
                                type:"post",
                                data:{
                    				id:delNum,
                    			},
                                success:function(result){
                                	if(isJson(result)==false){
                            			result=JSON.parse(result);
                            		}; 
                              	/*判断是否删除成功*/
                              	if(result.result!=0){
                              		setTimeout(function(){
                	             		swal(" 删除失败！",result.message, "error");
                	             	},0);
                						 return false;
//                              		alert(result.message)
                              	}else{
                              		/*删除成功提醒并将该条区域所在的行移除*/
                              		 setTimeout(function(){
                 	    				swal("删除成功！", "", "success");
              	             		},0);
//                                  	alert("删除成功")
                                    $("#zoneDel_"+delNum+"").parent("tr").remove();
                              	}
                                },
                    		})
                			
                		}
                		
                	});
/*                	var delConfirm = confirm("确定删除该区域吗？");
                	if(delConfirm){
                		$.ajax({
                			发送删除区域请求
                            url:"region/delRegion.html",
                            type:"post",
                            data:{
                				id:delNum,
                			},
                            success:function(result){
                          	  result = JSON.parse(result);
                          	判断是否删除成功
                          	if(result.result==-1){
                          		alert(result.message)
                          	}else{
                          		删除成功提醒并将该条区域所在的行移除
                              	alert("删除成功")
                                $("#zoneDel_"+delNum+"").parent("tr").remove();
                          	}
                            },
                		})
                	}*/
                });
                /*点击新增区域按钮事件*/
                $("#addZoneBtn").click(function(){
                	/*获取新增区域输入框中数据*/
                	var addZoneVal = $("#newZone").val();
                	/*判断新增区域是否为空*/
                	if(!addZoneVal){
                    	setTimeout(function(){
                    		swal("新增区域为空!");
                    		$("div h2").css("fong-size","20px")
                    	},0);
//                		alert("新增区域为空！")
                	}else{
                   		$.ajax({/*将新增区域信息发送到服务器端进行保存*/
                            url:"region/addRegion.html",
                            type:"post",
                            data:{
                            	name:addZoneVal,
                			},
                            success:function(res){
                            	if(isJson(res)==false){
                        			res=JSON.parse(res);
                        		}; 
                            	/*判断是否添加成功*/
                            	if(res.result!=0){
//                            		alert("新增失败!"+res.message);
                            		setTimeout(function(){
                	             		swal(" 新增区域失败！",result.message, "error");
                	             	},0);
                            		return false;
                            	}
                            	/*添加成功后提醒并重新获取区域信息，移除之前的表格，重新插入新的表格并对选择、删除时间进行绑定*/
                            	 setTimeout(function(){
             	    				swal("新增区域成功！", "", "success");
          	             		},0);
//                            	alert("新增区域成功")
                            	$.ajax({
                            		url:"region/findRegion.html",
                            		type:"get",
                            		success:function(result){
                            			if(isJson(result)==false){
                            				result=JSON.parse(result);
                            			}; 
                            			var myTable = '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%"><tr><th colspan=2>区域名</th><th colspan=2>操作</th></tr>'
                            				for(var i=0;i<result.data.length;i++){
                            					myTable=myTable+'<tr><td colspan=2 id="zoneName_'+result.data[i].id+'">'+result.data[i].name+'</td><td id="zoneSel_'+result.data[i].id+'" class="zoneSel" name="'+i+'">√</td><td id="zoneDel_'+result.data[i].id+'" name="'+result.data[i].id+'" class="zoneDel">×</td></tr>';
                            				}
                            			myTable=myTable+'</tbody></table></a>';
                            			$("#addZoneVal table").remove();
                            			$("#addZoneVal #myWhite").remove();
                            			$("#addZoneVal").append(myTable);
                       				 var myWhite = '<div style="height:100px;width:100%;background:transparent" id="myWhite"></div>'
                       					 $("#addZoneVal").append(myWhite);
                                        $(".zoneSel").click(function(){ 	
                                        	var selNum = $(this).attr("name")
                                        	$("#addLableVal").css("display","block");
                                        	$("#addZoneVal").css("display","none");
                                        	$("#addLableRegoin").val(result.data[selNum].name);
                                        	$("#addLableRegoin").attr("name",selNum);
                                        })
                            	          $(".zoneDel").click(function(){
                                          	var delNum = $(this).attr("name")
                                        	delNum =parseInt(delNum);
                                        	swal({
                                        		title: "确定删除吗?",
                                        		text: "",
                                        		type: "warning",
                                        		showCancelButton: true,
                                        		confirmButtonColor: '#DD6B55',
                                        		confirmButtonText: '确定',
                                        		cancelButtonText: "取消",
                                        		closeOnConfirm: false,
//                                        		closeOnCancel: false
                                        	},
                                        	function(isConfirm){
                                        		if (isConfirm){
                                        			$.ajax({
                                            			/*发送删除区域请求*/
                                                        url:"region/delRegion.html",
                                                        type:"post",
                                                        data:{
                                            				id:delNum,
                                            			},
                                                        success:function(result){
                                                        	if(isJson(result)==false){
                                                    			result=JSON.parse(result);
                                                    		}; 
                                                      	/*判断是否删除成功*/
                                                      	if(result.result!=0){
                                                      		setTimeout(function(){
                                        	             		swal(" 删除失败！",result.message, "error");
                                        	             	},0);
                                        						 return false;
                                                      	}else{
                                                      		/*删除成功提醒并将该条区域所在的行移除*/
                                                      		 setTimeout(function(){
                                         	    				swal("删除成功！", "", "success");
                                      	             		},0);
                                                            $("#zoneDel_"+delNum+"").parent("tr").remove();
                                                      	}
                                                        },
                                            		})
                                        		}
                                        		
                                        	});
                                          })
            						}
                            	})

                            },
                		
                		})
                	}
                })
            },
            error:function(err){
                console.log(err);
            }
        }) 
    })
})
		
	   function setFocus(e){
	/*将对象滚动到可见范围内*/
		    e.scrollIntoView(false);
		};
	/*输入框获取焦点触发事件*/
    $("input").focus(function () {
    	if(this.id!="AddHumMin"&&this.id!="AddHumMax"){
        	if($(this).parent().nextAll("p").children("input").length!=0){
        		 var myId = $(this).parent().nextAll("p").children("input")[0].id;
            	 myInput = document.getElementById(myId);
        	}else{ 
        		myInput = document.getElementById(this.id);
        	};
        }else{
        	var myId =$(this).parent().nextAll("div").children("input")[0].id;
        	myInput = document.getElementById(myId);
        } ;
        setTop = setInterval('setFocus(myInput)',10);
    });
    $("input").blur(function(){
        clearInterval(setTop);
    });
