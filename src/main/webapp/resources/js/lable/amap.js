$(function (){
	isJson = function(obj){
		var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
		return isjson;
}
	$(".leftNav a:nth-child(4)").css("background","#1a63be");
//	$('#rightMain_title').html('地图信息');
	$('#lableViews',window.parent.document).find("#rightMain_title").html("地图信息");

	var map = new AMap.Map('container', {
		resizeEnable : true,
		zoom : 13,
	});
	map.setMapStyle("normal");
	map.setFeatures([ "road", "bg", "point"]);
	$.ajax({
		url : "tag/findDeviceBoxJson.html",
		type : "get",
		success : function(result) {
			console.log(result)
			result = JSON.parse(result);
			console.log(result)
			if (result.result == 0) {
				setMapMarkers(result);
			} else {
				console.log(result.message);
			}
		},
		error : function(result) {
			console.log(result);
		}
	});
	map.plugin([ "AMap.ToolBar", 'AMap.Scale' ,"AMap.CitySearch"], function() {
		var citysearch = new AMap.CitySearch();
		map.addControl(new AMap.ToolBar());
		map.addControl(new AMap.Scale());
		AMap.event.addListener(citysearch, "complete", function(result) {
            var citybounds;
            if (result && result.city && result.bounds) {
                citybounds = result.bounds;
                map.setBounds(citybounds);
            }
        });
	});
//    map.setLimitBounds(map.getBounds());

	/**
	 * 加载map markers
	 * @param result
	 * @returns
	 */
	function setMapMarkers(result) {
		for (var i = 0; i < result.data.length; i++) {
			var box = result.data[i];
			var tagValue = box.tagValue;
			if (tagValue != null && tagValue.longitude != null) {
				var title = '创辉温湿度监测', content = [];
				content.push('编号:' + box.sn);
				content.push('电量:' + box.battery+"%"+"&nbsp;&nbsp;&nbsp;备注：" + box.title);
				content.push('上报时间：'+box.updatetime.substring(0,box.updatetime.length-2) +"&nbsp;&nbsp;&nbsp;<a href='routerDetails.html?sign="+ box.id + "'>详细信息</a>&nbsp;");
				var info = createInfoWindow(title, content.join("<br/>"));
				map.panTo([tagValue.longitude, tagValue.latitude]);
				var marker = new AMap.Marker({
					position : [ tagValue.longitude, tagValue.latitude ],
					title : box.title,
					icon : 'resources/images/amap_location.png',
					extData:info,
				});
				marker.setMap(map);
				marker.on('click', function() {
					var infoWindow = new AMap.InfoWindow({
						isCustom : true, // 使用自定义窗体
						content : this.getExtData(),
						offset : new AMap.Pixel(16, -45)
					});
					infoWindow.open(map, this.getPosition());
				});
			}
		}
	}

	/**
	 * 	 //构建自定义信息窗体
	 * @param title
	 * @param content
	 * @returns
	 */
	function createInfoWindow(title, content) {
		console.log(132)
		var info = document.createElement("div");
		info.className = "info";
		var top = document.createElement("div");
		var titleD = document.createElement("div");
		var closeX = document.createElement("img");
		top.className = "info-top";
		titleD.innerHTML = title;
		closeX.src = "http://webapi.amap.com/images/close2.gif";
		closeX.onclick = closeInfoWindow;

		top.appendChild(titleD);
		top.appendChild(closeX);
		info.appendChild(top);
		// 定义中部内容
		var middle = document.createElement("div");
		middle.className = "info-middle";
		middle.style.backgroundColor = 'white';
		middle.innerHTML = content;
		info.appendChild(middle);
		// 定义底部内容
		var bottom = document.createElement("div");
		bottom.className = "info-bottom";
		bottom.style.position = 'relative';
		bottom.style.top = '0px';
		bottom.style.margin = '0 auto';
		var sharp = document.createElement("img");
		sharp.src = "http://webapi.amap.com/images/sharp.png";
		bottom.appendChild(sharp);
		info.appendChild(bottom);
		return info;
	}

	//关闭信息窗体
	function closeInfoWindow() {
		map.clearInfoWindow();
	}

});
