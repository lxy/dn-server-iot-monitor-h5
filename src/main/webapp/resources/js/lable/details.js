/**
 * Created by karim on 2017/1/23.
 */


$(function(){
	isJson = function(obj){
		var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
		return isjson;
}
    $("title").text("标签详情页");/*修改网页名称*/
	/*获取该标签的ID*/
    var location = getUrlParam("sign");
//    window.location.search;
//    location = location.substring(1,999999)
    /*发送标签ID请求该标签数据*/
    $.ajax({
	    url:"tag/findDeviceTag.html",
	    data:{
	        id:location,callback:'content::lableDetails'
	    },
	    type:"post",
	    success:function(result){
	        $("#content").html(result) 
	    },
	    error:function(err){
	        console.log(err);
	    }
	})
        /*编辑按钮点击事件*/
    $("#editLable").click(function(){
    	/*跳转至编辑页面*/
    		window.location.href='addLable.html?sign='+location+'';
    })
    /*历史图表按钮点击事件*/
    $("#temp").click(function(){
    	/*点击跳转至历史图标页面*/
    	window.location.href='diagram.html?sign='+location;
    })
    /*删除按钮点击事件*/
    $("#delLable").click(function(){
    	swal({
    		title: "确定删除吗?",
    		text: "",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonColor: '#DD6B55',
    		confirmButtonText: '确定',
    		cancelButtonText: "取消",
    		closeOnConfirm: false,
//    		closeOnCancel: false
    	},
    	function(isConfirm){
    	    if (isConfirm){
    	 		$.ajax({
        			url:'tag/delTag.html',
        			type:'post',
        			data:{
        				id:location,
        			},
        			success:function(result){
        				result=JSON.parse(result);
        				/*判断是否删除成功*/
        				if(result.result==0){
        					/*删除成功提示成功并跳转到标签列表页面*/
//        					alert(result.message);
        					 setTimeout(function(){
        	    				swal("删除成功！", "点击ok返回首页!", "success");
        	    				$(".confirm").unbind();
        	    				 $(".confirm").click(function(){
            						 window.location.href='lable.html'
            					 })
     	             		},0);
        					
        				}else{
        					setTimeout(function(){
        	             		swal(" 删除失败！",result.message, "error");
        	             	},0);
        						 return false;
        				}
        			}
        		})
    	    }
    		});
    	
//    	var delConfirm = confirm("确定删除该标签吗？")
//    	if(delConfirm){
//    		/*确定删除，发送删除请求到服务器端*/
//    		$.ajax({
//    			url:'tag/delTag.html',
//    			type:'post',
//    			data:{
//    				id:location,
//    			},
//    			success:function(result){
//    				result=JSON.parse(result);
//    				/*判断是否删除成功*/
//    				if(result.result==0){
//    					/*删除成功提示成功并跳转到标签列表页面*/
////    					alert(result.message);
//    					 setTimeout(function(){
//    	    				swal("删除成功！", "点击ok返回首页!", "success")
// 	             		},0);
//    					window.location.href='lable.html'
//    				}else{
//    					setTimeout(function(){
//    	             		swal(" 删除失败！",result.message, "error");
//    	             	},0);
//    						 return false;
//    				}
//    			}
//    		})
//    	}
    })
})