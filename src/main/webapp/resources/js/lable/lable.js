/**
 * Created by karim on 2017/1/17.
 */
$(function () {
	 if (window.location.search==""){/*判断是搜索结果页面还是标签列表页*/
		 /*标签列表页*/
		 /*发送请求获取所有标签数据*/
		 $.ajax({
//				url:"tag/findDeviceTag.html",
				url:"tag/findTagByPage.html",
				type:"get",
				data: {indexPage:2,pageSize:20,orderType:null,searchText:null},
				dataType:"json",
			 success:function(result){
				 console.log(result);
//				 result=JSON.parse(result)//解析字符串
				 if(result.result!=0){
					 setTimeout(function(){
             			swal(" 数据获取失败！",result.message, "error");
             		},0);
					 return false;
				 };
				 /*显示标签总数*/
				 var items = result.data.list;
				 $("#lableNum").text(items.length);
				 var options={
					 "id":result.data.currentPage,//显示页码的元素
					 "data":result.data.list,//显示数据
					 "maxshowpageitem":result.data.totalPage,//最多显示的页码个数
					 "pagelistcount":10,//每页显示数据个数
					 "callBack":function(items){
						 var page ="";
						 for(var i=0;i<items.length;i++){
							 /*判断tagValue中的数据是否为空*/
							 if(items[i].tagValue!=null){
								 var updataLenth = items[i].tagValue.updatetime.length-2;
								 var updatatime = items[i].tagValue.updatetime.substring(0,updataLenth);
								 /*判断是否绑定路由ID*/
								 if(items[i].serviceSn!=null){
									 var routerId = items[i].serviceSn;
								 }else{
									 var routerId = '未绑定';
								 };
								 /*生成标签列表信息表格*/

								 var myA='<a id="a_'+items[i].id+'" href="details.html?sign='+items[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+items[i].id+'">'+items[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+items[i].id+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+items[i].id+'">'+items[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+items[i].id+'">'+items[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+items[i].id+'">'+parseInt(items[i].tagValue.hum)+'%</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+items[i].id+'">'+items[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+items[i].id+'">'+items[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+items[i].id+'">上报时间:'+updatatime+'</td></tr>'+
									 '</tbody></table></a>';
//								 $("#lableList").append(myA);/*将生成的每一条标签信息插入显示框*/
								 page += myA;
								 /*判断数据是否为空*/
								 if(items[i].tagValue.temp==null){
									 $("#lableTemp_"+itemss[i].id+"").text("未获取");
								 };
								 if(items[i].tagValue.hum==null){
									 $("#lableHum_"+itemss[i].id+"").text("未获取");
								 };
								 if(items[i].tagValue.battery==null){
									 $("#lablebattery_"+items[i].id+"").text("未获取");
								 };
							 }else{
								 /*tagValue数据为空，生成表格，tagValue内容显示为未获取*/
								 if(items[i].serviceSn!=null){
									 var routerId = items[i].serviceSn;
								 }else{
									 var routerId = '未绑定';
								 };
								 var myA='<a id="a_'+items[i].id+'" href="details.html?sign='+items[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+items[i].id+'">'+items[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+items[i].id+'">'+items[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+items[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+items[i].id+'">未获取</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+items[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+items[i].id+'">'+items[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+items[i].id+'">上报时间:未获取</td></tr>'+
									 '</tbody></table></a>';
								 page += myA;
							 };
						 };
						 $("#lableList").append(page);
						 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>';
						 $("#lableList").append(myWhite);
					 }
				 };
				 page.init(result.data.list.length,1,options);
			 },
			 error:function(err){
				 console.log(err);
			 }
			 });
			 	/*区域排序按钮点击事件*/
			 	$("#sortZone").click(function(){
			 		$("#sortZone").attr("disabled",true);
			 	    $("#sortTime").attr("disabled",true);
			 		/*点击获取该按钮的name*/
			 		var sortZoneType =  parseInt($("#sortZone").attr("name"));
			 		$("#sortTime").val("时间")/*修改时间排序按钮value为时间*/
			 		if(sortZoneType==3){/*判断sortZoneType是否为3*/
			 			/*为3，区域升序，修改区域排序按钮name为4，value为区域↑*/
			 			$("#sortZone").attr("name","4");
			 			$("#sortZone").val("区域↑");
			 		}else{/*不为3，区域降序，修改区域排序按钮name为3，value为区域↓*/
			 			$("#sortZone").attr("name","3");
			 			$("#sortZone").val("区域↓");
			 		};
			 		/*根据选择的排序发送请求获取按照选择方式排序的标签数据*/
			 	     $.ajax({
							url:"tag/findDeviceTag.html",
							type:"post",
							data:{
								orderType:sortZoneType,
				            },
						 success:function(result){
							 result=JSON.parse(result);
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
							 if(result.result!=0){
								 setTimeout(function(){
			             			swal(" 数据获取失败！",result.message, "error");
			             		},0);
								 return false;
							 }
							 $("#lableList a").remove();
							 $("#lableList #myWhite").remove();
							 $("#lableNum").text(result.data.length);
							 for(var i=0;i<result.data.length;i++){
								 if(result.data[i].tagValue!=null){
									 var updataLenth = result.data[i].tagValue.updatetime.length-2;
									 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth);
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };	 
								 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
								 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
								 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
								 '<tr><td>路由ID</td><td id="lableRouter_'+result.data[i].id+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
								 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
								 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
								 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
								 '</tbody></table></a>';
								 $("#lableList").append(myA);
								 if(result.data[i].tagValue.temp==null){
									 $("#lableTemp_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.hum==null){
									 $("#lableHum_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.battery==null){
									 $("#lablebattery_"+result.data[i].id+"").text("未获取");
								 };
								 }else{
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };
									 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
									 '</tbody></table></a>';
									 $("#lableList").append(myA);
								 };
							 };
							 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
							 $("#lableList").append(myWhite);
							 for(var each in tempInterval){
								    clearInterval(tempInterval[each]);
								};
							 for(var each in humpInterval){
								    clearInterval(humpInterval[each]);
								};
						 },
						 error:function(err){
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
						 }
			 	     })
				 })
				 $("#sortTime").click(function(){
			 		var sortTimeType =  parseInt($("#sortTime").attr("name"));
			 		$("#sortZone").val("区域");
					 $("#sortZone").attr("disabled",true);
				 	 $("#sortTime").attr("disabled",true);
			 		if(sortTimeType==1){
			 			$("#sortTime").attr("name","2");
			 			$("#sortTime").val("时间↓");
			 		}else{
			 			$("#sortTime").attr("name","1");
			 			$("#sortTime").val("时间↑");
			 		};
			 	     $.ajax({
							url:"tag/findDeviceTag.html",
							type:"post",
							data:{
								orderType:sortTimeType,
				            },
						 success:function(result){
							 result=JSON.parse(result);
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
							 if(result.result!=0){
								 setTimeout(function(){
			             			swal(" 数据获取失败！",result.message, "error");
			             		},0);
								 return false;
							 }
							 $("#lableList a").remove();
							 $("#lableList #myWhite").remove();
							 $("#lableNum").text(result.data.length);
							 for(var i=0;i<result.data.length;i++){
								 if(result.data[i].tagValue!=null){
									 var updataLenth = result.data[i].tagValue.updatetime.length-2;
									 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth); 
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };	 
								 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
								 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
								 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
								 '<tr><td>路由ID</td><td id="lableRouter_'+result.data[i].id+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
								 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
								 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
								 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
								 '</tbody></table></a>';
								 $("#lableList").append(myA);
								 if(result.data[i].tagValue.temp==null){
									 $("#lableTemp_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.hum==null){
									 $("#lableHum_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.battery==null){
									 $("#lablebattery_"+result.data[i].id+"").text("未获取");
								 };
								 }else{
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };
									 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
									 '</tbody></table></a>';
									 $("#lableList").append(myA);
								 };
							 }
							 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
							 $("#lableList").append(myWhite);
							 for(var each in tempInterval){
								    clearInterval(tempInterval[each]);
								}
							 for(var each in humpInterval){
								    clearInterval(humpInterval[each]);
								}
						 },
						 error:function(err){
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
						 }
			 	     })
				 })	 
	 }
	/* else{
		 var location =getUrlParam('searchText');
	        location =decodeURI(location.substring(1,999999));
	        为搜索结果页面
	        $.ajax({发送请求获取该搜索内容下的标签数据
				url:"tag/findDeviceTag.html",
				type:"post",
				data:{
					searchText:location,
	            },
			 success:function(result){
				 result=JSON.parse(result)
				 if(result.result==-1){
					 setTimeout(function(){
             			swal(" 查询失败！",result.message, "error");
             		},0);
					 return false;
				 }
				  $("#lableNum").text(result.data.length)
				 for(var i=0;i<result.data.length;i++){
					 if(result.data[i].tagValue!=null){
						 
						 var updataLenth = result.data[i].tagValue.updatetime.length-2;
						 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth)
						 
						 
						 if(result.data[i].serviceId!=null){
							 var routerId = result.data[i].serviceId;
						 }else{
							 var routerId = '未绑定'
						 }
					 var myA='<a id="a_'+result.data[i].id+'" href="details.html?'+result.data[i].id+'" class="tagList">'+
					 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
					 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
					 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
					 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
					 '<tr><td>电量</td><td id="lableBattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
					 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
					 '</tbody></table></a>';
					 $("#lableList").append(myA);
					 if(result.data[i].tagValue.temp==null){
						 $("#lableTemp_"+result.data[i].id+"").text("未获取")
					 }
					 if(result.data[i].tagValue.hum==null){
						 $("#lableHum_"+result.data[i].id+"").text("未获取")
					 }
					 if(result.data[i].tagValue.battery==null){
						 $("#lableBattery_"+result.data[i].id+"").text("未获取")
					 }
					 }else{
						 if(result.data[i].serviceId!=null){
							 var routerId = result.data[i].serviceId;
						 }else{
							 var routerId = '未绑定'
						 }
						 var myA='<a id="a_'+result.data[i].id+'" href="details.html?'+result.data[i].id+'" class="tagList">'+
						 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
						 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
						 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
						 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
						 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
						 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
						 '</tbody></table></a>';
						 $("#lableList").append(myA); 
					 }
				 }
				 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
					 $("#lableList").append(myWhite);
			 },
			 error:function(err){
				 console.log(err)
			 }
			 })	
	 
			 $("#sortZone").click(function(){
			 		var sortZoneType =  parseInt($("#sortZone").attr("name"));
			 		$("#sortTime").val("时间")
			 		if(sortZoneType==3){
			 			$("#sortZone").attr("name","4");
			 			$("#sortZone").val("区域↑")
			 		}else{
			 			$("#sortZone").attr("name","3");
			 			$("#sortZone").val("区域↓")
			 		}
			 	     $.ajax({
							url:"tag/findDeviceTag.html",
							type:"post",
							data:{
								orderType:sortZoneType,
								searchText:location,
				            },
						 success:function(result){
							 result=JSON.parse(result)
							 if(result.result==-1){
								 setTimeout(function(){
			             			swal(" 数据获取失败！",result.message, "error");
			             		},0);
								 return false;
							 }
							 $("#lableList a").remove();
							 $("#lableList #myWhite").remove();
							 $("#lableNum").text(result.data.length)
							 for(var i=0;i<result.data.length;i++){			 
								 if(result.data[i].tagValue!=null){
									 
									 var updataLenth = result.data[i].tagValue.updatetime.length-2;
									 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth)
									 
									 if(result.data[i].serviceId!=null){
										 var routerId = result.data[i].serviceId
									 }else{
										 var routerId = '未绑定'
									 }
									 
								 var myA='<a id="a_'+result.data[i].id+'" href="details.html?'+result.data[i].id+'" class="tagList">'+
								 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
								 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
								 '<tr><td>路由ID</td><td id="lableRouter_'+result.data[i].id+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
								 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
								 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
								 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
								 '</tbody></table></a>';
								 $("#lableList").append(myA);
								 if(result.data[i].tagValue.temp==null){
									 $("#lableTemp_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.hum==null){
									 $("#lableHum_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.battery==null){
									 $("#lablebattery_"+result.data[i].id+"").text("未获取");
								 };
								 }else{
									 if(result.data[i].serviceId!=null){
										 var routerId = result.data[i].serviceId;
									 }else{
										 var routerId = '未绑定'
									 };
									 var myA='<a id="a_'+result.data[i].id+'" href="details.html?'+result.data[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
									 '</tbody></table></a>';
									 $("#lableList").append(myA);

								 }
							 }
							 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
							 $("#lableList").append(myWhite);
							 for(var each in tempInterval){
								    clearInterval(tempInterval[each]);
								};
							 for(var each in humpInterval){
								    clearInterval(humpInterval[each]);
								};
						 }
			 	     })
				 })	 
	 }*/
	 else{
		    var location = getUrlParam("sign");
//		    window.location.search;
//		    location = location.substring(1,999999);
	        /*为搜索结果页面*/
	        $.ajax({/*发送请求获取该搜索内容下的标签数据*/
				url:"tag/findDeviceTag.html",
				type:"post",
				data:{
					searchText:location,
	            },
			 success:function(result){
				 result=JSON.parse(result);
				 console.log(result);
				 if(result.result!=0){
					 setTimeout(function(){
             			swal(" 查询失败！",result.message, "error");
             		},0);
					 return false;
				 }
//				  $("#lableNum").text(result.data.length);
				 
				  var items = result.data.list;
					 $("#lableNum").text(items.length);
				 for(var i=0;i<result.data.length;i++){
					 if(result.data[i].tagValue!=null){ 
						 var updataLenth = result.data[i].tagValue.updatetime.length-2;
						 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth); 
						 if(result.data[i].serviceSn!=null){
							 var routerId = result.data[i].serviceSn;
						 }else{
							 var routerId = '未绑定';
						 };
					 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
					 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
					 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
					 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
					 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
					 '<tr><td>电量</td><td id="lableBattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
					 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
					 '</tbody></table></a>';
					 $("#lableList").append(myA);
					 if(result.data[i].tagValue.temp==null){
						 $("#lableTemp_"+result.data[i].id+"").text("未获取")
					 };
					 if(result.data[i].tagValue.hum==null){
						 $("#lableHum_"+result.data[i].id+"").text("未获取")
					 };
					 if(result.data[i].tagValue.battery==null){
						 $("#lableBattery_"+result.data[i].id+"").text("未获取")
					 };
					 }else{
						 if(result.data[i].serviceSn!=null){
							 var routerId = result.data[i].serviceSn;
						 }else{
							 var routerId = '未绑定'
						 };
						 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
						 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
						 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
						 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
						 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
						 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
						 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
						 '</tbody></table></a>';
						 $("#lableList").append(myA); 
					 };
				 }
				 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
					 $("#lableList").append(myWhite);
			 },
			 error:function(err){
				 console.log(err);
			 }
			 })		 
			 $("#sortZone").click(function(){
			 		var sortZoneType =  parseInt($("#sortZone").attr("name"));
			 		$("#sortTime").val("时间");
					 $("#sortZone").attr("disabled",true);
				 	 $("#sortTime").attr("disabled",true);
			 		if(sortZoneType==3){
			 			$("#sortZone").attr("name","4");
			 			$("#sortZone").val("区域↑");
			 		}else{
			 			$("#sortZone").attr("name","3");
			 			$("#sortZone").val("区域↓");
			 		};
			 	     $.ajax({
							url:"tag/findDeviceTag.html",
							type:"post",
							data:{
								orderType:sortZoneType,
								searchText:location,
				            },
						 success:function(result){
							 result=JSON.parse(result);
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
							 if(result.result!=0){
								 setTimeout(function(){
			             			swal(" 数据获取失败！",result.message, "error");
			             		},0);
								 return false;
							 };
							 $("#lableList a").remove();
							 $("#lableList #myWhite").remove();
							 $("#lableNum").text(result.data.length);
							 for(var i=0;i<result.data.length;i++){
								 if(result.data[i].tagValue!=null){
									 var updataLenth = result.data[i].tagValue.updatetime.length-2;
									 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth);
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定'
									 };
									 
								 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
								 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
								 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
								 '<tr><td>路由ID</td><td id="lableRouter_'+result.data[i].id+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
								 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
								 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
								 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
								 '</tbody></table></a>';
								 $("#lableList").append(myA);
								 if(result.data[i].tagValue.temp==null){
									 $("#lableTemp_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.hum==null){
									 $("#lableHum_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.battery==null){
									 $("#lablebattery_"+result.data[i].id+"").text("未获取");
								 };
								 }else{
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };
									 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
									 '</tbody></table></a>';
									 $("#lableList").append(myA);
								 };
							 };
							 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
							 $("#lableList").append(myWhite);
							 for(var each in tempInterval){
								    clearInterval(tempInterval[each]);
								};
							 for(var each in humpInterval){
								    clearInterval(humpInterval[each]);
								};
						 },
						 error:function(err){
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
						 }
			 	     });
				 });
				 $("#sortTime").click(function(){
			 		var sortTimeType =  parseInt($("#sortTime").attr("name"));
			 		$("#sortZone").val("区域");
					 $("#sortZone").attr("disabled",true);
				 	 $("#sortTime").attr("disabled",true);
			 		if(sortTimeType==1){
			 			$("#sortTime").attr("name","2");
			 			$("#sortTime").val("时间↓");
			 		}else{
			 			$("#sortTime").attr("name","1");
			 			$("#sortTime").val("时间↑");
			 		};
			 	     $.ajax({
							url:"tag/findDeviceTag.html",
							type:"post",
							data:{
								searchText:location,
								orderType:sortTimeType,
				            },
						 success:function(result){
							 result=JSON.parse(result);
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
							 if(result.result!=0){
								 setTimeout(function(){
			             			swal(" 数据获取失败！",result.message, "error");
			             		},0);
								 return false;
							 }
							 $("#lableList a").remove();
							 $("#lableList #myWhite").remove();
							 $("#lableNum").text(result.data.length);
							 for(var i=0;i<result.data.length;i++){
								 if(result.data[i].tagValue!=null){
									 var updataLenth = result.data[i].tagValue.updatetime.length-2;
									 var updatatime = result.data[i].tagValue.updatetime.substring(0,updataLenth); 
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };	 
								 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
								 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
								 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
								 '<tr><td>路由ID</td><td id="lableRouter_'+result.data[i].id+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
								 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">'+result.data[i].tagValue.temp+'℃</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">'+parseInt(result.data[i].tagValue.hum)+'%</td></tr>'+
								 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">'+result.data[i].tagValue.battery+'%</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
								 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:'+updatatime+'</td></tr>'+
								 '</tbody></table></a>';
								 $("#lableList").append(myA);
								 if(result.data[i].tagValue.temp==null){
									 $("#lableTemp_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.hum==null){
									 $("#lableHum_"+result.data[i].id+"").text("未获取");
								 };
								 if(result.data[i].tagValue.battery==null){
									 $("#lablebattery_"+result.data[i].id+"").text("未获取");
								 };
								 }else{
									 if(result.data[i].serviceSn!=null){
										 var routerId = result.data[i].serviceSn;
									 }else{
										 var routerId = '未绑定';
									 };
									 var myA='<a id="a_'+result.data[i].id+'" href="details.html?sign='+result.data[i].id+'" class="tagList">'+
									 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
									 '<tr><td colspan="4">编号<span id="lableSn_'+result.data[i].id+'">'+result.data[i].sn+'</span></td></tr>'+
									 '<tr><td>路由ID</td><td id="lableRouter_'+routerId+'">'+routerId+'<s/td><td>备注</td><td id="lableTile_'+result.data[i].id+'">'+result.data[i].title+'</td></tr>'+
									 '<tr><td>温度</td><td id="lableTemp_'+result.data[i].id+'">未获取</td><td>湿度</td><td id="lableHum_'+result.data[i].id+'">未获取</td></tr>'+
									 '<tr><td>电量</td><td id="lablebattery_'+result.data[i].id+'">未获取</td><td>区域</td><td id="lableZone_'+result.data[i].id+'">'+result.data[i].zoneName+'</td></tr>'+
									 '<tr><td colspan="4" id="lastUpdataTime_'+result.data[i].id+'">上报时间:未获取</td></tr>'+
									 '</tbody></table></a>';
									 $("#lableList").append(myA);
								 };
							 }
							 var myWhite = '<div id="myWhite" style="height:80px;width:100%;background:transparent"></div>'
							 $("#lableList").append(myWhite);
							 for(var each in tempInterval){
								    clearInterval(tempInterval[each]);
								}
							 for(var each in humpInterval){
								    clearInterval(humpInterval[each]);
								}
						 },
						 error:function(err){
							 $("#sortZone").attr("disabled",false);
						 	 $("#sortTime").attr("disabled",false);
						 }
			 	     })
				 })
	 
	 }

	 
	//获取url中的参数
	 function getUrlParam(name) {
	  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	  var r = window.location.search.substr(1).match(reg); //匹配目标参数
	  if (r != null) return unescape(r[2]); return null; //返回参数值
	 }
})
