

$(function () {
    var location = getUrlParam("sign");
//  window.location.search;
//  location = location.substring(1,999999);
	/*获取路由器sn号*/
    var boxId = location.substring(0,location.indexOf("_"))
    /*获取路由器ID*/
    var routerId = location.substring(location.indexOf("_")+1,location.indexOf("title="));
    var routerTitle = location.substring(location.indexOf("title=")+6,999999);
    $("#awakenSn").html(boxId)
	$.ajax({/*通过sn号请求该路由器配置信息*/
        url:"tag/findTagConfig.html?boxId="+boxId,
        type:"get",
        data:{
        	callback:'content::setting'
        },
        success:function(result){
        	$("#content").html(result)
            $("#routerTitleInput").val(decodeURI(routerTitle));
        },
    });

    
	$(".leftNav a:nth-child(6)").css("background","#1a63be");
//	$('#rightMain_title').html('设置');
	$('#lableViews',window.parent.document).find("#rightMain_title").html("设置");

    $("title").text("设置");
    /*点击返回按钮事件*/
	$("#resetSettingLable").click(function(){
		/*点击返回至路由器详情页*/
		window.location.href='routerDetails.html?sign='+routerId;
	})
	$("#settingLable p input").change(function(){
		/*判断配置信息内容是否超过127，超过则为127*/
		if($(this).val().length>5){
			$(this).val($(this).val().slice(0,5));
		}if(parseInt($(this).val())<0){
			$(this).val(0);
		}if(parseInt($(this).val())>127){
			$(this).val(127);
		}
	})
	/*保存按钮点击事件*/
	$("#saveSettingLable").click(function(){
				/*获取配置信息输入框中的数据*/
		      	var awakenLable = $("#awakenLable").val();
		        var reportData = $("#reportData").val();
		   	 	var reportGPS = $("#reportGPS").val();
		   	 	var serviceState = $("#serviceState").val();
		   	 	var alert_1 = $("#alert_1").val();
		   	 	var alert_2 = $("#alert_2").val();
		   	 	var alertInterval = $("#alertInterval").val();
		   	 	var routerTitleVal = $("#routerTitleInput").val()
				var myData = new Array();
				var myDataName=["标签唤醒周期","数据上报周期","GPS上报周期","服务状态查询","本地报警1","本地报警2","报警间隔"];
				myData.push(awakenLable,reportData,reportGPS,serviceState,alert_1,alert_2,alertInterval)
				for(var i=0;i<7;i++){
					if(myData[i]==""){
						setTimeout(function(){
		            		swal(myDataName[i]+"为空!");
		            		$("div h2").css("fong-size","20px");
		            	},0);
						return false;
					};
				};
				if(routerTitleVal==""){
					setTimeout(function(){
	            		swal("路由备注为空!");
	            		$("div h2").css("fong-size","20px");
	            	},0);
					return false;
				};
				$.ajax({
					/*将备注信息发送到服务器端保存*/
		            url:"tag/updateBox.html",
		            type:"post",
		            data:{
		            	id:routerId,
		            	title:routerTitleVal,
		            },
		            success:function(result){
		            	
		            }
				});
				console.log(awakenLable)
//				$.ajax({
//					/*将路由配置信息发送到服务器端保存*/
//		            url:"tag/addConfig.html",
//		            type:"post",
//		            data:{
//		            	boxId:boxId,
//		            	wakeup_time:awakenLable,
//		            	dat_report_time:reportData,
//		            	gps_report_time:reportGPS,
//		            	service_status:serviceState,
//		            	alarm_time1:alert_1,
//		            	alarm_time2:alert_2,
//		            	alarm_time:alertInterval,
//		            },
//		            success:function(result){
//		            	result=JSON.parse(result);
//		            	if(result.result==0){
//		            		setTimeout(function(){
//        	    				swal("保存成功！", "点击ok返回路由详情页!", "success");
//        	    				$(".confirm").unbind();
//        	    				 $(".confirm").click(function(){
//            						 window.location.href='routerDetails.html?sign='+routerId;
//            					 });
//     	             		},0);
//		            	}else{
//		            		setTimeout(function(){
//        	             		swal(" 保存失败！",result.message, "error");
//        	             	},0);
//		            	}
//		            	},
//		            })	
	})
});
	   function setFocus(e){
		    e.scrollIntoView(false);
		};
    $("p input").focus(function () {	
    	if(this.id!="alertInterval"){
        	if($(this).parent().nextAll("p").children("input").length!=0){
        		 var myId = $(this).parent().nextAll("p").children("input")[0].id;
            	 myInput = document.getElementById(myId);
        	}else{ 
        		myInput = document.getElementById(this.id);
        	};	
        }else{
        	var myId =$(this).parent().nextAll("div").children("input")[0].id;
        	myInput = document.getElementById(myId);
        };
        setTop = setInterval('setFocus(myInput)',10);
    });
    $("p input").blur(function(){
        clearInterval(setTop);
    });
    
