/**
 * Created by karim on 2017/1/12 0012.
 */
		isJson = function(obj){
				var isjson = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length; 
				return isjson;
		}
 function getUrlParam(name) {
   var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
   var r = window.location.search.substr(1).match(reg); //匹配目标参数
   if (r != null) return unescape(r[2]); return null; //返回参数值
  }

//	$.ajax({						//获取用户信息
//		url:"user/getLoginUser.html",
//		type:"get",
//		success:function(result){
//
//		if(isJson(result)==false){
//			result=JSON.parse(result);
//		}; 
//			if(result.result!=0){
//				setTimeout(function(){
//    				swal("未登录！", "点击ok登录!", "error");
//    				$(".confirm").unbind();
//    				 $(".confirm").click(function(){
//						 window.location.href='login.html';
//					 })
//          		},0);
////				window.location.href='login.html';
//			}
//			$("#titleRight").find("a").first().text(result.data.username);
//			var myImg = '<img class="userPng" style="vertical-align: middle;height: 20px;">';
//			var mySrc = 'resources/images/user.png';
//			$("#titleRight a:nth-child(1)").prepend(myImg);
//			$(".userPng").attr("src",mySrc);
//		},
//		error:function(result){
//			window.location.href='login.html';
//		}
//		})
		var mySearchID = getUrlParam("sign");
//	var mySearchID = window.location.search;
//	mySearchID = mySearchID.substring(1,999999);
        $.ajax({/*获取标签信息*/
            url:"tag/findDeviceTagJson.html",
            type:"post",
            data:{
                id:mySearchID,
            },
            success:function(result){
    		if(isJson(result)==false){
    			result=JSON.parse(result);
    		}; 
                if(result.result!=0){
                	setTimeout(function(){
	    				swal("阀值获取失败！",result.message, "error");
	             		},0);
                	return false;
                }
                /*在统计图表头显示温湿度阀值*/
                if(result.data[0].tempAlert==null){
                    $("#tempLegend").text('未获取');
                }else{
                    $("#tempLegend").text(result.data[0].tempAlert[0]+"℃～"+result.data[0].tempAlert[1]+"℃");
                };
                if(result.data[0].humAlert==null){
                    $("#humLegend").text('未获取');
                }else{
                    $("#humLegend").text(result.data[0].humAlert[0]+"%～"+result.data[0].humAlert[1]+"%");     
                    }

            }
        })


		var calendar = new LCalendar();
		var myDate = new Date();
		//获取当前年
		var myear=myDate.getFullYear();
		//获取当前月
		var month=myDate.getMonth()+1;
		//获取当前日
		var date=myDate.getDate();
		calendar.init({
			'trigger': '#startData', //标签id
			'type': 'date', //date 调出日期选择 datetime 调出日期时间选择 time 调出时间选择 ym 调出年月选择,
			'minDate': (new Date().getFullYear()-3) + '-' + 1 + '-' + 1, //最小日期
			'maxDate': (new Date().getFullYear()) + '-' + month + '-' + date //最大日期
		});
		var calendar_end = new LCalendar();
		calendar_end.init({
			'trigger': '#endData', //标签id
			'type': 'date', //date 调出日期选择 datetime 调出日期时间选择 time 调出时间选择 ym 调出年月选择,
			'minDate': (new Date().getFullYear()-3) + '-' + 1 + '-' + 1, //最小日期
			'maxDate': (new Date().getFullYear()) + '-' + month + '-' + date //最大日期
		});

$("#timeSel").change(function(){
	/*当选择的时间段为自定义时间时显示开始时间与结束时间输入框，非自定义时间则隐藏*/
	var mySel = $("#timeSel").val();
	if(mySel=="自定义时间"){
		$("#startData").css("display","block");
		$("#endData").css("display","block");

	}else{
		$("#startData").css("display","none");
		$("#endData").css("display","none");
	}
})
/*点击生成统计图按钮事件*/
$("#searchDiagram").click(function(){
	var myDate = new Date();
	//获取当前年
	var year=myDate.getFullYear();
	//获取当前月
	var month=myDate.getMonth()+1;
	//获取当前日
	var date=myDate.getDate();
	var endTime = year+"-"+month+"-"+date;
    var location = getUrlParam("sign");
//    window.location.search;
//    location = location.substring(1,999999);
	var selName = $("#timeSel").val();
    var myType = 1;
    var myDay = 1;
    var XType = 0;
    /*点击生成统计图之后，选择菜单和生成按钮禁用*/
    $("#timeSel").attr("disabled",true);
    $("#searchDiagram").attr("disabled",true);
    /*判断生成统计图时间段*/
	if(selName=="今天"){
		myType = 1;
		myDay = 1;
		XType = 0;
		var myData = {"tagId":location,"type":myType,"endTime":endTime,"days":myDay};
	}
	else if(selName=="最近3天"){
		myType = 1;
		myDay = 3;
		XType = 1;
		var myData = {"tagId":location,"type":myType,"endTime":endTime,"days":myDay};
	}
	else if(selName=="最近7天"){
		myType = 1;
		myDay = 7;
		XType = 1;
		var myData = {"tagId":location,"type":myType,"endTime":endTime,"days":myDay};
	}
	else if(selName=="最近30天"){
		myType = 1;
		myDay = 30;
		XType = 2;
		var myData = {"tagId":location,"type":myType,"endTime":endTime,"days":myDay};
	}
	else if(selName=="最近90天"){
		myType = 1;
		myDay = 90;
		XType = 2;
		var myData = {"tagId":location,"type":myType,"endTime":endTime,"days":myDay};
	}
	else if(selName=="自定义时间"){
		myType = 2;
		var startTime = $("#startData").val();
		endTime = $("#endData").val();
		if(startTime==""){
        	setTimeout(function(){
        		swal("开始时间为空!");
        		$("div h2").css("fong-size","20px");
        	},0)
//			alert("开始时间为空！")
        	$("#timeSel").attr("disabled",false);
        	$("#searchDiagram").attr("disabled",false);
			return false;
		}
		if(endTime==""){
        	setTimeout(function(){
        		swal("结束时间为空!");
        		$("div h2").css("fong-size","20px");
        	},0)
//			alert("结束时间为空！")
        	$("#timeSel").attr("disabled",false);
     		$("#searchDiagram").attr("disabled",false);
			return false;
		}
		XType = 2;
		if(timeTell(startTime,endTime)==-2){
			setTimeout(function(){
        		swal("结束时间不能大于开始时间!");
        		$("div h2").css("fong-size","20px");
        	},0)
//			alert("结束时间不能大于开始时间！");
		    $("#timeSel").attr("disabled",false);
		    $("#searchDiagram").attr("disabled",false);
			return false;
		}
		if(timeTell(startTime,endTime)!=0){
			setTimeout(function(){
        		swal("最大时间间隔为三个月!");
        		$("div h2").css("fong-size","20px");
        	},0)
//			alert("最大时间间隔为三个月！");
		    $("#timeSel").attr("disabled",false);
		    $("#searchDiagram").attr("disabled",false);
			return false;
		}else{
			var myData = {"tagId":location,"type":myType,"endTime":endTime,"startTime":startTime};
		}
	}else{
		setTimeout(function(){
    		swal("请选择正确的时间!");
    		$("div h2").css("fong-size","20px");
    	},0)
//		alert("请选择正确的时间");
		$("#timeSel").attr("disabled",false);
		$("#searchDiagram").attr("disabled",false);
		return false;
	}
    var tempList = [];
    var humList = [];
    var timeList = [];
	 $.ajax({
			url:"tag/getReport.html",
			type:"post",
			data:myData,
		 success:function(result){
	        var data = JSON.parse(result);
	        if(data.result!=0){
	        	setTimeout(function(){
             		swal(" 报表获取失败！",result.message, "error");
             	},0);
//	        	alert(data.message)
	        $("#timeSel").attr("disabled",false);
		    $("#searchDiagram").attr("disabled",false);
	        	return false;
	        }
	        var timeCompany =data.data.unit;
	        if(data.data.unit=="2min"||data.data.unit=="5min"||data.data.unit=="15min"||timeCompany=="1day"){
	        	timeCompany =timeCompany.substring(0,timeCompany.length-2);
	        }else if(timeCompany=="1hour"){
	        	timeCompany =timeCompany.substring(0,timeCompany.length-3);
	        }
	        if(data.data.unit=="2min"||data.data.unit=="5min"||data.data.unit=="15min"){
	        	var myX = 48;
	        	var myX_2=54;
	        }else if(data.data.unit=="1hour"||data.data.unit=="1day"){
	        	var myX = 68;
	        	var myX_2=72;
	        }
	        $(".timeLegend").text(timeCompany);
		    $("#timeSel").attr("disabled",false);
		    $("#searchDiagram").attr("disabled",false);
	        var days =parseInt(data.data.days);
	        if(days==1){
		        var myTime = [];
		        for(var i=0;i<data.data.reports.length;i++){
		        	tempList.push(data.data.reports[i].tempMean.toString().substring(0,5));
		        	humList.push(data.data.reports[i].humMean.toString().substring(0,5));
		        	timeList.push(data.data.reports[i].time.substring(11,16))
		        	//timeList.push(data.data.reports[i].time.substring(0,data.data.reports[i].time.length-3))
		        }
	        }else if(days>1&&days<=3){
	        	 var myTime = [];
			        for(var i=0;i<data.data.reports.length;i++){
			        	tempList.push(data.data.reports[i].tempMean.toString().substring(0,5));
			        	humList.push(data.data.reports[i].humMean.toString().substring(0,5));
			        	timeList.push((data.data.reports[i].time.substring(5,16)).replace(/-/g,"/"));
			        	//timeList.push(data.data.reports[i].time.substring(0,data.data.reports[i].time.length-3))
			        }
	        }else if(days>3&&days<=7){
	        	 var myTime = [];
			        for(var i=0;i<data.data.reports.length;i++){
			        	tempList.push(data.data.reports[i].tempMean.toString().substring(0,5));
			        	humList.push(data.data.reports[i].humMean.toString().substring(0,5));
			        	timeList.push(data.data.reports[i].time.substring(5,16).replace(/-/g,"/"));
			        	//timeList.push(data.data.reports[i].time.substring(0,data.data.reports[i].time.length-3))
			        }
	        }else if(days>7&&days<=30){
	        	 var myTime = [];
			        for(var i=0;i<data.data.reports.length;i++){
			        	tempList.push(data.data.reports[i].tempMean.toString().substring(0,5));
			        	humList.push(data.data.reports[i].humMean.toString().substring(0,5));
			        	timeList.push(data.data.reports[i].time.substring(2,16).replace(/-/g,"/"));
			        	//timeList.push(data.data.reports[i].time.substring(0,data.data.reports[i].time.length-3))
			        }
	        }else{
	        	 var myTime = [];
			        for(var i=0;i<data.data.reports.length;i++){
			        	tempList.push(data.data.reports[i].tempMean.toString().substring(0,5));
			        	humList.push(data.data.reports[i].humMean.toString().substring(0,5));
			        	timeList.push(data.data.reports[i].time.substring(2,10).replace(/-/g,"/"));
			        	//timeList.push(data.data.reports[i].time.substring(0,data.data.reports[i].time.length-3))
			        }
	        }

	        setEcharts('tempCanvasParent',tempList,timeList,myX,myX_2);
	        setEcharts('humCanvasParent',humList,timeList,myX,myX_2);
       
	    }});


})

function timeTell(a,b){
	var startTime = a;
	var endTime = b;
	startTime = startTime.replace(/-/g,"/");
	endTime = endTime.replace(/-/g,"/");
	startTime = new Date(startTime);
	endTime = new Date(endTime);
	startTime = startTime.getTime()/1000;
	endTime = endTime.getTime()/1000;
	if(endTime-startTime<0){
		return -2;
	}else if(endTime-startTime>7776000){
		return -1;
	}else{
		return 0;
	}
	
}


window.onload = function(){
setTimeout(function(){
	{
		var myDate = new Date();
		//获取当前年
		var year=myDate.getFullYear();
		//获取当前月
		var month=myDate.getMonth()+1;
		//获取当前日
		var date=myDate.getDate();
		var endTime = year+"-"+month+"-"+date;
	    var location = getUrlParam("sign");
//	    window.location.search;
//	    location = location.substring(1,999999);
		var selName = $("#timeSel").val();
	    var myType = 1;
	    var myDay = 1;
		var searchVal = $("#dayData").val();
		var myData = {"tagId":location,"type":myType,"endTime":endTime,"days":myDay};
	    var tempList = [];
	    var humList = [];
	    var timeList = [];
	    $("#timeSel").attr("disabled",true);
	    $("#searchDiagram").attr("disabled",true);
		 $.ajax({
				url:"tag/getReport.html",
				type:"post",
				data:myData,
			 success:function(result){
		        var data = JSON.parse(result);
		        if(data.result!=0){
		        	setTimeout(function(){
	             		swal(" 报表获取失败！",result.message, "error");
	             	},0);
//		        	alert(data.message);
				    $("#timeSel").attr("disabled",false);
				    $("#searchDiagram").attr("disabled",false);
		        	return false;
		        }
		        $("#timeSel").attr("disabled",false);
		        $("#searchDiagram").attr("disabled",false);
		        
		        var myTime = [];
		        for(var i=0;i<data.data.reports.length;i++){
		        	if(data.data.reports[i].tempMean==null){
		        		tempList.push(0);
		        	}else{
		        		tempList.push(data.data.reports[i].tempMean.toString().substring(0,5));
		        	}
		        	if(data.data.reports[i].humMean==null){
		        		humList.push(0);
		        	}else{
		        		humList.push(data.data.reports[i].humMean.toString().substring(0,5));
		        	}
		        	if(data.data.reports[i].time==null){
		        		timeList.push(0);
		        	}else{
		        		timeList.push(data.data.reports[i].time.substring(5,16).replace(/-/g,"/"));
		        	}
		    /*    	tempList.push(data.data.reports[i].tempMean);
		        	humList.push(data.data.reports[i].humMean);
		        	timeList.push(data.data.reports[i].time.substring(11,16))
		        	*/
/*		        	timeList.push(data.data.reports[i].time.substring(0,data.data.reports[i].time.length-3))
*/		        }
		        setEcharts('tempCanvasParent',tempList,timeList,48,54);
		        setEcharts('humCanvasParent',humList,timeList,48,54); 
		    }});
	// setTimeout(function(){
//	     if(warning.length>0){
//	         alert("警报!"+warning)
//	     }
	// },1000);
	}
},0)
   
};

function setEcharts(id,myData,myDate,myX,myX_2){
    if (id=="tempCanvasParent"){
        var titleText = "温度图";
        var yFormatter = '{value} °C';
    }else{
        var titleText = "湿度图";
        var yFormatter = '{value} %';
    }
var x = parseInt(myX);
var x_2 = parseInt(myX_2);
var dom = document.getElementById(id);
var myChart = echarts.init(dom);
var app = {};
option = null;
var date = myDate;
var data = myData;
option = {
    tooltip: {
        trigger: 'axis',
        position: function (pt) {
            return [pt[0], '10%'];
        }
    },
    title: {
        left: 'center',
        text: titleText,
    },
    toolbox: {
    	show : false,
        feature: {
            dataZoom: {
                yAxisIndex: 'none'
            },
            restore: {},
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: date.map(function (str) {
            return str.replace(' ','\n')
        })
    },
    yAxis: {
        type: 'value',
/*        axisLabel : {
            formatter: yFormatter//设置Y轴单位
        },*/
        boundaryGap: [0, '100%']
    },
    grid: { // 控制图的大小，调整下面这些值就可以，
        x: x,
        x2: x_2,
        y2:75,
       // y2可以控制 X轴跟Zoom控件之间的间隔，避免以为倾斜后造成 label重叠到zoom上
    },
    dataZoom: [{
        type: 'inside',
        start: 0,
        end: 100
    }, {
        start: 0,
        end: 10,
        handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
        handleSize: '100%',
        handleStyle: {
            color: '#fff',
            shadowBlur: 3,
            shadowColor: 'rgba(0, 0, 0, 0.6)',
            shadowOffsetX: 2,
            shadowOffsetY: 2
        }
    }],
    series: [
        {
            name:'',
            type:'line',
            smooth:true,
            symbol: 'none',
            sampling: 'average',
            itemStyle: {
                normal: {
                    color: 'rgb(255, 70, 131)'
                }
            },
//            areaStyle: {
//                normal: {
//                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                        offset: 0,
//                        color: 'rgb(255, 255, 255)'
//                    }, {
//                        offset: 1,
//                        color: 'rgb(255, 255, 255)'
//                    }])
//                }
//            },
            data: data
        }
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
}
$("#titleRight a:nth-child(1)").click(function(){
	window.location.href='user.html';
})

