/**
 * Created by karim on 2017/1/17.
 */
$(".leftNav a:nth-child(5)").css("background","#1a63be")
$('#rightMain_title').html('用户信息');	//修改标题
$("#editUserBtn").click(function(){
	window.location.href='editUser.html';
})

$.ajax({			//获取订单信息
		url:"iotsim/orderQuery.html",
		data:{
			num: "898602B9191750169301",
            simType: "iccid",
            ordersn:"201705151428406724281",
            
		},
		type:"get",
			success:function(result){
				console.log(result);
				result=JSON.parse(result)//解析字符串
				if(result.error!=0){//判断是否获取成功
					 setTimeout(function(){
						 swal(" 查询失败！",result.message, "error");
//		              	 window.location.href="error/error.html";
		       		},0);
				}
				var myA = '<p style="font-weight: bold; color: #169BD5; text-align: center; height: 20px; line-height: 30px; font-size: 22px">订单列表</p>'+
				 '<table cellspacing="1" cellpadding="0"><tbody cellspacing="1" cellpadding="0" style="padding:0;margin:0;width:100%">'+
				 '<tr><td>订单编号</td><td>201705151428406724281</td></tr>'+
				 '<tr><td>续费状态</td><td>'+(result.result.renewals_status==1? "成功":"失败")+'</td></tr>'+
				 '<tr><td>续费时间</td><td>'+result.result.renewals_time+'</td></tr>'+
				 '</tbody></table>';
				$("#userVal").append(myA);
				
		 },
		 error:function(err){
			 console.log(err);
		 }
	 });

$("#editUserSave").click(function(){/*点击保存用户信息*/
	/*获取input框中的输入的用户信息*/
	var userName = $("#editUsername").val();
	var trueName = $("#editTruename").val();
	var loginName = $("#editUsername").val();
	var userId = $("#editUserVal").attr("name");
	var phone = $("#editUserPhone").val();
	var psw = $("#editUserPsw").val();
	/*判断输入的用户信息是否为空*/
	var pattern = /^1[34578]\d{9}$/;    
	if(psw==""){
		setTimeout(function(){
    		swal("密码为空!");
    		$("div h2").css("fong-size","20px")
    	},0);
//		alert("密码为空！");
		return false;
	};
	$.ajax({/*将新编辑好的用户信息发送到服务器端保存*/
		url:"user/updateUser.html",
		type:"post",
		data:{"id":userId,"password":psw},
	 success:function(result){
		 result=JSON.parse(result);
		 /*判断是否保存成功*/
		 if(result.result==0){
			 setTimeout(function(){
 				swal("保存成功！", "点击ok返回用户页!", "success");
 				$(".confirm").unbind();
 				 $(".confirm").click(function(){
						 window.location.href='user.html'
					 });
       		},0);
//			 alert(result.message);
//			 window.location.href='user.html';
		 }else{
				setTimeout(function(){
             		swal(" 保存失败！",result.message, "error");
             	},0);
//			 alert(result.message);
		 };
		 }
	 })
})

	$("#recharge").click(function(){
		window.location.href = 'pay.html';
	});

	$("#editUserBack").click(function(){
		window.location.href='user.html';
	});

    function setFocus(e){
	/*将对象滚动到可见范围内*/
	    e.scrollIntoView(false);
	};

$("input").focus(function () {
	/*设置input获取焦点时，将下一行的input滚动到课件范围内*/
	if(this.id!="editUserPsw"){/*判断是否为最后一个输入框*/
    	if($(this).parent().nextAll("p").children("input").length!=0){/*判断是否有下一个输入框*/
    		 var myId = $(this).parent().nextAll("p").children("input")[0].id;
        	 myInput = document.getElementById(myId);
    	}else{ 
    		myInput = document.getElementById(this.id);
    	};
    }else{
    	var myId =$(this).parent().nextAll("div").children("input")[0].id;
    	myInput = document.getElementById(myId);
    };
	/*通过计时器执行setFocus()将对象滚动到可见范围内*/
    setTop = setInterval('setFocus(myInput)',10);
})

    $("input").blur(function(){
    	/*输入框失焦时清除计时器*/
        clearInterval(setTop);
    });
var myImg = '<img class="userPng" style="vertical-align: middle;height: 20px;">';
var mySrc = 'resources/images/user.png';
$("#titleRight a:nth-child(1)").prepend(myImg);
$(".userPng").attr("src",mySrc);
