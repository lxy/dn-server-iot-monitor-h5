$("#titleRight a:nth-child(2)").click(function(){	
	swal({
		title: "确定退出吗?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: '确定',
		cancelButtonText: "取消",
		closeOnConfirm: false,
//		closeOnCancel: false
	},
	function(isConfirm){
		if (isConfirm){
			 var exp = new Date();
			 exp.setTime(exp.getTime() + 1*24*60*60*1000);
			 document.cookie="expires=" + exp.toGMTString();
			 $.ajax({/*向服务器端发送退出登录请求，清除session信息*/
					url:"logout.html",
					type:"get",
				 success:function(result){
					 window.location.href="login.html";
				 }
			 })
		};	
	});
/*	
	var con;
	con=confirm("确定更要退出吗?"); //在页面上弹出对话框
	if(con==true){确认退出
		 var exp = new Date();
		 exp.setTime(exp.getTime() + 1*24*60*60*1000);
		 document.cookie="sessionId=1;expires=" + exp.toGMTString();
		 $.ajax({向服务器端发送退出登录请求，清除session信息
				url:"logout.html",
				type:"get",
			 success:function(result){
				 window.location.href="login.html";
			 }
		 })
		
	}*/
})

	var getCookie = function(name)
	{
	    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
	    if(arr=document.cookie.match(reg))
	        return (arr[2]);
	    else
	        return null;
	}

	var delCookie = function (name){
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval=getCookie(name);
		if(cval!=null)
		    document.cookie= name + "="+cval+";expires="+exp.toGMTString(); 
    } 

	var getUrlParam = function(name){
		var reg  = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
		var r = window.location.search.substr(1).match(reg);
		if (r!=null) return unescape(r[2]); return null;
	}

