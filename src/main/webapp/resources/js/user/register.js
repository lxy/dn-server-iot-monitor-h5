 var inp = document.getElementById('inputCode');
 var code = document.getElementById('code');
 var submit = document.getElementById('registerSuccess');
/* $("#userName").val(getCookie("userName"));
 function getCookie(name){
	var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
	if(arr=document.cookie.match(reg))
		return unescape(arr[2]);
	else
		return null;
 };*/
 
 $("#phone").change(function(){
	 var phone = $("#phone").val();
	 var pattern = /^1[34578]\d{9}$/; 
	 console.log(pattern.test(phone))
	 if(pattern.test(phone)==true){
	 	$("#phoneCode").css("display","none");
	 	$("#phoneCode").removeClass("glyphicon-remove");
	 }else{
	 	$("#phoneCode").css("display","inline-block");
		$("#phoneCode").addClass("glyphicon-remove");
		$("#phoneCode").css("color","red");
	 }
 })
	 /*
	 $.ajax({
			url:"user/checkUser.html",
			type:"post",
			data:{
				"username":userName,
			},
		 success:function(result){
			 result=JSON.parse(result);
			 if(result.result!=0){
				$("#userNameCode").css("display","inline-block");
				$("#userNameCode").removeClass("glyphicon-ok");
				$("#userNameCode").addClass("glyphicon-remove");
				$("#userNameCode").css("color","red");
				 //alert(result.message);
			 }else if(result.result==0){
				$("#userNameCode").css("display","inline-block");
				$("#userNameCode").removeClass("glyphicon-remove");
				$("#userNameCode").addClass("glyphicon-ok");
				$("#userNameCode").css("color","lawngreen");
			 };
		 }})
 });*/
 
 
 var c = new KinerCode({
     len: 4,//需要产生的验证码长度
//     chars: ["1+2","3+15","6*8","8/4","22-15"],//问题模式:指定产生验证码的词典，若不给或数组长度为0则试用默认字典
     chars: [
         1, 2, 3, 4, 5, 6, 7, 8, 9, 0,
         'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
     ],//经典模式:指定产生验证码的词典，若不给或数组长度为0则试用默认字典
     question: false,//若给定词典为算数题，则此项必须选择true,程序将自动计算出结果进行校验【若选择此项，则可不配置len属性】,若选择经典模式，必须选择false
     copy: false,//是否允许复制产生的验证码
     bgColor: "transparent",//背景颜色[与背景图任选其一设置]
     bgImg: "",//若选择背景图片，则背景颜色失效
     randomBg: true,//若选true则采用随机背景颜色，此时设置的bgImg和bgColor将失效
     inputArea: inp,//输入验证码的input对象绑定【 HTMLInputElement 】
     codeArea: code,//验证码放置的区域【HTMLDivElement 】
     click2refresh: true,//是否点击验证码刷新验证码
     false2refresh: true,//在填错验证码后是否刷新验证码
     validateObj: submit,//触发验证的对象，若不指定则默认为已绑定的输入框inputArea
     validateEven: "click",//触发验证的方法名，如click，blur等
     validateFn: function (result, code) {//验证回调函数
         if (result) {
             //console.log('验证成功');
        	 /*判断登录信息是否为空*/
        	setTimeout(function(){
        		if($("#userName").val()==""){
            		setTimeout(function(){
            			swal("注册失败","用户名为空", "error");
            		},0);
            		return false;
            	}
            	if($("#password").val()==""){
            		setTimeout(function(){
            			swal("注册失败","密码为空", "error");
            		},0);
            		return false;
            	}
            	if($("#password").val()!=$("#passwordNext").val()){
            		setTimeout(function(){
            			swal("注册失败","两次密码不一致", "error");
            		},0);
            		return false;
            	}
            	if($("#trueName").val()==""){
            		setTimeout(function(){
            			swal("注册失败","真实姓名为空", "error");
            		},0);
            		return false;
            	}
/*            	if($("#phone").val()==""){
            		setTimeout(function(){
            			swal("注册失败","手机号码为空", "error");
            		},0);
            		return false;
            	}*/
            	if($("#phoneCode").attr("class").indexOf("remove")!=-1){
            		setTimeout(function(){
            			swal("注册失败","手机号不正确", "error");
            		},0);
            		return false;
            	}
            	var userName = $("#userName").val();
        		var password = $("#password").val();
        		var trueName = $("#trueName").val();
        		var phone = $("#phone").val();
        		$.ajax({
        			url:"user/addUser.html",
        			type:"post",
        			data:{
        				"username":userName,
        				"password":password,
        				"truename":trueName,
        				"mobile":phone,
        			},
        		 success:function(result){
        			 result=JSON.parse(result);
        			 if(result.result!=0){
        					setTimeout(function(){
                    			swal(" 注册失败！",result.message, "error");
                    		},0);
        				 //alert(result.message);
        			 }else if(result.result==0){
        				 setTimeout(function(){
                 			swal(" 注册成功！",result.message, "success");
                 			$(".confirm").click(function(){
                 				 var exp = new Date();
                				 exp.setTime(exp.getTime() + 5*24*60*60*1000);
                				 document.cookie="userName="+userName+";expires=" + exp.toGMTString();
                				 document.cookie="token="+result.data.sessionId+";expires=" + exp.toGMTString();
                				 window.location.href='lable.html';
    	    				});
                 		},0);
        			 };
        		 }})
            	
        	},10)
          
        	/* else{
        		登录信息通过验证，获取输入框中的登录信息
        		var userName = $("#userName").val();
        		var password = $("#password").val();
        		$.ajax({将登录信息发送到服务器端
        			url:"user/login.html",
        			type:"post",
        			data:{
        				"userName":userName,
        				"password":password,
        			},
        		 success:function(result){
        			 result=JSON.parse(result);
        			 if(result.result!=0){判断是否登录成功
        					setTimeout(function(){
                    			swal(" 登录失败！",result.message, "error");
                    		},0);
        				 //alert(result.message);
        			 }else if(result.result==0){
        				 var exp = new Date();
        				 exp.setTime(exp.getTime() + 5*24*60*60*1000);
        				 document.cookie="userName="+userName+";expires=" + exp.toGMTString();
        				 document.cookie="sessionId="+$.md5($.md5(password))+";expires=" + exp.toGMTString();
        				 window.location.href='lable.html'
        			 };
        		 }})
        	};*/
         } else {
             if (this.opt.question) {
                 alert('验证失败:' + code.answer+"11");
             } else {
            	 $("#inputCode").val("");
                 //alert("验证码错误！")
            	 
             	if($("#userName").val()==""){
            		setTimeout(function(){
            			swal("注册失败","用户名为空", "error");
            		},0);
            		return false;
            	}
            	if($("#password").val()==""){
            		setTimeout(function(){
            			swal("注册失败","密码为空", "error");
            		},0);
            		return false;
            	}
            	if($("#password").val()!=$("#passwordNext").val()){
            		setTimeout(function(){
            			swal("注册失败","两次密码不一致", "error");
            		},0);
            		return false;
            	}
            	if($("#trueName").val()==""){
            		setTimeout(function(){
            			swal("注册失败","真实姓名为空", "error");
            		},0);
            		return false;
            	}
            	if($("#phone").val()==""){
            		setTimeout(function(){
            			swal("注册失败","手机号码为空", "error");
            		},0);
            		return false;
            	}
            		setTimeout(function(){
            			swal("注册失败！", "验证码错误！", "error");
            		},0);
             };
         };
     }
 });
 /*将对象滚动到可见范围内*/
 $("#backLogin").click(function(){
	 window.location.href = 'login.html'
 })
 function setFocus(e){
	    e.scrollIntoView(false);
	};
 /*输入框获取焦点后将下一输入框滚动到可见范围内*/
	    $("#password").focus(function () {
	         passwordText = document.getElementById('passwordNext');
	        setTop = setInterval('setFocus(passwordText)',200);
	    });
	    $("#password").blur(function(){
	        clearInterval(setTop)
	    });
	    $("#passwordNext").focus(function () {
	         passwordNextText = document.getElementById('inputCode');
	        setTop = setInterval('setFocus(passwordNextText)',200);
	    });
	    $("#passwordNext").blur(function(){ 
	        clearInterval(setTop)
	    });
	    $("#userName").focus(function () {
	         userNameText = document.getElementById('password');
	        setTop = setInterval('setFocus(userNameText)',200);
	    });
	    $("#userName").blur(function(){
	        clearInterval(setTop)
	    });
	     $("#inputCode").focus(function () {
	         inputCodeText = document.getElementById('registerSuccess');
	        setTop = setInterval('setFocus(inputCodeText)',10);
	    });
	    $("#inputCode").blur(function(){
	        clearInterval(setTop)
	    });