<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
	<title>曲线图</title>
	 <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<script src="<%=basePath%>resources/js/jquery.js"></script>
<%-- 	<script src="<%=basePath%>resources/js/diagram/Chart.js"></script> --%>
	<script src="<%=basePath%>resources/js/lable/LCalendar.js" type="text/javascript" charset="utf-8"></script>
	<link href="<%=basePath%>resources/css/LCalendar.css" rel="stylesheet" type="text/css">
	<link href="<%=basePath%>resources/css/style.css" rel="stylesheet" type="text/css">
	<link href="<%=basePath%>resources/css/sweet-alert.css" type="text/css" rel="stylesheet">
	<script src="<%=basePath%>resources/js/sweet-alert.js"></script>
</head>
<style>
	*{margin: 0;padding: 0}
	.back{
	height: 35px;
    width: 80%;
    border: transparent;
    background-color: #169BD5;
    color: white;
    font-size: 2rem;
    border-radius: 10px;
    box-shadow:3px 4px 5px #666666;
    margin: 10px auto 0;
    }
	
</style>
<body>
<script src="<%=basePath%>resources/js/diagram/echarts.min.js"></script>
      <!--  <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts-all-3.js"></script> -->
<div id="diagram">
	<p class="topTitle"><img src="<%=basePath%>resources/images/Logo_2.png">创辉温湿度监测</p>
 		<div class="leftNav">
        	<a href="lable.html">标签</a>
<!--         	<a href="search.html">查询</a> -->
        	<a href="addLable.html">新增</a>
        	<a href="router.html">路由器</a>
        	<a href="page.html?jsp=lable/amap">地图</a>
        	<a href="user.html">用户</a>
 		</div>
 		<div class="title">
        <span class="left">历史图表</span>
        <div class="right" id="titleRight">
            <a>{{用户名}}</a>
            <a>退出</a>
        </div>
    </div>
 	<div class="rightMain">
  	<div id="diagramVal">
        <select id="timeSel">
			<option value="今天">今天</option>
			<option value="最近3天">最近3天</option>
			<option value="最近7天">最近7天</option>
			<option value="最近30天">最近30天</option>
			<option value="最近90天">最近90天</option>
			<option value="自定义时间">自定义时间</option>
		</select>
		<div id="timeDIY">
			<input type="text" class="left" name="startData" id="startData" placeholder="请输入开始日期" readonly="readonly"/>
			<input type="text" class="right" name="endData" id="endData" placeholder="请输入结束日期" readonly="readonly"/>
		</div>
		<input type="button" id="searchDiagram" value="绘制统计图">
		<div style="width:100%;overflow: hidden;height: 300px;position: relative;">
		<div style="position: absolute;top: 30px;left:2%;font-size: 1.2rem">单位：℃　阀值：<span id="tempLegend"></span>　时间单位：<span class="timeLegend">2m</span></div>
		<div id="tempCanvasParent">
		</div>
		</div>
		<div style="width:100%;overflow: hidden;height: 300px;position: relative;">
		<div style="position: absolute;top: 30px;left:2%;font-size: 1.2rem">单位：%　阀值：<span id="humLegend"></span>　时间单位：<span class="timeLegend">2m</span></div>
		<div id="humCanvasParent">	
		</div>
		</div>
		<div style="width:100%;text-align:center;">
		<input type="button" value="返回" id="back" class="back" onclick="back()">
		</div>
		<div style="height:90px;width:100%;background: transparent;"></div>
     </div>
</div>	
</div>
</body>
<script  type="text/javascript" charset="utf-8">
function back(){
	window.location.href='details.html'+window.location.search;
};
</script>
<script src="<%=basePath%>resources/js/diagram/diagram.js"></script>
<script src="<%=basePath%>resources/js/user/loginout.js"></script>
</html>
