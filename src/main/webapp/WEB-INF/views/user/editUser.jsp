<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
    <script src="<%=basePath%>resources/js/jquery.js"></script>
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
    <title>编辑用户信息</title>
</head>
<body class="editUserPage">
<div id="editUser">
	<jsp:include page="./../menu.jsp"></jsp:include>
   <div id="editUserVal">
       <p>用户名：<input type="text" id="editUsername" disabled="disabled" style="background: #ddd;"/></p>
       <p>实　名：<input type="text" id="editTruename" disabled="disabled" style="background: #ddd;"/></p>
       <p>手机号：<input type="tel" id="editUserPhone" maxlength="11" disabled="disabled" style="background: #ddd;"></p>
       <p>密　码：<input type="password" id="editUserPsw"></p>  
<!--     <div id="editUserSel">
    	<input type="button" value="保存" id="editUserSave" class="editBtn" style="margin-right: 10%;margin-left:0 ">
    	<input type="button" value="返回" id="editUserBack" class="editBtn" style="margin-left: 0">
    </div> -->
    </div>
</div>
 
</div>

</body>

<script src="<%=basePath%>resources/js/user/user.js"></script>
</html>