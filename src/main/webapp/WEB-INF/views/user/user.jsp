<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
    <script src="<%=basePath%>resources/js/jquery.js"></script>
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
    <title>用户信息</title>
</head>
<body class="userPage">
<div id="userDetails">
<jsp:include page="./../menu.jsp"></jsp:include>
   <div id="userVal">
   
<!--     <div id="userEditSel">
    	<input type="button" value="充值" id="recharge" class="lableOperation">
        <input type="button" value="编辑" id="editUserBtn" class="lableOperation">
    </div> -->
    </div>
</div>
 
</div>

</body>

<script src="<%=basePath%>resources/js/user/user.js"></script>
</html>