<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
 	String path = request.getContextPath();    
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
 	pageContext.setAttribute("basePath",basePath);    
 %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/js/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" 	rel="stylesheet">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
    <link href="<%=basePath%>resources/css/sweet-alert.css" type="text/css" 	rel="stylesheet">
    <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
    <script src="<%=basePath%>resources/js/jquery-1.9.1.min.js"></script>
    <script src="<%=basePath%>resources/js/sweet-alert.js"></script>
    <title>用户注册</title>
</head>
<body>
<div id="login_bg">
<div id="login_logoDiv">
    <%-- <img src="<%=basePath%>resources/images/Logo_1.png" style="width: 30%"> --%>
    <h1>用户注册</h1>
</div>
    <form id="login_form" method="post" name="login_form">
<!--         <label>用户名：</label> -->
        <div class="inputParent"><span class="glyphicon glyphicon-user" style="font-size:20px;width: 30px;vertical-align: text-bottom;color:white;"></span><input type="text" id="userName" class="input" name ='userName' placeholder="请输入用户名">
<!--         <span id="userNameCode" class="glyphicon glyphicon-ok" style="display:none;position:absolute;right:10%;top:10px;font-size:20px;width: 30px;"></span>
 -->        </div>
       <!--  <label style="font-family: '宋体'">密&nbsp;码：</label> -->
        <div class="inputParent"><span class="glyphicon glyphicon-lock" style="font-size:20px;width: 30px;vertical-align: text-bottom;color:white;"></span><input type="password" id="password" class="input" name = 'password'  placeholder="请输入密码">
<!--         <span class="glyphicon glyphicon-ok" style="color:lawngreen;position:absolute;right:10%;top:10px;font-size:20px;width: 30px;"></span>
 -->        </div>
        <div class="inputParent"><span class="glyphicon glyphicon-lock" style="font-size:20px;width: 30px;vertical-align: text-bottom;color:white;"></span><input type="password" id="passwordNext" class="input" name = 'passwordNext'  placeholder="请确认密码">
<!--         <span class="glyphicon glyphicon-ok" style="color:lawngreen;position:absolute;right:10%;top:10px;font-size:20px;width: 30px;"></span>
 -->        </div>
 		<div class="inputParent"><span class="glyphicon glyphicon-user" style="font-size:20px;width: 30px;vertical-align: text-bottom;color:white;"></span><input type="text" id="trueName" class="input" name = 'trueName'  placeholder="请输入真实姓名">
<!--         <span class="glyphicon glyphicon-ok" style="display:none;lawngreen;position:absolute;right:10%;top:10px;font-size:20px;width: 30px;"></span>
 -->        </div>
        <div class="inputParent"><span class="glyphicon glyphicon-phone" style="font-size:20px;width: 30px;vertical-align: text-bottom;color:white;"></span><input type="tel" id="phone" class="input" name = 'phone'  placeholder="请输入手机号码">
        <span id="phoneCode" class="glyphicon" style="display:none;color:lawngreen;position:absolute;right:10%;top:10px;font-size:20px;width: 30px;"></span>
    	</div>
        <input id="inputCode"  type="text" class="gradient" placeholder="验证码"/>
        <span id="code" class="mycode"></span>
        <br>
        <input type="button" value="完成注册"  id="registerSuccess">
        <input type="button" value="返回登录"  id="backLogin">
    </form>
</div>
</body>
<script src="<%=basePath%>resources/js/login/KinerCode.js"></script>
<script src="<%=basePath%>resources/js/user/register.js"></script>
<script src="<%=basePath%>resources/js/jquery.md5.js"></script>
</html>
