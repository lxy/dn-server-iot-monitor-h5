<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();  
String wspath="ws://"+request.getServerName()+":8981"+request.getContextPath()+"/";  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="zh-CN">
 <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
<body>
 <div id="lableViews">
    <jsp:include page="./../menu.jsp"></jsp:include>
    <div id="lableList">
    <div id="searchLableDiv">
    	<input id="searchText" type="text" placeholder="请输入要搜索的内容" class="left">
        <input id="searchBtn" value="查询" type="button" class="left">
    </div>
    <div id="lableNumDiv">标签总数：<span id="lableNum"></span><input type="button" class="right" style="margin:0 10px" value="时间↓" id="sortTime" name="2"><input id="sortZone" name="4" type="button" value="区域" class="right"></div>
    </div>
</div>
</body>
<script type="text/javascript">
/* $("#sortTime").attr("disabled",true);
$("#sortZone").attr("disabled",true); */
tempInterval = [];
humpInterval = [];
function lable(){
	window.location.href='lable.html';
}
function router(){
	window.location.href='router.html';
} 
if (window.location.search==""){
	 //建立websocket链接	
	 var wsurl = "<%=wspath%>webSocket.html";
	 var ws = null;
		 ws = new WebSocket(wsurl);
	 //当websocket连接建立成功时
		ws.onopen = function() {
	    console.log('websocket 打开成功');	 
	    ws.send("tag");
	}; 
	 //当收到服务端的消息时
	ws.onmessage = function(result) {
	    //result.data 是服务端发来的数据
	    if(result.data=="WebSocket:connected OK!"){
	       // console.log(result.data)
	    }else{
	        var result=JSON.parse(result.data);
	        for(var i=0;i<result.length;i++){
	        	if($("#a_"+result[i].id).length!=0){
	        	 	var myId = result[i].id;
	        	 	if(result[i].tagValue!=null){
	        	 		if($("#lastUpdataTime_"+myId+"").length!=0){
		        	 		 var updataLenth = result[i].tagValue.updatetime.length-2;
							 var updatatime = result[i].tagValue.updatetime.substring(0,updataLenth);
		        	 			$("#a_"+result[i].id+" .arrow").remove();
			        	 		var nowTemp=$("#lableTemp_"+myId+"").text();
			        	 		var nowHum = $("#lableHum_"+myId+"").text();
			        	 		var nowBattery = $("#lableBattery_"+myId+"").text();
			        	 		var tempType = compare(nowTemp,result[i].tagValue.temp);
			        	 		if(tempType==0){
			        	 			$("#lableTemp_"+myId+"").text(result[i].tagValue.temp+"℃");
			        	 			$("#lableTemp_"+myId+"").append('<span style="color:green" class="arrow">↓</span>');
			        	 		}else if(tempType==1){
			        	 			$("#lableTemp_"+myId+"").text(result[i].tagValue.temp+"℃");
			        	 			$("#lableTemp_"+myId+"").append('<span style="color:red" class="arrow">↑</span>');
			        	 		};
			        	 		var humType = compare(nowHum,parseInt(result[i].tagValue.hum));
			        	 		if(humType==0){
			        	 			$("#lableHum_"+myId+"").text(parseInt(result[i].tagValue.hum)+"%");
			        	 			$("#lableHum_"+myId+"").append('<span style="color:green" class="arrow">↓</span>');
			        	 		}else if(humType==1){
			        	 			$("#lableHum_"+myId+"").text(parseInt(result[i].tagValue.hum)+"%");
			        	 			$("#lableHum_"+myId+"").append('<span style="color:red" class="arrow">↑</span>');
			        	 		};

			        	 		var batteryType = compare(nowBattery,result[i].tagValue.battery);
			        	 		if(batteryType==0){
			        	 			$("#lableBattery_"+myId+"").text(result[i].tagValue.battery+"%");
			        	 			$("#lableBattery_"+myId+"").append('<span style="color:green" class="arrow">↓</span>');
			        	 		}else if(batteryType==1){
			        	 			$("#lableBattery_"+myId+"").text(result[i].tagValue.battery+"%");
			        	 			$("#lableBattery_"+myId+"").append('<span style="color:red" class="arrow">↑</span>');
			        	 		};
	/* 		        	 		$("#lableHum_"+myId+"").text(result[i].tagValue.hum+"%");	            	
				            	$("#lableBattery_"+myId+"").text(result[i].tagValue.battery+"%"); */
				            	$("#lastUpdataTime_"+myId+"").text("上报时间:"+updatatime);
		        	 		}
	        	 		};
		            	switch (result[i].tagValue.status){
		        		 case 0:
		        			 break;
		        		 case 1:
		        			 status_1(myId,result[i].tempAlert[0]);
		        			 break;
		        		 case 2:
		        			 status_2(myId,result[i].humAlert[0]);
		        			 break;
		        		 case 3:
		        			 status_3(myId,result[i].tempAlert[0],result[i].humAlert[0]);
		        			 break; 
		        	 };
	        	 	}else{
	        	 		$("#lableHum_"+myId+"").text("未获取");
		            	$("#lableTemp_"+myId+"").text("未获取");
		            	$("#lableBattery_"+myId+"").text("未获取");
		            	$("#lastUpdataTime_"+myId+"").text("上报时间:未获取");
	        	 	};	
	        	};
	       
	        }
	    }
	// 当websocket关闭时
	//ws.onclose = function() {
	 //   alert("websocket 连接关闭");
	//};
	// 当出现错误时
	ws.onerror = function(e) {
	    console.log("出现错误"+e);
	}

	};


function compare(a,b){
		a = a.substring(0,a.length-1);
		var nowVal = parseFloat(a);
		var newVal = parseFloat(b);
		if(nowVal>newVal){
			return 0;
		}else if(nowVal<newVal){
			return 1;
		}else if(nowVal==newVal){
			return -1;
		};
};

function compareNum(a,b){
	var nowNum = parseFloat(a);
	var newNum = parseFloat(b);
	if(nowNum>newNum){
		return 0;
	}else if(nowNum<newNum){
		return 1;
	}else{
		return -1;
	};
};
function place(e){
    e = e.replace(/-/g,"");
    e = e.replace(/ /g,"");
    e = e.replace(/:/g,"");
    e = e.replace(/：/g,"");
    e=parseInt(e);
    return e;
};
function status_3(id,tempAlert,humAlert){
	var myId = id;
	if($("#lableTemp_"+myId+"").attr("name")!="3"){
		$("#lableTemp_"+myId+"").attr("name","3");
		if(compareNum($("#lableTemp_"+myId+"").text(),tempAlert)==0){
			 tempInterval[myId] = setInterval(function(){
				 if($("#lableTemp_"+myId+"").attr("class")=="red"){
					 $("#lableTemp_"+myId+"").attr("class","black");
				 }else{
					 $("#lableTemp_"+myId+"").attr("class","red");
				 };
			 },500);
		 }else if(compareNum($("#lableTemp_"+myId+"").text(),tempAlert)==1){
			 tempInterval[myId] =  setInterval(function(){
				 if($("#lableTemp_"+myId+"").attr("class")=="blue"){
					 $("#lableTemp_"+myId+"").attr("class","black");
				 }else{
					 $("#lableTemp_"+myId+"").attr("class","blue");
				 };
			 },500);
		 };
	};
	if($("#lableHum_"+myId+"").attr("name")!="3"){
		$("#lableHum_"+myId+"").attr("name","3");
			 if(compareNum($("#lableHum_"+myId+"").text(),humAlert)==0){
				 humpInterval[myId] = setInterval(function(){
				 if($("#lableHum_"+myId+"").attr("class")=="red"){
					 $("#lableHum_"+myId+"").attr("class","black");
				 }else{
					 $("#lableHum_"+myId+"").attr("class","red");
				 };
			 },500);
		 }else if(compareNum($("#lableHum_"+myId+"").text(),humAlert)==1){
			 humpInterval[myId] =  setInterval(function(){
				 if($("#lableHum_"+myId+"").attr("class")=="blue"){
					 $("#lableHum_"+myId+"").attr("class","black");
				 }else{
					 $("#lableHum_"+myId+"").attr("class","blue");
				 };
			 },500);
		 };
	};
};
function status_2(id,humAlert){
	var myId = id;
	if($("#lableHum_"+myId+"").attr("name")!="2"){
		$("#lableHum_"+myId+"").attr("name","2");
	 if(compareNum($("#lableHum_"+myId+"").text(),humAlert)==0){
		 humpInterval[myId] = setInterval(function(){
			 if($("#lableHum_"+myId+"").attr("class")=="red"){
				 $("#lableHum_"+myId+"").attr("class","black");
			 }else{
				 $("#lableHum_"+myId+"").attr("class","red");
			 };
		 },500);
	 }else if(compareNum($("#lableHum_"+myId+"").text(),humAlert)==1){
		 humpInterval[myId] = setInterval(function(){
			 if($("#lableHum_"+myId+"").attr("class")=="blue"){
				 $("#lableHum_"+myId+"").attr("class","black");
			 }else{
				 $("#lableHum_"+myId+"").attr("class","blue");
			 };
		 },500);
	 };
	};
};
function status_1(id,tempAlert){
	var myId = id;
	if($("#lableTemp_"+myId+"").attr("name")!="1"){
		$("#lableTemp_"+myId+"").attr("name","1");
		 if(compareNum($("#lableTemp_"+myId+"").text(),tempAlert)==0){
			 tempInterval[myId] = setInterval(function(){
				 if($("#lableTemp_"+myId+"").attr("class")=="red"){
					 $("#lableTemp_"+myId+"").attr("class","black");
				 }else{
					 $("#lableTemp_"+myId+"").attr("class","red");
				 };
			 },500);
		 }else if(compareNum($("#lableTemp_"+myId+"").text(),tempAlert)==1){
			 tempInterval[myId] = setInterval(function(){
				if($("#lableTemp_"+myId+"").attr("class")=="blue"){
					 $("#lableTemp_"+myId+"").attr("class","black");
				 }else{
					 $("#lableTemp_"+myId+"").attr("class","blue");
				 };
			 },500);
		 };
	};
	};
$('#rightMain_title').html('标签列表');	
$(".leftNav a:nth-child(1)").css("background","#1a63be");
</script>
<script src="<%=basePath%>resources/js/lable/lable.js"></script>
<script src="<%=basePath%>resources/js/lable/search.js"></script>
</html>
