<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加新标签</title>
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
</head>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<body class="codeAddPage">
 <div id="addLable">
	<jsp:include page="./../menu.jsp"></jsp:include>
    <div id="addLableVal">
        <p>标签编号：<input type="text" id="addLableSn"></p>
        <p>备注名称：<input type="text" id="addLableName"></p>
        <p>监测区域：<input type="text" id="addLableRegoin"></p>
       <!--  <p>介&nbsp;&nbsp;绍：<input type="text" id="addLableContent"></p> -->
        <p>温度阀值：<input type="number" id="AddTempMin">--<input type="number" id="AddTempMax"></p>
    	<p>湿度阀值：<input type="number" id="AddHumMin">--<input type="number" id="AddHumMax"></p>
    <div id="addLableBtnDiv">
    	<div id="scanQRCode">
    		<img src="<%=basePath%>resources/images/scan.jpg">
    	</div>
<!--         <input type="button" value="保存" id="saveAddLable" class="editBtn">
        <input type="button" value="重置" id="resetAddLable" class="editBtn"> -->
        <div style="height:80px;width:100%;box-sizing:border-box"></div>
    </div>
    </div>
    <div id="addZoneVal" style="display:none">
    
    </div>
</div>    
</div>
</body>
    <script src="<%=basePath%>resources/js/lable/codeAddLable.js"></script>
</html>