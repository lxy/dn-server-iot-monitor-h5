<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
	<script src="<%=basePath%>resources/js/jquery-1.9.1.min.js"></script>
    <title>Title</title>
</head>
<body>
<div id="search">
 	<jsp:include page="./../menu.jsp"></jsp:include>
  	<div id="searchLableDiv">
        <input id="searchText" type="text" placeholder="请输入要搜索的内容" class="left">
        <input id="searchBtn" value="查询" type="button" class="left">
        <div id="searchPrompt">

        </div>
     </div>
</div>
</div>
</body>
<script src="<%=basePath%>resources/js/lable/search.js"></script>
</html>