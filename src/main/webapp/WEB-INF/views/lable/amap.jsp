<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path + "/";
	pageContext.setAttribute("basePath", basePath);
%>
<!doctype html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta name="viewport"	content="initial-scale=1.0, user-scalable=no, width=device-width">
 <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
<style type="text/css">
#container {
	height: 100%;
	margin: 0px;
}

.info {
	border: solid 1px silver;
}

div.info-top {
	position: relative;
	background: none repeat scroll 0 0 #F9F9F9;
	border-bottom: 1px solid #CCC;
	border-radius: 5px 5px 0 0;
}

div.info-top div {
	display: inline-block;
	color: #333333;
	font-size: 14px;
	font-weight: bold;
	line-height: 31px;
	padding: 0 10px;
}

div.info-top img {
	position: absolute;
	top: 10px;
	right: 10px;
	transition-duration: 0.25s;
}

div.info-top img:hover {
	box-shadow: 0px 0px 5px #000;
}

div.info-middle {
	font-size: 12px;
	padding: 6px;
	line-height: 20px;
}

div.info-bottom {
	height: 0px;
	width: 100%;
	clear: both;
	text-align: center;
}

div.info-bottom img {
	position: relative;
	z-index: 104;
}

span {
	margin-left: 5px;
	font-size: 11px;
}

.info-middle img {
	float: left;
	margin-right: 6px;
}
</style>
<title>地图设备</title>
</head>
<body>
	<div id="mapMain" style="width: 100%;height: 100%;overflow: hidden;">
	<jsp:include page="./../menu.jsp"></jsp:include>
		<div style="height: 100%;width: 100%">
		
		<div id="container" tabindex="0"></div>
		</div>
	</div>

	<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=39c0972805c0bd7f0e7649ee5ab9258c"></script>
	<script src="<%=basePath%>resources/js/jquery-1.9.1.min.js"></script>
	<script src="<%=basePath%>resources/js/lable/amap.js"></script>
	<script type="text/javascript">
	window.onload = function(){
		$("#container").css("top","-80px")
	}
	</script>
</body>
</html>

