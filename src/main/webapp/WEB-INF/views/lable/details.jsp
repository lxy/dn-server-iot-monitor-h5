<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
    <script src="<%=basePath%>resources/js/jquery.js"></script>
    <title>Title</title>
</head>
<body class="detailsPage">
<div id="lableDetails">
	<jsp:include page="./../menu.jsp"></jsp:include>
    <div id="lableDetailsVal">
<!-- 	    <div id="lableDetailsSel">
	        <input type="button" value="历史" id="temp" class="lableOperation left">
	        <input type="button" value="编辑" id="editLable" class="lableOperation right">
	        <input type="button" value="删除" id="delLable" class="lableOperation left">
	        <input type="button" value="返回" id="back" class="lableOperation right" onclick="back()">
	    </div> -->
    </div>
</div>

</body>
<script>
$('#rightMain_title').html('标签详情');	
function back(){
	window.location.href='lable.html';
};

</script>
<script src="<%=basePath%>resources/js/lable/details.js"></script>
</html>