<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加新标签</title>
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
</head>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<body class="addEditPage">
 <div id="addLable">
	<jsp:include page="./../menu.jsp"></jsp:include>
    <div id="addLableVal">
        <p>标签编号：<input type="text" id="addLableSn"></p>
        <p>备注名称：<input type="text" id="addLableName"></p>
        <p>监测区域：<input type="text" id="addLableRegoin"></p>
       <!--  <p>介&nbsp;&nbsp;绍：<input type="text" id="addLableContent"></p> -->
        <p>温度阀值：<input type="number" id="AddTempMin">--<input type="number" id="AddTempMax"></p>
    	<p>湿度阀值：<input type="number" id="AddHumMin">--<input type="number" id="AddHumMax"></p>
    <div id="addLableBtnDiv">
    	<div id="scanQRCode">
    		<img src="<%=basePath%>resources/images/scan.jpg">
    	</div>
<!--         <input type="button" value="保存" id="saveAddLable" class="editBtn">
        <input type="button" value="重置" id="resetAddLable" class="editBtn"> -->
        <div style="height:80px;width:100%;box-sizing:border-box"></div>
    </div>
    </div>
    <div id="addZoneVal" style="display:none">
    
    </div>
</div>    
</div>
</body>
    <script src="<%=basePath%>resources/js/lable/addEditLable.js"></script>
    <script type="text/javascript">
    if (window.location.search==""){
   	 function is_weixn(){
         var ua = navigator.userAgent.toLowerCase();
         if(ua.match(/MicroMessenger/i)=="micromessenger") {
             return true;
         } else {
             return false;
         };
     };
     if(!is_weixn()){
    	 $("#scanQRCode").click(function(){
         	setTimeout(function(){
        		swal("扫码添加仅支持在微信内部浏览器使用!");
        		$(".custom h2").css("fong-size","16px");
        	},0);
    		/* alert("扫码添加仅支持在微信内部浏览器使用！");*/
    	 });
     }else{
    	    $.ajax({
    	        url:"wx/getWxConfig.html",
    	        type:"get",
    	        success:function(result){
    	        	result=JSON.parse(result);
    	        	console.log(result.date);
    	     wx.config({
    	        debug: false,  
    	        appId: result.date.appid,  
    	        timestamp:result.date.timestamp,  
    	        nonceStr:result.date.nonceStr,  
    	        signature:result.date.signature,  
    	        jsApiList : [ 'checkJsApi', 'scanQRCode' ]  
    	    });//end_config
    	    wx.error(function(res) {  
    	        alert("出错了：" + res.errMsg);  
    	    });  
    	  
    	     wx.ready(function() {
    	        wx.checkJsApi({  
    	            jsApiList : ['scanQRCode'],  
    	            success : function(res) {  

    	            }  
    	        });  
    	  
    	        //扫描二维码
    	        document.querySelector('#scanQRCode').onclick = function() {
    	            wx.scanQRCode({  
    	                needResult : 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，  
    	                scanType : [ "qrCode", "barCode" ], // 可以指定扫二维码还是一维码，默认二者都有  
    	                success : function(res) {
    	                    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果  
    	                    if(result.substring(0,2)=="ff"&&result.substring(result.length-2,result.length)=="ff"){
    	                    	window.location.href = encodeURI('codeAddLable.html?'+result);
    	                    }else{
    	                    	setTimeout(function(){
    	                 			swal("","二维码信息不正确","error");
    	                 		},0);
    	                    }
    	                    //document.getElementById("wm_id").value = result;//将扫描的结果赋予到jsp对应值上  
    	                    //alert("扫描成功::扫描码=" + result); 
    	                    //result = JSON.parse(result)
    	                    //alert(result.data[0]);
/*     	                    if(result.data[0]==null){
    	                    	 setTimeout(function(){
    	                 			swal(" 添加失败！","二维码信息不正确", "error");
    	                 		},0);
    	                    	alert("二维码信息不正确，添加失败！")
    	                    	return false;
    	                    }else if(result.data[0].title==null||result.data[0].sn==null||result.data[0].zone==null){
    	                    	setTimeout(function(){
    	                 			swal(" 添加失败！","二维码信息不正确", "error");
    	                 		},0);
    	                    	alert("二维码信息不正确，添加失败！")
    	                    	return false;
    	                    }; */
/*     	              $.ajax({
    	                    url:"tag/addDeviceTag.html",
    	                    type:"post",
    	                    data:{
    	                        title:result.data[0].title,
    	                        sn:result.data[0].sn,
    	                        zone:result.data[0].zone,
    	                        tempAlert:result.data[0].tempAlert[0]+','+result.data[0].tempAlert[1],
    	                        humAlert:result.data[0].humAlert[0]+','+result.data[0].humAlert[1],
    	                    },
    	                    success:function(result){
    	                        result=JSON.parse(result);
    	                        if(result.result==0){
    	                        	setTimeout(function(){
    	        	    				swal("添加成功！", "点击ok返回首页!", "success");
    	        	    				$(".confirm").unbind();
    	        	    				$(".confirm").click(function(){
    	        	    					window.location.href='lable.html';
    	        	    				});
    	     	             		},0);
    	                        }else if(result.result!=0)
    	                        	setTimeout(function(){
    	        	             		swal(" 添加失败！",result.message, "error");
    	        	             	},0);
    	                    },
    	                    error:function(err){
    	                        console.log(err);
    	                    }
    	                }); */
    	                }  
    	            });  
    	        };//end_document_scanQRCode  
    	          
    	    });//end_ready 
    	        	}
    	        })
     }

    
    }
   
</script>  
    
</html>