<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
 	String path = request.getContextPath();    
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
 	pageContext.setAttribute("basePath",basePath);    
 %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/js/bootstrap-3.3.6-dist/css/bootstrap.css" type="text/css" 	rel="stylesheet">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
    <link href="<%=basePath%>resources/css/sweet-alert.css" type="text/css" 	rel="stylesheet">
    <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
    <script src="<%=basePath%>resources/js/jquery-1.9.1.min.js"></script>
    <script src="<%=basePath%>resources/js/sweet-alert.js"></script>
    <title>流量充值</title>
</head>
<body class="payPage">
<div id="trafficPay">
<jsp:include page="./../menu.jsp"></jsp:include>
   <div id="trafficPayType">
<!--    <table id="userTable">
   		<tr>
   			<td>用户名</td>
   			<td id="userNameTitle"></td>
   		</tr>
   		<tr>
   			<td>真实姓名</td>
   			<td id="trueNameTitle"></td>
   		</tr>
   		<tr>
   			<td>剩余流量</td>
   			<td id="traffic"></td>
   		</tr>
   </table> -->
   
   <table id="moneyTable">
   		<tr>
   			<td class="selMoney">10元</td>
   			<td class="selMoney">30元</td>
   			<td class="selMoney">50元</td>
   		</tr>
   		<tr><td colspan="3" style="height:10px;border:none;"></td></tr>
   		<tr>
   			<td class="selMoney">100元</td>
   			<td class="selMoney">200元</td>
   			<td class="selMoney">500元</td>
   		</tr>
   </table>
   <div id="payMoneyVal">选择购买<span id="payMoney"></span>流量</div>
<!--     <div id="paySel">
    	<input type="button" value="下单" id="order" class="lableOperation">
        <input type="button" value="返回" id="backUser" class="lableOperation">
    </div> -->
    </div>
</div>
 
</div>

</body>

<script src="<%=basePath%>resources/js/pay/pay.js"></script>
</html>