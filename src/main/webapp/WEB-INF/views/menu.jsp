<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"  import="org.dragonnova.business.model.User"%>
<!DOCTYPE html>
<html lang="zh-CN">
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%>
<head>
<title>方格云</title>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-rim-auto-match" content="none"> 
<meta name="viewport" id="viewport"	content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width">
<link href="<%=basePath%>resources/css/style.css" type="text/css" 	rel="stylesheet">
<link href="<%=basePath%>resources/css/sweet-alert.css" type="text/css" rel="stylesheet">
<link href="<%=basePath%>resources/css/media.css" type="text/css" rel="stylesheet">
<script src="<%=basePath%>resources/js/jquery-1.9.1.min.js"></script>
<script src="<%=basePath%>resources/js/sweet-alert.js"></script>
<script src="<%=basePath%>resources/js/jquery.md5.js"></script>
</head>
<body>
<div id="facilityDiv">
	<div id="facilityBtn">
		<img src="<%=basePath%>resources/images/facility.png" style="width: 100%">
	</div>
</div>
<p class="topTitle"><img src="<%=basePath%>resources/images/Logo_2.png">创辉温湿度监测</p>
 <div class="leftNav">
        <a href="lable.html">标签</a>
        <!-- <a href="search.html">查询</a> -->
        <a href="addLable.html">新增</a>
        <a href="router.html">路由器</a>
        <a href="page.html?jsp=lable/amap">地图</a>
        <a href="user.html">用户</a>
 </div>
   <div class="title">
        <span id = 'rightMain_title' class="left"></span>
        <div class="right" id="titleRight">
            <a ></a>
            <a>退出</a>
        </div>
    </div>
<div class="rightMain">
  
</body>
<script src="<%=basePath%>resources/js/user/loginout.js"></script>
<script src="<%=basePath%>resources/js/facility/facility.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var userName= '<%=((User)session.getAttribute("login_user_session")).getUsername()%>';
		if(userName.length>0){
			$("#titleRight").find("a").first().text(userName);
			var myImg = '<img class="userPng" style="vertical-align: middle;height: 20px;">';
			var mySrc = 'resources/images/user.png';
			$("#titleRight a:nth-child(1)").prepend(myImg);
			$(".userPng").attr("src",mySrc);
		};
		$("#titleRight a:nth-child(1)").click(function(){
			window.location.href='user.html';
		});
	});
</script>
</html>

