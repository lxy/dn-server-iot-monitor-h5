<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
    <script src="<%=basePath%>resources/js/jquery.js"></script>
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
    <title>设置</title>
</head>
<body class="settingPage">
<div id="settingLable">
<jsp:include page="./../menu.jsp"></jsp:include>
   <div id="settingLableVal">
   		<p style="text-align: center;">路由编号:<span id="awakenSn"></span></p>
		<p><span class="settingName">标签唤醒周期</span><input maxlength="4" type="number" id="awakenLable">秒</p>
		<p><span class="settingName">数据上报周期</span><input maxlength="4" type="number" id="reportData">秒</p>
		<p><span class="settingName">GPS上报周期</span><input maxlength="4" type="number" id="reportGPS">秒</p>
		<p><span class="settingName">服务状态查询</span><input maxlength="4" type="number" id="serviceState">秒</p>
		<p><span class="settingName">本地报警1</span><input maxlength="4" type="number" id="alert_1">秒</p>
		<p><span class="settingName">本地报警2</span><input maxlength="4" type="number" id="alert_2">秒</p>
		<p><span class="settingName">报警间隔</span><input maxlength="4" type="number" id="alertInterval">秒</p>
    	<p><span class="settingName">路由备注</span><input type="text" id="routerTitleInput"></p>
<!--     <div id="settingLableSel">
        <input type="button" value="保存" id="saveSettingLable" class="editBtn">
        <input type="button" value="返回" id="resetSettingLable" class="editBtn">
    </div> -->
    </div>
</div>
 
</div>

</body>

<script src="<%=basePath%>resources/js/setting/setting.js"></script>
</html>