<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
</head>
<body class="routerDetailsPage">
<div id="routerDetails">
 	<jsp:include page="./../menu.jsp"></jsp:include>
     <div id="routerDetailsVal">
<!--     <div id="routerDetailsSel">
    <input type="button" value="设置" id="editSave" class="routerOperation" name="edit">
        <input type="button" value="返回" id="back" class="routerOperation" onclick="back()">
    </div> -->
    </div>
</div>
 

</div>

</body>
<script>
$('#rightMain_title').html('路由器详情');	
function back(){
	if($("#back").attr("name")==1){
		window.history.go(-1);
	}else{
		window.location.href='router.html'
	};
};
</script>
<script src="<%=basePath%>resources/js/router/routerDetails.js"></script>
</html>