<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%    
String path = request.getContextPath();  
String wspath="ws://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";  
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <link href="<%=basePath%>resources/css/style.css" type="text/css" rel="stylesheet">
     <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
	<script src="<%=basePath%>resources/js/jquery-1.9.1.min.js"></script>
    <title>创辉医疗监测</title>
</head>
<body>
<div id="routerViews">
 <jsp:include page="./../menu.jsp"></jsp:include>
    <div id="routerList">
	
    </div>
</div>


</body>
<script type="text/javascript">
$('#rightMain_title').html('路由器列表');	
function lable(){
	window.location.href='lable.html';
};
function router(){
	window.location.href='router.html';
};
</script>
<script src="<%=basePath%>resources/js/router/router.js"></script>
</html>
