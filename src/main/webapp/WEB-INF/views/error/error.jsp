<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<html>
<%    
String path = request.getContextPath();    
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";    
pageContext.setAttribute("basePath",basePath);    
%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<head>
<title>页面不存在！</title>
 <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico" type="image/x-icon" />  
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<style type=text/css>
body {
	background: #eee;
	text-align: center;
}

a:link {
	text-decoration: none;
	color: #03F;
}

a:visited {
	text-decoration: none;
	color: #F60;
}

a:hover {
	text-decoration: underline;
	color: #F60;
}

a:active {
	text-decoration: none;
	colorwhite;
}

.main {
	margin: 0 auto;
}

.con {
	margin: 0 auto;
	width: 100%;
}

.errorNotes {
		text-align: center;
}

.errorNotes ul {
	height: 30px;
	width: 480px;
	margin: 20px auto;
}

.errorNotes li {
	float: left;
	width: 200px;
	text-align: center;
	line-height: 30px;
	list-style: none;
}

.re {
	margin: 0 auto;
	width: 50%;
	text-align: center;
}

.re .title {
	text-align: center;
	line-height: 30px;
	font-size: 20px;
	font-weight: bold;
	color: #F00;
}

.re dt {
	text-align: left;
	line-height: 30px;
}
</style>
</head>
<body>
	<div class="main">
		<div class="con">
			<div class="errorNotes">
				<div class="re">
					<div class="title">抱歉，页面出错……</div>
					<br>
					<div class="title"><%=response.getStatus()%></div>
						<div th:text="${ex.message }"></div>
				</div>
				${error_message}
				<ul>
					<li><a href="<%=basePath%>">返回首页</a></li>
					<li><a href="javascript:history.go(-1);">返回上一页</a></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>